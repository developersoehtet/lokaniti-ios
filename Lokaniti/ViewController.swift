//
//  ViewController.swift
//  Lokaniti
//
//  Created by Soe Htet on 21/10/16.
//  Copyright © 2016 Soe Htet. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var pageMenu : CAPSPageMenu?
    
    var mmTitle : [String] = ["ပညာရွိ","သူေတာ္ေကာင္း","လူမိုက္","မိတ္ေဆြ","အမ်ိဳးသမီး","ရွင္ဘုရင္","အေထြေထြ"]
    var engTitle : [String] = ["Sage","Good Man","Bad Man","FriendShip","Women","King","Miscellaneous"]
    var firstTable : TextVc?

    @IBOutlet weak var segmentedcontrol: UISegmentedControl!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        
        // MARK: - UI Setup
        
        //UIColor(red: 30.0/255.0, green: 30.0/255.0, blue: 30.0/255.0, alpha: 1.0)

        
        //self.title = "Lokaniti"
        self.navigationController?.navigationBar.barTintColor = .black
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.barStyle = UIBarStyle.black
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.orange]
        
        
//        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "->", style: UIBarButtonItemStyle.Done, target: self, action: "didTapGoToRight")
        
        self.navigationController?.navigationBar.backgroundColor = UIColor(red: 30.0/255.0, green: 30.0/255.0, blue: 30.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.isTranslucent = false
        
        
        var strTitle : String
        
        // MARK: - Scroll menu setup
        
        var tmpTitle : [String]
        if UserDefaults.standard.integer(forKey: "CurrentLanguage") == 0 {
            tmpTitle = mmTitle
            segmentedcontrol.selectedSegmentIndex = 0
            strTitle = "ေလာကနီတိ"
        }else{
            tmpTitle = engTitle
            segmentedcontrol.selectedSegmentIndex = 1
            strTitle = "Lokaniti"
        }
        
//        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: strTitle, style: UIBarButtonItemStyle.plain, target: nil, action: nil)
        
        let backButton = UIBarButtonItem(title: strTitle, style: UIBarButtonItemStyle.plain, target: self, action: nil)
        navigationItem.leftBarButtonItem = backButton
        navigationItem.leftBarButtonItem?.setTitleTextAttributes([NSAttributedStringKey.font: UIFont(name: "Zawgyi-One", size: 14)!], for: UIControlState.normal)
        
        // Initialize view controllers to display and place in array
        var controllerArray : [UIViewController] = []
        
        self.firstTable  = TextVc(nibName: "TextVc", bundle: nil)
        self.firstTable!.title = tmpTitle[0]
        controllerArray.append(self.firstTable!)
        let controller2  = goodManVC(nibName: "goodManVC", bundle: nil)
        controller2.title = tmpTitle[1]
        controllerArray.append(controller2)
        let controller3  = badmanVC(nibName: "badmanVC", bundle: nil)
        controller3.title = tmpTitle[2]
        controllerArray.append(controller3)
        let controller4  = friendVC(nibName: "friendVC", bundle: nil)
        controller4.title = tmpTitle[3]
        controllerArray.append(controller4)
        let controller5  = womenVC(nibName: "womenVC", bundle: nil)
        controller5.title = tmpTitle[4]
        controllerArray.append(controller5)
        let controller6  = kingVC(nibName: "kingVC", bundle: nil)
        controller6.title = tmpTitle[5]
        controllerArray.append(controller6)
        let controller7  = miscellaneousVC(nibName: "miscellaneousVC", bundle: nil)
        controller7.title = tmpTitle[6]
        controllerArray.append(controller7)
        
        // Customize menu (Optional)
        let parameters: [CAPSPageMenuOption] = [
            .scrollMenuBackgroundColor(UIColor(red: 30.0/255.0, green: 30.0/255.0, blue: 30.0/255.0, alpha: 1.0)),
            .viewBackgroundColor(UIColor(red: 20.0/255.0, green: 20.0/255.0, blue: 20.0/255.0, alpha: 1.0)),
            .selectionIndicatorColor(UIColor.orange),
            .bottomMenuHairlineColor(UIColor(red: 70.0/255.0, green: 70.0/255.0, blue: 80.0/255.0, alpha: 1.0)),
            .menuItemFont(UIFont(name: "HelveticaNeue", size: 13.0)!),
            .menuHeight(40.0),
            .menuItemWidth(90.0),
            .centerMenuItems(true)
        ]
        
        // Initialize scroll menu
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0.0, y: 0.0, width: self.view.frame.width, height: self.view.frame.height), pageMenuOptions: parameters)
        
        self.addChildViewController(pageMenu!)
        self.view.addSubview(pageMenu!.view)
        
        pageMenu!.didMove(toParentViewController: self)
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func didTapGoToLeft() {
        let currentIndex = pageMenu!.currentPageIndex
        
        if currentIndex > 0 {
            pageMenu!.moveToPage(currentIndex - 1)
        }
    }
    
    func didTapGoToRight() {
        let currentIndex = pageMenu!.currentPageIndex
        
        if currentIndex < pageMenu!.controllerArray.count {
            pageMenu!.moveToPage(currentIndex + 1)
        }
    }
    
    // MARK: - Container View Controller
    override var shouldAutomaticallyForwardAppearanceMethods : Bool {
        return true
    }
    
    override func shouldAutomaticallyForwardRotationMethods() -> Bool {
        return true
    }

    @IBAction func switch_language(_ sender: AnyObject) {
        
        switch segmentedcontrol.selectedSegmentIndex
        {
        case 0:
            UserDefaults.standard.set(0, forKey: "CurrentLanguage")
            self.viewDidAppear(true)
            //self.firstTable?.tableView.reloadData()
            break
        case 1:
            UserDefaults.standard.set(1, forKey: "CurrentLanguage")
            self.viewDidAppear(true)
            //self.firstTable?.tableView.reloadData()
            break
        default:
            break
        }
    }

    

}

