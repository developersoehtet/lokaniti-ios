//
//  goodManVC.swift
//  Lokaniti
//
//  Created by Soe Htet on 5/11/16.
//  Copyright © 2016 Soe Htet. All rights reserved.
//

import UIKit
import Social

class goodManVC: UITableViewController {
        
    var expText : [String] = ["ရွင္းခ်က္","Explanation"]
    
    var mmNumArray : [String] = ["၁","၂","၃","၄","၅","၆","၇","၈","၉","၁၀","၁၁","၁၂","၁၃","၁၄","၁၅","၁၆","၁၇","၁၈","၁၉","၂၀","၂၁","၂၂","၂၃","၂၄","၂၅","၂၆","၂၇","၂၈","၂၉","၃၀","၃၁","၃၂","၃၃","၃၄","၃၅","၃၆","၃၇","၃၈","၃၉","၄၀"]
    
    var mmArray : [String] = ["သူေတာ္ေကာင္းတို႔ႏွင့္သာလွ်င္ ေပါင္းဖက္ရာ၏။ သူေတာ္ေကာင္းတို႔ႏွင့္ ေပါင္းဖက္ျခင္း ကိုျပဳရာ၏။ သူေတာ္ေကာင္းတို႔၏ တရားကိုသိ၍ ျမတ္သည္ျဖစ္၏။ ယုတ္မာသည္ မျဖစ္။","သူယုတ္တို႔ႏွင့္ ေပါင္းသင္းျခင္းကို စြန္႔ေလာ့။ ေပါင္းဖက္အပ္ေသာ သူေတာ္ေကာင္းကို ဆည္းကပ္ေလာ့။ ေန႔စဥ္ပတ္လံုး ေကာင္းမႈကို ျပဳေလာ့။ သခၤါရတရားတို႔၏ မၿမဲေသာအျဖစ္ကို အၿမဲေအာက္ေမ့ေလာ့။","ေရသဖန္းသီးမွည့္တုိ႔သည္ နီစြာသာလွ်င္ကတည္း။ အတြင္း၌ကား ပိုးမတိျပည္ကုန္သည္ ျဖစ္သကဲ့သုိ႔ ထို႔အတူ သူယုတ္တု႔ိ၏ ႏွလံုးတို႔သည္ ျဖစ္ကုန္၏။","ပိႏၷဲသီးတို႔သည္ကား အပ၌ ဆူးသာထင္ကုန္၏။ အတြင္း၌ကား အၿမိန္အရသာတို႔ႏွင့္ ျပည့္ စံုကုန္သည္ ျဖစ္ကုန္သကဲ့သို႔ ထို႔အတူ သူေတာ္ေကာင္းတို႔သည္လည္း ျဖစ္ကုန္၏။","ေလာက၌ စႏၵကူးပင္သည္ ေျခာက္ေသြ႔ေသာ္လည္း အနံကို မစြန္႔။ ဆင္ေျပာင္သည္ စစ္ေျမ ျပင္သို႔ ေရာက္ေသာ္လည္း လူတို႔၏ မ်က္ေမွာက္၌ တင့္တယ္ျခင္းကိုမစြန္႔။ ႀကံသည္ ယႏၱရားစက္ ၏ အ၀သို႔ေရာက္ေသာ္လည္း ခ်ိဳေသာအရသာကို မစြန္႔။ ပညာရွိသူေတာ္ေကာင္းသည္ ဆင္းရဲျခင္း သို႔ေရာက္ေသာ္လည္း သူေတာ္ေကာင္းတရားကို မစြန္႔သာလွ်င္သတည္း။","ၿခေသၤ့သည္ မြတ္သိပ္ေသာ္လည္း သစ္ရြက္စသည္တုိ႔ကို မစား။ ျခေသၤ့မည္သည္ကား ႀကံဳလီွေသာ္လည္း ဆင္သားကို မစား။","ၿမတ္ေသာအမ်ိဳး၌ ျဖစ္ထေသာ အမ်ိဳးအႏြယ္ကိုေစာင့္ထေသာ အမ်ိဳးေကာင္းသားသည္ မိမိ သည္ ဆင္းရဲျခင္းကို ေရာက္ေသာ္လည္း ယုတ္ေသာအမႈကို မျပဳရာ။","ေလာက၌ စႏၵကူးသည္ ခ်မ္းေျမ့စြာ၏။ ထိုစႏၵကူး၏ ခ်မ္းေျမ့သည္ထက္ လသည္သာလွ်င္ ခ်မ္းေျမ့၏။ စႏၵကူးႏွင့္ လတို႔ခ်မ္းေျမ့သည္ထက္ ေကာင္းစြာဆိုအပ္ေသာ သူတိုေကာင္းတို႔၏ စကားသည္ ခ်မ္းေျမ့စြာ၏။","ေရာင္ျခည္တစ္ေထာင္ေဆာင္ေသာ ေနမင္းသည္ အေနာက္မ်က္ႏွာ၌ တက္ရာ၏။ ျမင္းမိုေတာင္မင္းသည္ ညႊတ္ရာ၏။ အကယ္မလြဲ ငရဲမီးသည္ ခ်မ္းေျမ့ရာ၏။ ေတာင္ထိပ္၌လည္း ၾကာသည္ ပြင့္ရာ၏။ သူေတာ္ေကာင္းတို႔၏ စကားသည္ တရံတဆစ္မွ် မေဖာက္ျပန္ရာ။","သစ္ပင္၏ အရိပ္သည္လွ်င္ ခ်မ္းသာ၏။ ထိုသစ္ပင္အရိပ္၏ ခ်မ္းသာသည္ထက္ ေဆြမ်ိဳး မိဘ၏အရိပ္သည္ ခ်မ္းသာ၏။ ထိုေဆြမ်ိဳးမိဘအရိပ္ခ်မ္းသာ သည္ထက္ ဆရာသမား၏ အရိပ္ သည္ခ်မ္းသာ၏။ ထိုဆရာသမားတို႔၏ အရိပ္ခ်မ္းသာသည္ထက္ မင္း၏အရိပ္သည္ ခ်မ္းသာ၏။ ထိုမင္း၏ အရိပ္ခ်မ္းသာသည္ထက္ ျမတ္စြာဘုရား၏အရိပ္ဟု ဆိုအပ္ေသာတရား မ်ားသည္ မ်ားစြာေသာအျပားအားျဖင့္ ခ်မ္းသာ၏။","ပိတုန္းတို႔သည္ ပန္းကိုအလိုရွိကုန္၏။ သူေတာ္ေကာင္းတုိ႔သည္ ဂုဏ္ေက်းဇူးကို အလိုရွိ ကုန္၏။ ယင္းတုိ႔သည္ အပုပ္ကို အလိုရွိကုန္၏။ သူယုတ္တုိ႔သည္ အမ်က္ကိုအလိုရွိကုန္၏။","အမိယုတ္ေသာသူကား မေကာင္းေသာစကား ရွိသည္ျဖစ္၏။ အဘယုတ္ေသာသူကား မေကာင္းေသာ အမူအက်င့္ရွိ၏။ မိဘႏွစ္ပါး စံုယုတ္မူကား၊ မေကာင္းေသာစကား ရွိသည္လည္း ျဖစ္၏။ မေကာင္းေသာ အမူအက်င့္ရွိသည္လည္း ျဖစ္၏။","အမိျမတ္ေသာသူအား ေကာင္းေသာစကား ရွိ၏။ အဘျမတ္ေသာသူအား ေကာင္းေသာအမူအက်င့္ရွိ၏။ မိဘႏွစ္ပါး စံုျမတ္မူကား ေကာင္းေသာ စကားလည္းရွိ၏။ ေကာင္းေသာ အမူအက်င့္ရွိ၏။","စစ္ေျမျပင္အရပ္၌ ရဲရင္ေသာသူတို႔သည္ အလိုရွိကုန္၏။ ခိုက္ရန္ျဖစ္ေသာအခါတြင္ စကား၌ လိမၼာသူကိုအလိုရွိကုန္၏။ ထမင္း အေဖ်ာ္စားေသာက္ေသာအခါ၌ ခ်စ္ေသာသူကို အလိုရွိကုန္၏။ အနက္သဒၵါခက္ခဲစြာေသာ အေရးအရာ အမူကိစၥရွိေသာ ကာလတို႔၌ ပညာရွိေသာ သူကို အလိုရွိကုန္၏။","ေခြးတစ္ေကာင္သည္ ေခြးတစ္ေကာင္ကို ျမင္၍ ခ်ဳပ္ခ်ယ္လ်က္ ညွဥ္ဆဲအံေသာငွာ သြားကို ျပ၏။ သူယုတ္သည္ သူေတာ္ေကာင္းကို ျမင္လွ်င္ ခ်ဳပ္ခ်ယ္လ်က္ ညွဥ္းဆဲ့ျခင္းအမူကို ျပဳျခင္းငွာ အလိုရွိ၏။","လ်င္စြာအမူကိစၥဟူသမွ်ကို မိမိလည္း မျပဳသင့္။ သူတစ္ပါးကိုလည္း မျပဳေစလင့္။ အေဆာ့တလ်င္ ျပဳမူကား ပညာနည္းသူသည္ ေနာက္မွ ပူပန္ရတက္သည္။","ေလာက၌ အမ်က္ကို ေဖ်ာက္ႏိုင္မူကား တစ္ရံတစ္ဆစ္မွ် မစိုးရိမ္ရ။ သူ႔ေက်းဇူးကို ဆိုတက္ေသာသူကို ဘုရားစေသာ သူေတာ္ေကာင္းတို႔သည္ ခ်ီးမြန္းကုန္၏။ ခပ္သိမ္းကုန္ေသာ သူတို႔၏ ၾကမ္းၾကဳတ္ေသာစကားကို သည္းခံေလာ့။ ဤသို႔ေသာ သည္းခံျခင္းကို ျမတ္၏ ဟူ၍ သူေတာ္ေကာင္းတုိ႔သည္ ဆိုကုန္၏။","ေလာက၌ မစင္အတိျပည့္ေသာ က်ဥ္းေျမာင္းေသာအရပ္၌ ေနရျခင္းသည္ ဆင္းရဲ၏။ ထိုထက္ မခ်စ္မႏွစ္သက္လိုေသာ ရန္သူထံ၌ ေနရျခင္းထက္ ဆင္းရဲ၏။ ထိုထက္လည္း သူေက်းဇူးကို မသိတက္ေသာသူႏွင့္ ေနရျခင္းထက္ ဆင္းရဲ၏။","ဆံုးမသင္ေသာအရာ၌ ဆံုးမရာ၏။ မျပတ္ဆံုးမရာ၏။ ယုတ္မာေသာ အက်င့္မွာလည္း တားျမစ္ရာ၏။ ထိုစကား သင့္စြ။ ထိုဆံုးမတက္ေသာ သူကို သူေတာ္ေကာင္းတို႔သည္ ခ်စ္ခင္အပ္ သည္ျဖစ္၏။ သူေတာ္မဟုတ္သူေတာ္တို႔သည္ မခ်စ္ခင္အပ္သည္ ျဖစ္၏။","ကိုယ့္ထက္ ျမတ္ေသာသူကို ကိုယ့္ကိုႏွိမ္ခ်သျဖင့္ ေအာင္ရာ၏။ ရဲရင့္ေသာသူ သင္းခြဲသျဖင့္ ေအာင္ရာ၏။ ကိုယ့္ေအာက္နိမ့္ေသာသူကို တစိုးတစိ ေပးျခင္းျဖင့္ ေအာင့္ရာ၏။ ကိုယ္ႏွင့္တူေသာ သူကို လံု႔လျပဳျခင္းျဖင့္ ေအာင့္ရာ၏။","ေလာက၌ အဆိပ္ကို အဆိပ္ဟူ၍ မဆိုကုန္။ သံဃာ၏ ဥစၥာကို အဆိပ္ဟူ၍ ဆိုအပ္၏။ အဆိပ္သည္ တစ္ႀကိမ္သာလွ်င္ သတ္တက္၏။ သံဃာ၏ဥစၥာသည္ကား အႀကိမ္ႀကိမ္ အဖန္ဖန္ သတ္တက္၏။","လ်င္ျမန္ျခင္းျဖင့္ ျမင္းေကာင္း ေက်းဇူးကို သိကုန္၏။ ေလးလံေသာ၀န္ကို ရြက္ေဆာင္ သျဖင့္ ႏြားေက်းဇူးကို သိကုန္၏။ ႏို႔ညစ္ျခင္းျဖင့္ ႏို႔ထြက္ႏြားမ၏ ေက်းဇူးကိုသိကုန္၏။ ႀကံစည္လွ်ိဳ႕ ၀ွက္ရာ၌ ေျပာဆိုျခင္းျဖင့္ ပညာရွိေက်းဇူးကို သိကုန္၏။","သူေတာ္ေကာင္းတို႔၏ ဥစၥာသည္ အနည္းငယ္ေသာ္လည္း တြင္း၀၌ ေရကဲ့သို႔ သူတစ္ပါးတို႔ မွီခိုရာ၏။ သူေတာ္ေကာင္း မဟုတ္ေသာ သူတို႔၏ ဥစၥာသည္မ်ားေသာ္လည္း သမုဒၵရာ၌ ေရကဲသို႔ ေသာက္ခ်ိဳးရာမီမွာမရ။","ေရကို ျမစ္တို႔သည္ မေသာက္ကုန္။ သစ္ပင္တို႔သည္ သစ္သီးကို မစားကုန္။ မိုးသည္ အခ်ိဳ႕ ေသာအရပ္တို႔၌ မရြာကုန္။ သူေတာ္ေကာင္းတု႔ိ၏ ဥစၥာသည္လည္း သူတစ္ပါးတို႔ အက်ိဳးမွာသာတည္း။","မေတာင္းတအပ္သည္ကို မေတာင္းတရာ။ မႀကံအပ္သည္ကို မႀကံရ။ တရားသေဘာႏွင့္ ေကာင္းစြာ ယွဥ္သူကိုသာေကာင္းစြာ ႀကံရာ၏။ အခ်ည္းႏွီးေသာ ကာလကို အလိုမရွိရာ။","မႀကံဘဲႏွင့္လည္း ျဖစ္တက္၏။ ႀကံေသာ္လည္း ပ်က္တက္၏။ ထိုးစကားသည္ သင့္စြ။ မိန္းမအား လည္းေကာင္း၊ ေယာက္်ားအား လည္းေကာင္း၊ စည္းစိမ္တို႔သည္ ႀကံတိုင္းလည္း မၿပီးကုန္။","အၾကင္သူသည္ သူေတာ္မဟုတ္ေသာသူအား ခ်စ္အပ္သည္ျဖစ္၏။ သူေတာ္ေကာင္းကို ခ်စ္ျခင္းကို မျပဳ။ မသူေတာ္တို႔၏ တရားကိုသာလွ်င္ ႏွစ္သက္၏။ ထိုသူ၏ ထိုအမူအက်င့္သည္ ပ်က္ဆီးအပ္သည္၏ အေၾကာင္းတည္း။"]
    
    var engArray : [String] = ["Associate with good men; form frienship with them. Knowing the Law from the good men is being noble, not mean.","Abandon the association with the wicked, Apporach the good man who is worthy of association. Do good deeds every day. Ponder always upon the law of impermanence.","The ripe fig is red on the outside, but full of worms in the inside. In the same way the hearts of the wicked are full of evil thoughts.","Jack fruits have thorns on the outside, but inside is sweet, juicy meat, so also the good men are of good heart.","In this world, the sandalwood, though dry, does not abandon its scent. The royal elephant, though on the battle-field, does not forsake its gracefulness in the eyes of people. The sugercane, though under the pressing machine, does not leave out its sweet taste. The virtuous person, though poor, does not abandon the habit of right eousness.","The lion, though hungry, does not eat leaves. Though lean , the lion does not eat elephant's flesh.","The man of noble lineage, preserving the dignity of his birth, though in poverth, does not commit any base act.","In this world, sandalwood gives off pleasant smell. The moon is more pleasant than sandalwood. Much more pleasant is the word of the virtous person.","Should the sun rise in the west; Meru,the monarch of mountains, bend; the hell-fire grow cold and pleasent; the lily bloom on mountain-top, yet the word of the virtuous will remain unaltered.","The shade of a tree is pleasant; more pleasant is the shade of relatives and parents; more pleasant is the shade of the teacher; more pleasant is the shade of the king; much more pleasant, in many ways, is the Dhamma which can be called the shade of Buddha.","Best hanker after the juice of flowers. The virtuous wish for virtue. Flies carve for the putrid. The wicked wish to indulge in anger.","The son of the wicked mother speaks foul language. The son of the wicked father behaves badly. The son of parents who are wicked speaks and acts badly.","The son of a virtuous mother speaks politely. The son of virtuous father has good conduct. The son of who are virtuous speaks politely and behaves well.","At the battle-field brave men are needed. In a dispute, a man who can talk well is required. In a feasts, the loved one should be present. When there is difficulty in comprehension of a text, a man of learning is needed.","You should not do things hurriedly, nor should you make other do so. The person who does things in a hurry may have to repent later.","In this world, the person who can curb his anger does not have to worry even a little. The person who is grateful to other is praised by all virtuous people beginning form Buddha. Tolerate the rude words of vicious persons. Such tolerance is regarded as noble by the virtuous.","In this world, living in a narrow place full of faecues in misery. More miserable is staying with a hated enemy. Even more miserable is to be with an ungrateful person.","Admonish continuoualy where admonition is due. Check the wicked action. That is proper advice. The person who does these acts is loved by the virtuous and hated by the wicked.","One can win a person of higher status by humility; a brave man by causing dissemsion; a person of lower status by giving small favours; one's compeer by working hard.","In this world, poison is really not poison. The real poison is wealth belonging to a monk. Ordinary poison kills a person only once. The poison which is the monk's wealth, kills him over and over again.","The worth of a good horse can be determined by its speed. The usefulness of an ox can be seen froms its capacity to carry heavy burdens. The value of a milch cow can be assened by its-milk-giving. A wise man's intellect can be guaged from his ability to explain intricate thoughts.","The wealth of th virtuous, though magrer, can be depended upon by many, like the water from the well. The wealth of the vicious, though much, can benefit none, like the water in the ocean.","Rivers do not drink water. Trees do not eat fruit. Rain does not fall in some places. The wealth of the virtuous is for the good of all others.","Do not crave for what should not be desired. Do not ponder upon what should not be considered. Think upon what is consistent with the Law. Do not waste your time.","Things happen thought not planned. Though planned, things go away. That is the truth. Men or women do no succeed in obtaining wealth every time they try.","The person loves the person who is not virtuous and does not love the virtuous person, and is pleased with the teaching of the vivcious people. That person's action is the cause of his ruin."]
    
    
    var mmExpArray : [String] = ["သူေတာ္ေကာင္းသည္ ရိုင္းသားေျဖာင္မတ္ေတာ္မွန္သည္။ အကုသိုလ္ကို ေရွာင္ၾကဥ္ေပသည္။ သူေတာ္ေကာင္းတို႔ႏွင့္ ေပါင္းသင္း၍ သူေတာ္ေကာင္းတရားကို ၾကားနာသိမွတ္ျခင္းသည္ ျမတ္၏။ မယုတ္ေပ။","သူေတာ္ေကာင္းကို ေပါင္း၊ သူယုတ္မာကို ေရွာင္၊ ကုသိုလ္ေကာင္းမႈကို ေန႔စဥ္ မျပတ္ျပဳလုပ္၊ မၿမဲေသာ အျဖစ္ကိုေဖာ္ျပေသာ သခၤါရတရားကို အၿမဲဆင္ျခင္သံုးသပ္ေလာ့။","သူယုတ္တို႔သည္ အျပင္ပန္းမွ လွသည္၊ အတြင္းစိတ္မွ ဆိုးညစ္သည္။ သူယုတ္ တို႔သည္ သူေတာ္ေကာင္းေယာင္ေဆာင္တက္သည္။ အေျပာေကာင္းသည္ႏွင့္ အမွ် စိတ္ထား ဆိုးယုတ္သည္။","သူေတာ္ေကာင္းတို႔သည္ ရိုးသားၾက၏။ သူတို႔သည္ သူယုတ္တို႔ကဲ့သို႔  အေပၚယံ ေကာင္းဟန္ျပျခင္း မျပဳေပ။ စိတ္ရင္း ေကာင္းၾကေပသည္။","အခ်ိဳ႕ေသာအရာတို႔၌ ပင္ကိုအရည္အေသြး ရွိသည္။ ထိုအရည္အေသြးတုိ႔သည္ ေကာင္းေသာ အရည္အေသြးမ်ား ျဖစ္လွ်င္ သက္ဆိုင္ရာ အရာ သို႔မဟုတ္ ပုဂၢိဳလ္သည္ ထူးျခား၏။ သူေတာ္ေကာင္း၏ အရည္အေသြးမွာ ပစၥည္းဥစၥာ မျပည့္စံုျငားလည္း မတရားမႈကို ဘယ္ေတာ့မွမလုပ္ဘဲ လမ္းမွန္ကိုသာ ေလွ်ာက္သည္။","ၿခေသၤ့သည္ ဆာေသာ္လည္း သစ္ရြတ္ကိုမစားေပ။ အစာမစားရ၍ ပိန္ေနာ္ ေသာ္လည္း ဆင္သားကို မစားေပ။","မ်ိဳးေကာင္းသားသည္ မိမိ၏ အမ်ိဳးဂုဏ္ကို ေစာင့္ေရွာင့္သည္ အေလ်ာက္ ဆိုးမိုက္ယုတ္ညံ့ ေသာအမႈကို ဘယ္ေသာအခါမွ် မျပဳေပ။","စႏၵကူးနံႏွင့္ လေရာင္တို႔သည္ အသီးသီးခ်မ္းေျမ့ဖြယ္ ျဖစ္၏။ သူေတာ္ေကာင္း စကားသည္ ယင္းတို႔ထက္ ပို၍ ခ်မ္းေျမ့ေပသည္။ သူေတာ္ေကာင္း အဆိုအမိန္႔တို႔သည္ ပူေလာင္ ျခင္းတို႔ကို သက္သာ ေျပေပ်ာက္၍ ခ်မ္းသာျခင္းကိုရေစသည္။","ၿဖစ္ရိုးျဖစ္စဥ္အေပါင္းတုိ႔ ေဖာက္ျပန္ၾကေစကာမူ သူေတာ္ေကာင္း၏စကားသည္ မေဖာက္မျပန္ အၿမဲတည္ေနမည္သာ ျဖစ္၏။","ခ်မ္းသာျခင္းရႏိုင္ေသာ အရိပ္အာ၀ါသ အသီးသီးတို႔တြင္ ဗုဒၶျမတ္စြာ၏ တရားဓမၼတည္းဟူေသာ အရိပ္သည္ ခ်မ္းသာျခင္းအရႏိုင္ဆံုး ျဖစ္ေပသည္။ အရိပ္အသီးသီးတို႔ႏွင့္ ပတ္သတ္၍ ဆိုစရာရွိသည္။ ေဆြမ်ိဳးသားခ်င္းမ်ား၊ မိဘဆရာသမား ႏွင့္ မင္းတို႔၏အရိပ္မ်ားသည္ ပုဂၢိဳလ္၏ေစတနာကိုလိုက္၍သာ ခ်မ္းသားျခင္းကိုေပးႏိုင္သည္။ ေစတနာမေကာင္းေသာ ေဆြမ်ိဳးမ်ား၊ ဆရာမပီသေသာ ဆရာသမားမ်ား၊ မင္းဆိုးမင္းညစ္မ်ား၏ အရိပ္တို႔သည္ ခ်မ္းသာမႈကို မေပးၾကေခ်။ မိဘ၏ ေမတၱာႏွင့္ ဗုဒၶျမတ္စြာ၏ မဟာဂရုဏာေတာ္ ျဖင့္ေဟာၾကား အပ္ေသာ တရားတို႔၏ အရိပ္တို႔ကား အရိပ္ခိုသူတုိ႔အား ခ်မ္းသာျခင္းကို ေပးေပသည္။","ၿမတ္ေသာသူတို႔သည္ ေကာင္းမြန္ျမင့္ျမတ္ေသာ အရာကိုသာ အလိုရွိကုန္၏။ ယုတ္ည့ံသူတို႔သည္ ေဒါသတည္းဟူေသာ ယုတ္ညံ့ေသာ စိတ္ေစတသိတ္၌သာ ေမြ႔ေလ်ာ္၏။","လူမိုက္သာ ရမ္းကား၊ လူလိမၼာသား ယဥ္ပါး'' ဆုိေသာစကား ရွိသည္။ သားသမီး တို႔သည္ ဘ၀အစ၌ မိဘတို႔ထံမွ အရာရာတြင္ သင္ယူၾကသည္။ မိဘမိုက္တို႔၏ သားသမီးမ်ားသည္ လိမၼာခဲလွသည္။","မိဘရိုးရာကို သားသမီးတို႔ လိုက္ၾကသည္။ ဆိုး၀ါးေသာ မိဘတို႔၏ သားသမီးတုိ႔ သည္ အေျပာအဆို အျပဳအမူယုတ္ၾကသည္။ မိေကာင္းဘခင္သားသမီးတို႔၏ အမူအက်င့္ အေျပာအဆုိဆို႔သည္ ေကာင္းၾကေပသည္။","အရာရာတြင္ သက္ဆိုင္ေသာ စြမ္းေဆာင္ႏို္င္ေသာပုဂၢိဳလ္တို႔ကို လိုအပ္၏။ ယင္းသုိ႔ေသာ ပုဂၢိဳလ္တို႔ မရွိလွ်င္ မျဖစ္သင့္ရာတို႔ ျဖစ္ႏိုင္သည္။","","အရာရာကို စဥ္းစားဆင္ျခင္ၿပီးမွ လုပ္သင့္သည္။ မဆင္ျခင္ဘဲ စိတ္လိုက္မာန္ပါ မလုပ္သင့္။ မိမိလည္း မလုပ္သင့္။ သူတစ္ပါးကိုလည္း မခိုင္းသင့္။ သို႔မဟုတ္လွ်င္ ေနာက္တစ္ခါတြင္ ဒုကၡေတြ႔၍ ေနာင္တျဖစ္ေခ်မည္။","အမ်က္ေဒါသသည္  ခ်ိဳးႏွိမ္မႏိုင္ေလာက္ေအာင္ ၾကမ္းတမ္း၏။ အမ်က္ကို ထိန္းႏိုင္သူမွာ စိုးရိမ္ေၾကာင္းၾကျခင္း မရွိေပ။ ေက်းဇူးတရားသိသူကို ဘုရားအစရွိေသာ သူေတာ္ေကာင္းမ်ား ခ်ီးမြန္းၾကသည္။ ၾကမ္းတမ္းေသာစကားကို သည္းခံသူကို သူေတာ္ေကာင္းတို႔ ျမတ္သည္ဟု ဆိုၾကသည္။"," ေက်းဇူးမသိတက္ေသာသူသည္ ရြံမုန္းစရာအေကာင္းဆံံုးျဖစ္သည္။ ေက်းဇူးကန္း သူသည္ ယုတ္မာ၏။ မည္သည့္အကုသိုလ္မ်ိဳးကိုပင္ လုပ္ရန္မခဲခက္ေခ်။ ေက်းဇူးရွင္ ကုိ သတ္ရန္ မေႏွာင့္ေႏွးေခ်။","ဆံုးမစရာရွိလွ်င္ အၿမဲပင္ ဆံုးမပါ။ ယုတ္မာေသာ အမူအက်င္တို႔ကို တားျမတ္ပါ။ ယင္းသည္ သင့္ေလ်ာ္ေသာ စကားျဖစ္၏။ ဆံုးမတက္သူကို သူေတာ္ေကာင္းတို႔က ခ်စ္ခင္သည္ႏွင့္ အမွ် သူယုတ္တို႔က မုန္းတီးၾကသည္။"," မိမိထက္ အဆင့္ျမင့္သူ၊ အဆင့္နိမ့္သူ၊ တန္းတူသူႏွင့္ မိမိထက္ပို၍ ရဲရင့္သူတို႔ကို နည္းအမ်ိဳးမ်ိဳးျဖင့္ ေအာင္ရေပသည္။ နည္းအမ်ိဳးမ်ိဳးသံုး၍ မိမိကအႏိုင္ယူမွသာ မိမိအတြက္ အက်ိဳးရွိမည္။","သာမန္အဆိပ္သည္ လူကိုတစ္ႀကိမ္ တစ္ခါသာ ေသေစႏိုင္သည္။ ရဟန္း၏ ပစၥည္းဥစၥာဟူေသာ အဆိပ္သည္ (သံသရာ၌) အႀကိမ္ႀကိမ္ ေသေစ၏။","အရာရာမွာျဖစ္ေစ၊ ပုဂၢိဳလ္အသီးသီးမွာ ျဖစ္ေစတန္ဖိုးရွိၾက၏။ ယင္းတန္ဖိုးကို စြမ္းရည္ျဖင့္ ခ်င့္ခ်ိန္၍ သတ္မွတ္ႏိုင္သည္။ တန္ဖိုးျဖတ္ရာ၌ သက္ဆိုင္ရာ၏ ထူးျခားသာ စြမ္းရည္ကို ၾကည့္၍ ျဖတ္သင့္သည္။","သူေတာ္ေကာင္းတုိ႔သည္ ေစတနာထက္သန္သည့္အေလ်ာက္ သူတစ္ပါးတို႔အား ေပးကမ္း စြန္႔ႀကဲၾကသည္။ လူအမ်ားက သူတို႔၏ ဒါနအက်ိဳးကို ခံစားၾကရ၏။ သူေတာ္ေကာင္း မဟုတ္သူတို႔သည္ တစ္ကုိယ္ေကာင္းသေဘာရွိေသာေၾကာင့္ သူတို႔မွာ ပစၥည္းဥစၥာေသာ္လည္း တြန္႔တို၏။ ထို႔ေၾကာင့္ သမုဒၵရာမွ ေရကုိမည္မွ် ေသာက္သံုး၍ မရသကဲ့သို႔ မည္သူမွ်အက်ိဳးမခံစားၾကရေခ်။"," ျမစ္က ေရကိုမေသာက္၊ သစ္ပင္က သစ္သီးကိုမစား၊ မိုးကအခ်ိဳ႕ေသာေနရာမ်ား မွာ မရြာ။ ထို႔နည္းအတူ သူေတာ္ေကာင္းတုိ႔သည္ သူတို႔၏ပစၥည္း ဥစၥာမ်ားကို သူတို႔နည္းမသံုးဘဲ အမ်ားအက်ိဳးအတြက္ လွဴတန္းေပးကမ္းသည္။","မရႏိုင္တာကို ေတာင္းတလွ်င္ စိတ္ဆင္းရဲျခင္းသာ အဖတ္တင္မည္။ မႀကံစည္ အပ္တာကို ႀကံစည္လွ်င္ ဦးေႏွာက္ေျခာက္ယံုသာရွိမည္။ တရားဓမၼႏွင့္ ဆက္စပ္ေသာ အေတြးမ်ိဳးကိုသာ ေတြးပါ။ အခ်ိန္မျဖဳန္းပါႏွင့္။ အခ်ိန္သည္အဖိုးတန္သည္။","မႀကံစည္ မႀကိဳးစားပါဘဲလ်က္ ျဖစ္ခ်င္ရာ ျဖစ္တက္သည္။ ႀကံစည္ႀကိဳးစားေသာ္ လည္း မျဖစ္ခ်င္လွ်င္ မျဖစ္။ ကိုးရိုးကားရာ ျဖစ္သြားတက္သည္။ ေလာကမွာ ယင္းသည္ ဓမၼတာျဖစ္၏။","မသူေတာ္ႏွင့္ ေပါင္းဖက္ၿပီး မသူေတာ္တို႔ စကားကိုသာ ႏွစ္သက္လိုက္နာသည္ ပ်က္စီးမည္သာျဖစ္သည္။"]
    
    var engExpArray : [String] = [" A good and virtuous person is honest and right. He avoids ill deads. Associate with good men and learn from them the Law (Dhamma). Such knowledge is sacred, not debased.","Go along with the virtuous; shun the wicked. Do good deads continuously every day, Study always the impermanence of all things.","The wicked are apt to pose as the virtuous. They are very tickly. Their word are sweet, but their hearts are evil.","The virtuous persons are honest. They are not like the wicked who put up appearances; they have goos hearts.","Some things have intrinsic qualities, and if they are good qualities the things or persons who possess them are regarded as distinct. The virtuous person, though lacking wealth, never resorts to ill deeds, and walks alongs the right path.","Lion does not eat leaves though starving : It may be lean from starvation, but it does not eat elephant flesh.","Man of noble birth does not do any mean deed in order to keep up the dignity of his lineage.","The scent of the sandalwood and the light of the moon are resptively pleasant, The speech of the virtuous is much more pleasant. It reduces stressful suffering and render happiness."," Natural phenomena may change,but the world of the virtuous will remains unchanged. It will be good for all time.","Of the various shades and shelters that of the Dhamma, Buddha's word, is the most pleasant. Something has to said of the respective shades. Those of relatives, teachers, and king may give comfort in accordance with the nature of the individual concerned. The shades and shelters that the uncheritable relatives, teachers and king offer cannot possibly by pleasant. Only the love of parents, and the Dhamma which Buddhas taught in His great compassion can make for the shelter-seeker's happiness.","Noble persons wish for what is virtuous. The wicked and the lowly enjoy anger, the evil state of mind."," There is a saying to the effect that the son of a wicked man is rude, whereas the son of a wise man is polite. At the beginning of life, children learn everything from parents, Wicked parent's children are seldom wise.","Children follow their parents's tradition. Those of bad parents have wicked character, and those of virtuous parents are well-be-haved.","In various activities, persons who can give help and guidance are required to be present. If they are absent, undesirable incidents can occur.","","You should do things after due consideration. You should not do anythings impulsively, nor should you make others do hastily. Or you may suffer later and may have to repent.","Anger is so savage that it is difficult to curb. The person who can curb his anger does not have to worry. The one who knows gratitude is called noble by virtuous people.","An ungrateful wretch is the most loathsome one. Such a person is wicked and mean. He does not find it difficult to commit any kind of sin. He does not hesitate to murder his benefactor.","Admonish where admonition is called for. Deter wicked action. That is what it should be. The person who can give such guidance to others is loved by the good people and hated by the bad.","The one superior to him, the one inferior, the one of equal status, and the brave one: all these persons can be overcome by various ways of approach. Thus you will be the winner and get the benefit.","Ordinary poison kills many only once.The poison which is a monk's possessions kills him many times ( in the course of the cycle of existance).","The world of anything or any person can be determined by its capacity or his ability. Evaluation should be made by scanning the special quality concerned.","The virtuous are cheritable, so they give away what little they have. The nonvirtuous persons are selfish, so they are reluctant to give away anything although they have much, jusk like the water in the ocean which cannont enjoyed by anyone.","Just as rivers do not drink their water, trees do not eat their fruit, rain does not fall in all places, so also the virtuous do not enjoy their wealth all by themselves. They give it in charity for the benefit of others.","Do not long for things that cannont be obtained. Do not comtemplate what cannot be thought up, thus making your brain '' run dry''. Do not waste your time. Time is precious.","Trought not planned and tried, things happen by theimselves. Plams made and tried do not produce the desired results. Things are apt to go away. That is natural in this world.","The persom who associate with the wicked and listens to the words of the vicious, wll go to ruin."]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 20
        
        self.tableView.register(UINib(nibName: "text2Cell", bundle: nil), forCellReuseIdentifier: "text2Cell")
        
    }
    
    // number of cell
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return engArray.count
    }
    
    // cell for row id
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "text2Cell") as! text2Cell
        
        let tmp = UserDefaults.standard.integer(forKey: "CurrentLanguage")
        if tmp == 0{
            let indexTitle : String = mmNumArray[indexPath.row] + "/" + mmNumArray[mmArray.count - 1]
            let exp : String = expText[0]
            cell.usernameLbl.text = "\r" + indexTitle + "\r" + mmArray[indexPath.row] + "\r" + "***" + exp + "***" + "\r" + mmExpArray[indexPath.row] + "\r"
        }else{
            let indexTitle2 = String(indexPath.row + 1) + "/" + String(engArray.count)
            let exp : String = expText[1]
            cell.usernameLbl.text = "\r" + indexTitle2 + "\r" + engArray[indexPath.row] + "\r" + "***" + exp + "***" + "\r" + engExpArray[indexPath.row] + "\r"
        }
        
        return cell
        
    }
    
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! text2Cell
        
        // CANCEL ACTION
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        
        // create menu controller
        let menu = UIAlertController(title: "Menu", message: nil, preferredStyle: .actionSheet)
        
        let myanmar = UIAlertAction(title: "Myanmar", style: UIAlertActionStyle.default) { (UIAlertAction) -> Void in
            
            let indexTitle : String = self.mmNumArray[indexPath.row] + "/" + self.mmNumArray[self.mmArray.count - 1]
            let exp : String = self.expText[0]
            cell.usernameLbl.text = "\r" + indexTitle + "\r" + self.mmArray[indexPath.row] + "\r" + "***" + exp + "***" + "\r" + self.mmExpArray[indexPath.row] + "\r"
        }
        let english = UIAlertAction(title: "English", style: UIAlertActionStyle.default) { (UIAlertAction) -> Void in
            
            let indexTitle2 = String(indexPath.row + 1) + "/" + String(self.engArray.count)
            let exp : String = self.expText[1]
            cell.usernameLbl.text = "\r" + indexTitle2 + "\r" + self.engArray[indexPath.row] + "\r" + "***" + exp + "***" + "\r" + self.engExpArray[indexPath.row] + "\r"
        }
//        let facebook = UIAlertAction(title: "Facebook", style: UIAlertActionStyle.default) { (UIAlertAction) -> Void in
//            
//            if SLComposeViewController.isAvailable(forServiceType: SLServiceTypeFacebook){
//                let facebookSheet:SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
//                facebookSheet.setInitialText(cell.usernameLbl.text!)
//                self.present(facebookSheet, animated: true, completion: nil)
//            } else {
//                self.showAlert()
//            }
//        }
//        
//        
//        menu.addAction(facebook)
        menu.addAction(myanmar)
        menu.addAction(english)
        menu.addAction(cancel)
        
        // show menu
        self.present(menu, animated: true, completion: nil)
        
        
        
    }
    
    func showAlert(){
        let alert = UIAlertController(title: "Accounts", message: "Please login to a Facebook account to share.", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
}
