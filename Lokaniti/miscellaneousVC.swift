//
//  miscellaneousVC.swift
//  Lokaniti
//
//  Created by Soe Htet on 6/11/16.
//  Copyright © 2016 Soe Htet. All rights reserved.
//

import UIKit
import Social

class miscellaneousVC: UITableViewController {
    
    
    var expText : [String] = ["ရွင္းခ်က္","Explanation"]
    
    var mmNumArray : [String] = ["၁","၂","၃","၄","၅","၆","၇","၈","၉","၁၀","၁၁","၁၂","၁၃","၁၄","၁၅","၁၆","၁၇","၁၈","၁၉","၂၀","၂၁","၂၂","၂၃","၂၄","၂၅","၂၆","၂၇","၂၈","၂၉","၃၀","၃၁","၃၂","၃၃","၃၄","၃၅","၃၆","၃၇","၃၈","၃၉","၄၀"]
    
    var mmArray : [String] = ["မိန္းမႏွင့္ ရင္းႏွီးေသာ ရဟန္း၌ အဘယ္မွာ သီလရွိအံနည္း။ အသားစားၾကဴးေသာသူ၌ အဘယ္မွာ သနားရွိအံနည္။ ေသေသာက္ၾကဴးသည္သူ၌ အဘယ္မွာ စကားမွန္အံနည္း။ အလိုရမၼက္ႀကီးေသာသူ၌ အဘယ္မွာ အရွက္ရွိအံနည္း။ အပ်င္းႀကီးေသာသူ၌ အဘယ္မွာ အတက္ရွိအံနည္း။ အမ်က္ႀကီးေသာသူ၌ အဘယ္မွာ ဥစၥာရွိအံနည္း။",
        "ေသေသာက္ၾကဴးေသာသူသည္လည္းေကာင္း၊ အခါမဟုတ္သည္၌ သြားေလ့ရွိေသာ သူသည္လည္းေကာင္း၊ သဘင္အသြားမ်ားေသာ သူသည္လည္းေကာင္း၊ ေၾကြအန္အစရွိေသာ ေလာင္းကစားၾကဴး ေသာသူသည္လည္းေကာင္း၊ မေကာင္းေသာ အေဆြခင္ပြန္းရွိေသာ သူသည္လည္းေကာင္း၊ ပ်င္းရိဖင့္ႏႊဲ႔ေသာ သူသည္လည္းေကာင္း၊ ဤသူတို႔ကား စည္းစိမ္တို႔သည္ ပ်က္ဆီးအံ့ေသာ အေၾကာင္းတည္း။",
        "ေန႔အခါ၌ မရႈဦးဘဲ မဆိုအပ္၊ ညဥ့္အခါ၌ မေမးဦးဘဲ မဆိုအပ္၊ ေဘးမွေၾကာက္ေသာ ေတာမုဆိုးသည္ ေတာ၌ေရွ႕ေနာက္ရႈလ်က္ သြားသကဲ့သို႔ က်င့္အပ္၏။",
        "အသက္ရွင္လ်က္လည္း ေသသူဟူ၍ ငါးေယာက္ေသာသူတို႔ကို ဗ်ာသဆရာသည္ ၾကားအပ္ ကုန္၏။ ဆင္းရဲေသာသူ၊ အနာႏွိပ္စက္ေသာသူ၊ မိုက္မဲေသာသူ၊ ၿမီးမ်ားေသာသူ၊ မင္း၀ယ္ ကိုယ္ၾကပ္ခစားေသာသူ၊ ဤသူတို႔တည္း။",
        "ပညာရွိတို႔သေဘာကား မေရာက္ေသးေသာေဘးကို ျမင္သည္ရွိေသာ္ အေ၀းကလွ်င္ ၾကဥ္ရာ၏။ ေရာက္ၿပီး ေရာက္ဆဲေသာ ေဘးကုိျမင္မူကား ေၾကာက္ရြံတက္ေသာ သေဘာမရွိသည္ ျဖစ္၏။",
        "ေလာက၌ အအိပ္ၾကဴးေသာသူသည္ လည္းေကာင္း၊ ေမ့ေလ်ာ့ေသာသူသည္ လည္းေကာင္း၊ ခ်မ္းသာစြာ ေနတက္ေသာသူသည္ လည္းေကာင္း၊ အနာရွိေသာသူ လည္းေကာင္း၊ ပ်င္းေသာသူ လည္းေကာင္း၊ အလိုႀကီးေသာသူလည္းေကာင္း၊ အမူသစ္၌ ေမြ႔ေလ်ာ္ေသာသူ လည္းေကာင္းဤခုႏွစ္ေယာက္ေသာ သူတို႔သည္ က်မ္း၀န္မွ ကင္းကုန္၏။",
        "အိုလာဘ္၊ သူဆင္းရဲသို႔သြားပါေလ။ လာဘ္မ်ားေသာသူသည္ လာဘ္ျဖင့္ျပည့္၏။ အိုပဇ ၨဳန္ နတ္၊ ၾကည္း၌ရြာေလကုန္။ သမုဒၵရာသည္ ေရျဖင့္ျပည္၏။ ဤသဘာ၀တရားသည္ မရွိ။ ကံသာလွ်င္ ပဓာနတည္း။",
        "ေလာက၌ ျပဳဖြယ္ကိစၥသည္ ျပဳၿပီးသည္ရွိေသာ္ ျပဳတက္သူကို တစ္စံုတစ္ေယာက္ေသာသူ သည္ တစ္ဖန္မရႈၾကည့္တက္။ ထို႔ေၾကာင့္ ခပ္သိမ္းကုန္ေသာ အမႈတို႔ကို အၾကြင္းအားျဖင့္ ျပဳရာ၏။",
        "ေလာက၌လဲသည္ ေပါ့၏။ ထိုထက္ လွ်ပ္ေပၚေသာသူသည္ ေပါ့၏။ ထိုထက္ဆရာမိဘတို႔ ၏အဆံုး အမကိုမခံေသာသူသည္ ေပါ့၏။ ထို႔ထက္ ျမတ္ဗုဒၶၶ၏ သာသနာေတာ္၌ ေမ့ေလ်ာ့ေသာ သူသည္ေပါ့၏။",
        "ေလာက၌ ေက်ာက္ထီးသည္ ေလး၏။ ထိုထက္နတ္တို႔၏ ေျပာၾကားျခင္းသည္ ေလး၏။ ထို႔ထက္ ဆရာမိဘတို႔၏ အဆံုးအမသည္ ေလး၏။ ထို႔ထက္ ဗုဒၶၶျမတ္စြာ၏စကားသည္ ေလး၏။",
        "ေလာက၌ လက္ယာလက္ကားကိုယ္၏ ကၽြန္မည္၏။ ဤလက္ယာလက္၌ လက္သန္းကား နား၊ ႏွာေခါင္းတို႔၏ လည္းေကာင္း၊ မ်က္စိတို႔၏လည္းေကာင္းကၽြန္မည္၏။ လက္၀ဲလက္သည္ ေျခတို႔၏ ကၽြန္မည္၏။",
        "ကြမ္း၏ အလယ္ျဖစ္ေသာ အရြယ္္၌ ကုေ၀ရနတ္သည္ ခပ္သိမ္းေစာင့္၏။ အရင္၌ ဘီးလူးသည္ ေစာင့္၏။ အဖ်ား၌လည္း ေဒ၀မိစာၦနတ္ယုတ္သည္ ေစာင့္၏။ ထို႔ေၾကာင့္ ထိုအရင္း အဖ်ားတို႔ကို ျဖတ္၍ စားျငားအံ့၊ က်က္သေရသည္သာလွ်င္ ပြား၏။",
        "ျဗဟၼာသည္သာလွ်င္ သင္ပုဏ္းကို ေစာင့္၏။ နတ္သည္လည္းကမၸေလႊကိုေစာင့္၏။ ထို႔ေၾကာင့္သာလွ်င္ အတက္သင္ေသာ သူတို႔သည္ ထိုျဗဟၼာ၊ ထိုဗိႆဏိုးတို႔ကို ပူေဇာ္ေစကုန္သည္။ အခါခပ္သိမ္း ထိုပူေဇာ္သူတို႔ကို ျမတ္ႏိုးတက္ကုန္၏။",
        "ႏြားတို႔သည္ ခပ္သိမ္းေသာသူတို႔အားအၾကင္ေၾကာင့္ ေကၽြးေမြးတက္ကုန္၏။ စည္းစိမ္ကို ေပးတက္ကုန္၏။ ထို႔ေၾကာင့္သာလွ်င္ အမိအဖသကဲ့သို႔ ႏြားတို႔ကို ျမတ္ႏိုးရာ၏။ အရိုအေသလည္း ျပဳရာ၏။",
        "အၾကင္သူတို႔သည္လည္း ႏြား၏အသားကို စားကုန္၏။ ထိုသူတို႔သည္ အမိ၏အသားကို စာသည္မည္ရာ၏.။ ထိုႏြားတို႔သည္ ေသကုန္ရွိေသာ လင္းတတို႔အားေပးရာ၏။ ေရအယာဥ္၌ ေသာ္လည္းေမ်ာရာ၏။",
        "ၾကာသပေတးေန႔၌ အတက္ပညာစ၍ သင္မူကားၿပီးစီး၏။ တနဂၤေႏြေန႔၊ ေသာၾကာေန႔၌ သင္မူကားလ်စ္လ်ဴရႈ၏။ ဗုဒၶဟူးေန႔၊ တနလၤေန႔၌ သင္မူကားအတက္မတက္ရာ။ စေနေန႔၊ အဂၤါေန႔ ၌ သင္မူကားေသရာ၏။",
        "လဆန္းရွစ္ရက္၊ လဆုတ္ရွစ္ရက္ေန၌ အတက္ပညာသင္မူကား၊ ဆရာကို သတ္သည္ မည္၏။ လဆန္းဆယ့္ေလးရက္၊ လဆုတ္ဆယ့္ေလးရက္တို႔၌ အတက္သင္မူကား၊ တပည့္ကို သတ္သည္မည္၏။ လဆန္းဆယ္ရက္၊ လဆုတ္ဆယ္ရက္ေန႔၌ အတက္ပညာသင္မူကားအတက္ ကိုသတ္သည္မည္၏။ လျပည့္ေန႔၌ အတက္ပညာကိုသင္မူကား အမိအဖကိုသတ္သည္ မည္၏။",
        "ေလာက၌အတက္ပညာသင္ေသာ သူသည္ လဆန္းခုႏွစ္ရက္၊ လဆုတ္ခုႏွစ္ရက္၌ အုန္း သီးမစားရာ။ ကိုးရက္ေျမာက္၌ အိမ္ဘူးကို ထို႔အတူ မစားရာ။ ဆယ္ရက္ေျမာက္၌ ကင္းပိန္႔ကို မစားရာ။ သံုးရက္ေျမာက္၌ ဟင္းမ်ိဳးကို မစားရာ။ စားမိျငားအံ့၊ အတက္သည္ေပ်ာက္၏",
        "ေလာက၌ အမ်ိဳး၏ အက်ိဳးငွာတစ္ေယာက္ေသာ သူကိုစြန္႔ရာ၏။ ရြာ၏အက်ိဳးငွာ အမ်ိဳးကို စြန္႔ရာ၏.။ ဇနပုဒ္၏အက်ိဳးငွာ ရြာကိုစြန္႔ရာ၏.။ မိမိ၏အက်ိဳးငွာ ေျမအားလံုးကိုစြန္႔ရာ၏။",
        "ေလာက၌ ျခေသၤ့သည္လည္းေကာင္း၊ သူေတာ္ေကာင္းသည္လည္းေကာင္း၊ ဆင္သည္ လည္းေကာင္း၊ ဤသူတို႔သည္ အက်ိဳးမရွိေသာ အရပ္ကိုစြန္႔၍ သြားကုန္၏။ က်ီးသည္လည္း ေကာင္း၊ ယုတ္မာေသာ ေယာက္်ားသည္လည္းေကာင္း၊ သမင္သည္လည္းေကာင္း၊ဤသတၱ၀ါတို႔ သည္ ထိုမိမိတို႔က်င္လည္ရာ အရပ္၌သာလွ်င္ ပ်က္စီးျခင္းသို႔ေရာက္ကုန္၏။",
        "အၾကင္အရပ္၌ ျမတ္ႏိုးျခင္းမရွိ။ ခ်စ္ျခင္းလည္းမရွိ။ အေဆြခင္ပြန္းလည္းမရွိ။ တစ္စံုတစ္ေယာက္ေသာ အတက္ပညာသင့္စိမ့္သူလည္း မရွိ။ ထိုအရပ္၌ေနျခင္းကို မျပဳရာ။",
        "ပညာရွိေသာ္ကား လွမ္းေသာ ေျခတစ္ဖက္ႏွင့္ တူေသာအရပ္သစ္ကို ေျမာ္တင္းေသာစိတ္ ျဖင့္သြား၏။ ရပ္ေသာေျခတစ္ဖက္ႏွင့္တူေသာ အရပ္ေဟာင္းကိုမခြာေသာစိတ္ျဖင့္ ရပ္၏။ အရပ္ သစ္ မစူးစမ္းဘဲေနရာေဟာင္းကို မစြန္႔ရာ။",
        "ေလာက၌ ေရာင္း၀ယ္ေသာအခါ၌လည္းေကာင္း၊ အတက္သင္ေသာအခါ၌ လည္းေကာင္း၊ တစ္ဖန္အေစအပါးခံေသာအခါ၌ လည္းေကာင္း၊ ေမထုတ္မွီ၀ဲေသာအခါတို႔၌ လည္းေကာင္း၊ အရွက္အေၾကာက္ကို စြန္႔သည္ ျဖစ္ရာ၏။",
        "မိန္းမတို႔၏ အစားစားျခင္းကား ေယာက္်ားတို႔ထက္ ႏွစ္ဆလြန္သည္ ျဖစ္၏။ ပညာသည္ လည္းေလးဆလြန္၏။ လံုးလက ေျခာက္ဆလြန္သည္လည္းျဖစ္၏။ ကာမဂုဏ္တက္မက္ျခင္းကာ ရွစ္ဆလြန္၏။",
        "ေလာက၌ ႀကံသည္ အဖ်ားမွအစဥ္သျဖင့္ အဆစ္တိုင္းအဆစ္တိုင္း လြန္ေသာထူးေသာ အရသာသည္ျဖစ္၏။ ခင္ပြန္းေကာင္းျဖစ္ေသာ သူေတာ္သူျမတ္သည္လည္း ထိုႀကံ၏အဖ်ားမွ အရင္းတုိင္ေအာင္လြန္၍လြန္၍ ခ်ိဳသည္ႏွင့္တူ၏။ ယုတ္မာေသာအေဆြခင္ပြန္သည္ကား အရင္းအစ၌သာ ခ်ိဳ၍ အဖ်ားတိုင္ ေအာင္မခ်ိဳေသာ ႀကံ၏သေဘာႏွင့္တူ၏။",
        "လယ္လုပ္ေသာသူသည္ လည္းေကာင္း၊ ကုန္သည္သည္လည္းေကာင္း၊ အမတ္သည္ လည္းေကာင္း၊ အၾကားအျမင္ သီတင္းသီလႏွင့္ ျပည့္စံုေသာ ရဟန္သည္လည္းေကာင္း ထိုေလးပါး တို႔သည္ျပန္ေျပာစြာ ျဖစ္ကုန္လတ္ေသာ္ တိုင္းႏိုင္ငံသည္ ျပန္႔ေျပာသည္ျဖစ္ရာ၏။",
        "မသရဇၨ်ာယ္ေသာသူမ်ားအား ပညာသည္ ပ်က္၏။ ထၾကြလံုးလ ၀ီရိယ မရွိသူ၏အိမ္ သည္ ညစ္ႏြမ္းပ်က္ဆီး၏။ အဆင္းလွေသာသူအား ပ်င္းရိျခင္းသည္ အညစ္အေၾကးတည္း၊ ဣေျႏၵကို မေစာင္ေရွာင့္ေသာရဟန္းအား ေမ့ေလ်ာ့ျခင္းသည္ အညစ္အေၾကးတည္း။",
        "ေလာက၌ အခ်ိဳ႕ေသာလံုးလရွိသည္လူတို႔၏ ဥစၥာကားႀကီးေသာ လံုးလရွိကုန္ေသာသူတို႔၏ သာလွ်င္ ဥစၥာအျဖစ္သို႔ေရာက္တက္၏။ ယုတ္မာေသာလူတို႔သည္ ေရွးကံသာ ပဓာနဟု ဆိုကုန္သည္သာလွ်င္တည္း။",
        "ပညာရွိတို႔က ဤသုိ႔မဆိုကုန္။ ခပ္သိမ္းေသာအမႈကိစၥတို႔ကို လံုးလျပဳသည္သာလွ်င္တည္း။ အကယ္၍ ထိုအမူသည္မၿပီးခဲ့အံ့။ ရည္တိုင္းအက်ိဳးမျဖစ္ရံုသာလွ်င္တည္း။ အဘယ္မွာ အျပစ္ရွိအံ့ နည္း။",
        "အၾကင္ေယာက္်ား၊ မိန္းမသည္ နိမ့္ေသာအမ်ိဳး၌လည္းျဖစ္၏။ ႏႈတ္ပညာ၊ လက္ပညာလည္း မရွိ။ အရုပ္အရည္ လည္းမတင့္တယ္။ အစြမ္းအားလည္းမရွိ။ ထိုသို႔ အျပစ္တင္ေသာ္လည္းဤကာ လကား ကာာဆုတည္း၊ ဥစၥာသာလွ်င္ အထူးျပဳေသာကာလတည္း။"]
    
    var engArray : [String] = ["How can there be sila (percept) in monk who cohabits with a woman? How can the man who is greater eater of meat have pity? How can a heavy drinker speak the truth? How can a lascivious person have the sense of shame? How can a sluggard have learning? How can a person of greater anger have wealth?",
        "The drunkard, the one who is in the habit of going out at untimely period, the one who goes to theater often, the regular gambler, the one who has bad friends, the lazy one. These persons are liable to ruin.",
        "In daytime one should not talk about others without looking around, at night one should not speak without being asked. One should be as careful as the hunter who is afraid of harm, looks front and back while going about in the forest.",
        "Though alive, these five persons are dead declares the Niti master, Byasa, The poor man, the ailing man, the foolish man , the man with many debts, and the man who serves the king.",
        "The wise man on seeing the danger that is coming avoids it from afar, but when he sees danger arriving, he faces it without fear.",
        "In this world, the person who sleeps too much, the one who is forgetful, the one who lives in complacence, the one who is ailing, the lazy one, the passionate person, the person who delights in new actions: these seven do not resort to books.",
        "Oh, riches! Go to the poor man. The rich man has wealth in full. Oh, Rain-God! Pour on the dry land. The ocean is fill with water. This prayer is of no avail. No such thing comes up as prayed for Kamma is reigns supreme.",
        "When one has done what is to be done, the one who requested him to do it does not look act upon him. So, one should leave something still to be done.",
        "In this world, cotton is light, lighter is the wonton; lighter still is the one who does not accept the admonition of teachers and parents; even lighter is the person who disregards the admonition of Buddha.",
        "In this world, the umbrella made of stone is heavy. Still heavier, or of more weight, is the word of the gods. Weightier still is the admonition of teachers and parents. The word of Buddha is of greatest weight.",
        "In this world, the right-hand is the slave of the body. The little finger of the right hand is the slave of the ear, the nose and the eyes. The left-hand is the slave of the feet.",
        "The middle part of a betel leaf is guarded by Kuvera god, at the base guards of the ogre, and the evil god guards the tip. So the base and the tip of the betel leaf should be cut off before chewing the leaf, and that act is auspicious.",
        "Barhama looks after the slate. The god (Vishnu) watches the wrapper of the slate. The learners adore that Brahma and that Vishnu god. These celestial always look upon the worshippers with love and kindness.",
        "Cattle feed human beings and provide them with wealth. For that reason cattle should be adored and respected by people.",
        "Those who eat cattle-flesh are like those eating their mother’s flesh. When cattle die, they should be thrown to vultures or into the water.",
        "If one starts learning a lesson on a Thursday, the purpose of learning will be achieved. If one starts on a Sunday or a Friday, the study will be successful only by a half. If the starting day is either a Wednesday or a Monday, the studying will not succeed. If one starts the study on a Saturday or a Tuesday, the student will probably die.",
        "Learning lessons on the eight waxing and waning of the moon is like killing the teacher. If on the fourteen waxing and waning, it is like killing the pupil, if on the tenth waxing and waning it is like killing the study itself, and if on the fullmoon day, it is like killing the parents.",
        "In this world, the student learning an art should not eat cocoanut on the seventh waxing and waning day of the moon. Nor should he eat pumpkin on the ninth day, nor on the twelfth day pinnam, nor curry on the third day. If he happens to have eaten them, his knowledge will disappear.",
        "In this world, for the good of the clan an individual should be sacrificed. For the good of the village, the clean should be sacrificed. For the good of the region, the village should be sacrificed. For the good of oneself the entire world should be sacrificed.",
        "In this world, the lion, the virtuous man, the elephant; these leave the place not suitable for them to remain. The cow, the wicked person, the deer; these creatures remain where they are until they die.",
        "One should not live in a place where there is no respect, no love, no friend, nor a teacher.",
        "The wise person has the mind directed to wards the new place, like the foot moving forward with hope. At the same time, he has the mind still on the old place, like the other foot standing on the old place. One should not leave the old place without enquiring about the new place.",
        "In this world, when doing business of selling and buying, or acquiring knowledge, or serving as a messenger, or copulating, one should suspend the sense of shame and fear.",
        "The women’s appetite is two times stronger than men; their intelligence is four times higher than men; their industry six times greater and their sensuality eight times greater than men.",
        "In this world, sugar cane gradually increase it sweet taste from one joint to another. The good and noble friend is found to be more reliable as friendship develops, like the sugar cane. One the contrary, the evil-minded friend proves less and less reliable like the sugar cane, which at first sweet but gradually lacks sweetness.",
        "The cultivator, the trader, the minister, the virtuous monk: when all these four increase in number and spread out in a country, that country will become developed and prosperous.",
        "To the person who does not recite, the text is lost. To the man who lacks industry, his house is weak and in ruin. To the beautiful one, laziness is a blemish. To the monk who is lax in discipline, negligence of the Law of the Holy Order is blemish.",
        "In this world, the property of those who act industry is likely to become eventually the possession of those more industrious: The evil ones say that the effect of past deeds is of primary importance.",
        "The wise men do not say so. They work hard to do all things they have to do. If that purpose is not achieved, it is only that the desired result does not ensue. Where, then, is the fault?",
        "Men and women of low birth, lacking in wisdom, of no comely personality and without quality: such people are obviously defective, but this is the age of decadence, the age in which money matters most."]
    
    
    var mmExpArray : [String] = ["အက်င့္ဆိုးသူတို႔ ပ်က္စီးရျခင္း အေၾကာင္းကိုေဖာ္ျပသည္။ မိန္းမႏွင့္ ေပါင္းသင္း ေနေသာရဟန္းသည္ ပါရာဇိကက်၍ အဘယ္မွာ သီလရွိမည္နည္း။ အသားစားၾကဴသူသည္ သနားၾကင္နာျခင္း ကင္း၏။ အရက္သမားသည္ စကားမမွန္ျဖစ္တက္သည္။ အလိုရမၼက္ႀကီးသူမွာ ရွက္ရေကာင္းမွန္း မသိေခ်။ အကုသိုလ္ျပဳရမွာ မရွက္ေခ်။ ပ်င္းသူ စာမတက္ေခ်။ ေဒါသႀကီးသူသည္ မည္သည့္အလုပ္အကိုင္ႏွင့္မွ် မသင့္ေသာေၾကာင့္ ဥစၥာပစၥည္းရွိႏိုင္မည္ မဟုတ္ေခ်။",
        "အမူအက်င့္ဆိုးမွာ အလြန္အကၽြံရွိသူတုိ႔သည္ ပ်က္စီးတက္သည္။ အရက္သမား၊ ညဥ့္နက္သန္းေခါင္ လမ္းထြက္သူမ်ား၊ ပြဲႀကိဳက္သူမ်ား၊ ကစားသမာမ်ား၊ အေပါင္းမွားသူမ်ား၊ လူပ်င္းမ်ားသည္ ပ်က္စီးၾကမည္သာ ျဖစ္၏။ သူတို႔ ပ်က္စီးၾကပံုကို အမ်ားျမင္ေတြ႔ႏိုင္သည္။",
        "စကားကို အမွတ္မထင္ မေျပာရာ။ ေန႔တြင္မဆိုခင္ ဟိုဟိုသည္သည္ ဦးစြာၾကည့္ သင့္သည္။ ည၌သူတစ္ပါးက မေမးဘဲႏွင့္ မဆိုသင့္ေပ။ ေတာထဲသို႔လွည့္သြားေသာ မုဆိုးသည္ ေဘးအႏၱရာယ္ ေၾကာက္ေသာေၾကာင့္ ပတ္၀န္းက်င္ကို ၾကည့္၍သြားသကဲ့သို႔ က်င့္ရာ၏။",
        "ဤငါးဦးတို႔သည္ အသက္ရွင္ေသာ္လည္း ေသသကဲ့သို႔ ဆင္းရဲၾက၏။ ေငြေၾကးမဲ သူနာမက်န္းသူ၊ မိုက္မဲသူ၊ ေၾကြးမ်ားသူ၊ ဘုရင့္ကိုခစားရသူ၊ ဤငါးဦးသည္ ရွင္လ်က္ေသသူမ်ားပင္ တည္း၊ ထိုသူတို႔သည္ အျခားသူမ်ားသကဲ့သို႔ လြတ္လပ္ျခင္းမရွိဒုကၡအသီးသီး ေရာက္ၾကရသည္။ အသက္ရွင္လ်က္ လူ႔စည္းစိမ္ကို မခံစားၾကရသျဖင့္ ေသသူသကဲ့သို႔ပင္ျဖစ္၏ဟု နီတိဆရာ ဗ်ာသက ဆိုသည္။",
        "လာမည္ေဘးကို အေ၀းမွ ေရွာင္ရာ၏။ ေရာက္ေလၿပီး ျဖစ္ေသာေဘးကို မေၾကာက္မရြံ ရင္ဆိုင္ရာ၏ဟူ၍ ဆိုလိုသည္။",
        "ေဖာ္ျပပါ ပုဂိၢဳလ္ခုႏွစ္ေယာက္သည္ စာေပက်မ္းဂန္ကို ဖတ္ရႈေလ့လာျခင္း မျပဳ ၾကဟုဆိုလိုသည္။",
        "ဆင္းရဲေသာသူကို လာဘ္လာဘေပး၍မရ။ မိုးေခါင္ေရရွားအရပ္၌လည္း မိုးရြာ ေစျခင္းငွာ မစြမ္းသာ။ ဤဆုေတာင္းသည္ အခ်ည္းအႏွီးတည္း။ ကံသာလွ်င္ ပဓာနတည္း။",
        "တစ္ပါးေသာသူအား အက်ိဳးေဆာင္ေပးသျဖင့္ အက်ိဳးရသူသည္ အက်ိဳးရၿပီးေနာက္ အက်ိဳးျပဳသူကို ျပည္လွည့္မၾကည့္တက္ေခ်။ ထို႔ေၾကာင့္တစ္ခုခု လုပ္ေဆာင္ေပး လွ်င္ ၿပီးျပည့္စံုသည္အထိလုပ္မေပးဘဲ အနည္းငယ္ခ်န္ထားသင့္သည္။ သို႔မွသာထိုသူက မိမိကိုဆက္၍ ဂရုထားေပလိမ့္မည္။",
        "လဲသည္ေပါ့၏။ လွ်ပ္ေပၚသူသည္ ပို၍ ေပါ့၏။ ထိုထက္ပို၍ေပါ့သူသည္ ဆရာမိဘ တို႔၏အဆံုးအမကို မခံသူျဖစ္သည္။ အေပါ့ဆံုးမွာ ဗုဒၶၶ၏အဆံုးအမကို လ်စ္လ်ဴရူသူျဖစ္ေခ်သည္။ ဤေနရာ၌ ေပါ့သည္ဟူရာ၌ အေလးခ်ိန္နည္းျခင္းကို လဲ၌သာ ဆို၏။ အျခားႏိႈင္းယွဥ္ခ်က္ အဆင့္ဆင့္ တို႔တြင္ ေပါ့သည္ဟူသည္မွာ ေပါ့တန္သည္။ အသံုးမက် တန္ဖိုးမရွိဟူ၍ ဆိုလိုသည္။",
        "ေရွ႕အပိုဒ္ကို ေျပာင္းျပန္ယွဥ္ျပခ်က္ ျဖစ္သည္။ ေက်ာက္ထီးသည္ အေလးခ်ိန္ ေလး၏။ နတ္စကား၊ ဆရာမိဘစကားႏွင့္ ဗုဒၶၶစကားတုိ႔သည္ ပို၍ပို၍ အဓိပၸါယ္ ေလးနက္ေပသည္။",
        "လက္ယာလက္သည္ ကိုယ္၏ကၽြန္ျဖစ္သည္။ လႈပ္ရွားမႈမ်ား၌ လက္ယာလက္ကို အသံုးမ်ားသည္။ မ်က္စိ၊ နား၊ ႏွေခါင္းတို႔အတြက္ လက္ယာလက္သန္းကို အသံုးမ်ားသည္။ လက္၀ဲ လက္မွာမူ ေျခေထာက္အတြက္ အသံုးျပဳရသည္။ မ်ားေသာအားျဖင့္ ကိုယ္အထက္ပိုင္းအတြက္ လက္ယာလက္ကို သံုးမည္။ ေအာက္ပိုင္းအတြက္ လက္၀ဲလက္ကို အသံုးမ်ားသည္။",
        "ဤအပိုဒ္သည္ ကြမ္းစားေလ့ရွိေသာသူ (အိႏၵိယလူမ်ိဳးမ်ား) အတြက္ ျဖစ္သည္။ ေရွးျမန္မာတို႔လည္း ကြမ္းစားမည္ဆိုလွ်င္ အရင္းႏွင့္အဖ်ား ကိုက္ျဖတ္ၿပီးမွ ကြမ္းရြက္ကို၀ါး ၾကသည္။",
        "ဤစာပိုဒ္သည္လည္း အိႏၵိယေရွးေဟာင္းက်မ္းမ်ား အလိုကို ေဖာ္ျပသည္။ ပညာသင္သူတို႔က ျဗဟၼာႀကီးႏွင့္ ဗိႆဏိုးနတ္တို႔ကို ပူေဇာ္လွ်င္ ပညာတိုးတက္သည္ဟု ယူဆခဲ့ ၾကသည္။",
        "ဟိႏၵဴလူမ်ိဳးတို႔၏ ၀ါဒအရႏြားတို႔သည္ လူတို႔အား အာဟာရတိုက္ေကၽြး၏။ စည္းစိမ္ဥစၥာတိုးပြားေစ၏။ ထိုအေၾကာင္းေၾကာင့္ ႏြားတို႔ကို လူတို႔က ခ်စ္ခင္ျမတ္ႏိုးရေပသည္။",
        "ေရွ႔မွာပါေသာ ႏြားအေၾကာင္းအဆက္ျဖစ္သည္။ ဟိႏၵဴတို႔သည္ ႏြားသားမစားၾကရ ေပ။ ႏြားကို မိဘသဖြယ္ ေလးစားၾကသည္။ ႏြားေသလွ်င္လင္းတစားရန္ ပစ္ထားျခင္းေသာ္လည္း ေကာင္း၊ ေရ၌ ေမွ်ာျခင္းေသာ္လည္းေကာင္း ျပဳရာ၏။",
        "ဤစားပိုဒ္သည္လည္း ဟိႏၵဴက်မ္းမ်ားအလိုျဖစ္သည္။ ပညာစသင္လွ်င္ ၾကာသပေတးေန႔ကစလွ်င္ ပညာတက္ေျမာက္မည္ဟူေသာ အယူကို ေရွးေခတ္ျမန္မာတို႔လည္း ယူၾက၏။ စင္စစ္မွာပညာစသင္ရက္ ေရြးစရာ မလိုေပ။ သင္သူက ႀကိဳးစား၊ ဆရာကလည္း ေကာင္းစြာသင္ၾကားေပးလွ်င္ ပညာတက္မည္သာတည္း။",
        "ေရွးဟိႏၵဴတို႔၏ အယူျဖစ္သည္။ ဗဟုသုတအျဖစ္ မွတ္ထားၾကရမည္သာ ျဖစ္သည္။",
        "အထူးရွင္းစရာလိုမည္ မဟုတ္ေပ။ ဟိႏၵဴတို႔၏ အယူမွ်သာျဖစ္သည္။",
        "အမ်ားအက်ိဳးအတြ႕က္ အနည္းကိုစြန္႔ရမည္ ဆိုသည္မွာ အမ်ားအက်ိဳးရွိရန္ ေဆာင္ရြက္ရာတြင္ တစ္ဦးတစ္ေယာက္ကိုမငဲ့ဘဲ ေဆာင္ရြက္သင့္သည္ဟု ဆိုလိုသည္။ ေနာက္ဆံုး ၀ါက်မွာ မိမိအက်ိဳးအတြက္ ကမာၻေလာကႀကီးတစ္ခုလံုးကို စြန္႔ရသည္ ဆိုသည္မွာ တစ္ကိုယ္ေကာင္း ျပဳက်င့္ေစရန္ဆိုျခင္း မဟုတ္။ ေရွ႔၀ါက်ေတြမွာ အဆင့္ဆင့္အနစ္နာခံရမည္ ဟု မရည္ရြယ္ေပ။ မိမိအက်ိဳးအတြက္ဟူသည္မွာ ဤေနရာတြင္မိမိသံသရာမွ လြတ္ရာလြတ္ေၾကာင္း အတြက္အစြန္႔ႀကီး စြန္႔ရလိမ့္မည္ဟုဆိုလိုသည္။",
        "မိမိအက်ိဳးမရွိေသာ ေနရာေဒသတြင္ ဆက္လက္မေနသင့္။ သင့္ေတာ္ေသာ ေနရာေဒသသို႔ ေျပာင္းေရြ႔သင့္သည္။ ျခေသၤ့၊ သူေတာ္ေကာင္းႏွင့္ ဆင္တို႔သည္ မေနၾကေပ။ က်ီး၊သူယုတ္ႏွင့္ သမင္တို႔သည္ သူတို႔ရွိရာ ေနရာေဒမွာပင္ ေသသည့္တိုင္ေအာင္ ေနၾကသည္။ အက်ိဳးမေပးေသာေနရာျဖစ္ျငားလည္း ေျပာင္းေရြရန္ မသိၾကေခ်။",
        "ျမတ္ႏုိးမႈမရွိ၊ အခ်စ္မရွိ၊ အေဆြခင္ပြန္းမရွိ၊ ဆရာသမားမရွိ။ ယင္းတို႔မရွိေသာေန ရာ၌ေနလွ်င္ အက်ိဳးပ်က္စီးမည္။ မေနသင့္ေပ။",
        "အေဟာင္းမွ အသစ္ကိုေျပာင္းသင့္လွ်င္ ေျပာင္းရမည္။ သို႔ေသာ္ေျပာင္းတိုင္း လည္းမေကာင္းဟူ၍ ဆိုလိုသည္။ စိုင္ေသကိုပစ္ၿပီး။ စိုင္ရွင္ကို မလိုက္သင့္ဟု ဆိုၾကသည္။ မေသခ်ာဘဲ မလုပ္သင့္ဟု ဆိုလိုျခင္းျဖစ္သည္။ တစ္ရြာမေျပာင္းသူေကာင္းမျဖစ္ ဟူေသာစကားကို ေသခ်င္တဲ့က်ား ေတာေျပာင္းဟူေသာ စကားက ျပန္၍ ခ်ဳပ္ေပးသည္။",
        "အေရာင္းအ၀ယ္ လုပ္ရာမွာ အားနာသည္။ အလုပ္သင့္ဟူေသာ သေဘာထားမ်ိဳး မရွိရ။ ပညာသင္ရာမွာလည္း မသိလွ်င္ ေမးရမွာမရွက္ႏွင့္။ မွားမွာမေၾကာက္ႏွင့္၊ သံတမန္အျဖစ္ သြားရာတြင္ေျပာစရာ စကားကိုမေၾကာက္မရြံ ေျပာရေပမည္။ ေမထုတ္မွီ၀ဲရာတြင္ အရွက္အေၾကာက္ စိတ္ကိုခဏဖယ္ထားရေပမည္။",
        "အစားအေသာက္မွာ မိန္းမက ေယာက္်ားတို႔ထက္ ႏွစ္ဆပိုသည္ဆိုသည္မွာ အစားအေသာက္ေရြးစားတက္သည္။ ေယာက္်ားလိုေတြ႔ရာႀကံဳရာ စားျခင္းမ်ိဳးမဟုတ္ေပ၊ အစား အေသာက္ခ်က္ျပဳတ္တက္သျဖင့္ အစားေကာင္းေရြတက္ဟု ဆိုလုိသည္။ လံုးလေျခာက္ဆပိုသည္ ဆိုသည္မွာအိမ္မႈကိစၥတို႔သည္ ေထြျပားလွသည္။ ေသးငယ္သည္ ကိစၥကေလးမ်ားကိုပင္ လုပ္ရတာ အခ်ိန္ကုန္သည္။ ထို႔ေၾကာင့္မိန္းမတို႔ကေယာက်္ားထက္ ေျခာက္ဆပို၍ ႀကိဳးပမ္းအားထုတ္ရ သည္ဟု ဆိုလိုျခင္းျဖစ္သည္။ ဥာဏ္သြားရာမွာ ေယာက္်ားတို႔ထက္ ေလးဆပိုသည္ ဆိုရာ၌ ေယာက္်ားထို႔ထက္ ရုတ္တရက္ ဥာဏ္ရည္သာသည္ကိုဆိုလိုသည္။ ပရိယာယ္မာယာမ်ာျခင္းသည္ ဥာဏ္မ်ားျခင္းပင္ျဖစ္သည္။",
        "သူျမတ္ႏွင့္သူယုတ္တုိ႔သည္ အရာရာတြင္ေျပာင္းျပန္ျဖစ္သည္။ ျမင့္ျမတ္ေသာ သူေတာ္ေကာင္းမိတ္ေဆြသည္ တစ္စတစ္စပို၍ ရင္းႏွီးသည္ႏွင့္အမွ်ယံုၾကည္အားကိုးမႈ ပို၍ရႏိုင္ သည္။ ႀကံ၏အဖ်ားမွ အရင္းတိုင္ေအာင္ အခ်ိဳပိုလာသလိုႏွင့္ အရင္းမွ အဖ်ား အခ်ိဳေလ်ာ့၍လာ သည့္အျဖစ္တို႔ကို ပမာျပသည္။",
        "လယ္သမားသည္ စားေရးရိကၡာတို႔ကိုထုတ္လုပ္၏။ ကုန္သည္သည္ စီးပြားေရးကို ေဆာင္ရြက္၏။ အမတ္သည္ ႏိုင္ငံအုပ္ခ်ဳပ္ေရးတြင္ ေဆာင္ရြက္၏။ သီလႏွင့္ ျပည့္စံုေသာ ရဟန္း သည္သာသနာ ထြန္းကားေစသည္။ ထိုေလးပါးေသာပုဂၢိဳလ္တို႔ တိုးပြားလွ်င္ ႏုိင္ငံတိုးတက္ ဖြံၿဖိဳး ေပလိမ့္မည္။",
        "မိမိႏွင့္သက္ဆိုင္ေသာ တာ၀န္ပ်က္ကြက္လွ်င္ ပ်က္စီးဆံုးရံႈးျခင္း ျဖစ္သည္။ စာမက်က္သူသည္စာမတက္ေခ်။ ပ်င္းရိသူ၏အိမ္သည္ ပ်က္စီေနသည္။ ရုပ္လွသူမွာ ပ်င္းရိျခင္းသည္ အညစ္အေထးျဖစ္၏။ အက်င့္သီလအားေလ်ာ့ေသာ ရဟန္းအဖို႔၀ိနည္းေတာ္ကို မရိုေသျခင္းသည္ အညစ္အေထးျဖစ္သည္။",
        "လံုးလမရွိေသာသူ၏ ပစၥည္းဥစၥာသည္ သူတို႔၏ ပ်က္ကြက္မႈေၾကာင့္ သူတို႔ထံ၌ အား၍မျဖစ္ဘဲတျဖည္းျဖည္း ေလ်ာ့ပါးသြားကာ လံုးလရွိသည္အေလ်ာက္စီးပြားတိုးတက္သူထံ ေရာက္သြားၾကသည္။ ၀ီရိယနည္းသူတို႔က သူတို႔၏အျပစ္ကိုမျမင္ဘဲ ကံကိုအျပစ္ဖို႔ၾကသည္။",
        "ပညာရွိသူတို႔ကားကံကိုပံုမခ်ၾကေပ။ လုပ္စရာအလုပ္ကို အားႀကိဳးမာန္တက္ လုပ္ၾက၏။ အကယ္၍ ကိစၥတစ္ခုမၿပီးေျမာက္လွ်င္ ထင္တိုင္းမျဖစ္သည္သာပင္၊ ဘယ္သူမွ် မျပစ္ မရွိေပ။ ကံကံ၏အက်ိဳးကိုတကယ္ယံုၾကည္သူသည္ အလုပ္ကိုႀကိဳးစားလုပ္သည္။ မျဖစ္သာလွ်င္ မျဖစ္လာျခင္းမွ်သာျဖစ္သည္။ အလုပ္လုပ္သူမွာေရာ ကံမွာပါ အျပစ္မရွိေပ။",
        "ဇာတိလည္းညံ့၊ ပညာလည္းမရွိ၊ ရုပ္ရည္လည္းမသန္႔၊ ထိုသူတု႔ိသည္ အထင္အရွား ညံ့ၾက၏။ သို႔ေသာ္ဤေခတ္သည္ ဆုတ္ေခတ္၊ ေငြသာပဓာနေခတ္ ျဖစ္ေပတည္း။"]
    
    var engExpArray : [String] = ["Persons with bad habits are liable to ruin. The monk who cohabits with a woman falls from the state of being a monk, according to Parazikan rules of conduct. The great meat-eater does not have pity. The drunkard is a habitual liar. The passionate person has no sense of shame. He does not hesitate to commit a sin. The lazy one will not learn anything. The irritable person is not fit for any work, so he will not have wealth.",
        "People with an excess of bad habits are going to ruin. The drunkard, person who goes out at untimely hours, regular theatregoer, and gambler, person falling into bad company, sluggard: they are all going to ruin. All can see how they are ruined.",
        "One should not speak carelessly. In daytime he should look around before speaking. At night time he should not speak without being asked. The hunter fears danger that could come from any direction. So when goes about in the forest he looks around.",
        "Though alive, these five persons are to be taken as dead. They are man in poverty, man in sickness, man in folly, man I deep debt and man in the service of the king. They are as bad as dead because they are not able to enjoy the pleasures of human life, so says Niti master Byasa.",
        "This stanza means that the coming danger should be avoid from afar and that the arriving danger should be avoid from afar and that arriving danger should be faced without fear.",
        "This stanza says the seven persons mentioned are not given to the study of books.",
        "All the things are arranged by Kamma. The rich is rich. The poor is in the destitution. The ocean is full with water. May the rain fall on dry land! Such prayer is in vain. Kamma is the chief factor. We cannot amend as we wish, so we must accept cheerfully what Kamma offers us.",
        "A person does a good turn, but the receiver of the benefit does not turn back to look upon him. When one’s wishes are fulfilled, one usually fails to look up to him. (he fails to thank him). So do not do things completely for a person, leave something to be done. Then only that person’s attention will linger upon the benefactor.",
        "Cotton is light. The wonton person is lighter. The one who disobeys his teachers and parents is still lighter. The one who disobeys his teachers and parents is still lighter. The one who neglects the teaching of Buddha is the lightest. Here, in this context, the meaning of “light” varies. In the first instance it refers to small weight. In the remaining instances, it means “of no worth”, “useless”.",
        "This is the contrast of the preceding stanza. The stone umbrella is heavy. The word of the gods, that of teachers and parents and finally the word of Buddha are respectively weightier.",
        "Right-hand is the body’s slave. Most of movements and doings are made with the right-hand. As for the left-hand, it is used to deal with the feet. Usually, the upper part of the body is served by the right-hand while the left-hand is used for the lower part.",
        "The stanza is meat for betel-chewing people, (especially the Indians). The Myanmar of old usually followed this advice. They cut off the base and the tip of the betel leaf before they started chewing it.",
        "The stanza relates the custom as prescribed ancient Indian treatises.",
        "According to a doctrine held by the Hindus of India, cattle provide (human beings) with nourishment and promote prosperity. Therefore, people must love and adore cattle.",
        "This is a continuation of the preceding statement on cattle. Hindus must not take cattle-flesh. If cattle die, they should be given to vultures to eat, or thrown into a stream or a river.",
        "This stanza is also in accordance with Hindus treatises. The belief that the starting should be a Thursday was taken up by ancient Myanmars. In fact, there is no need to choose a particular date for starting a study. If the pupil tries hard, and the teacher teaches him well, he will become conversant with the subject he studies.",
        "Just the view of ancient Hindus. We have to note it only as general knowledge.",
        "No explanation seems necessary. These are the views of old time Hindus.",
        "In saying that the few must be sacrifices for the good of many, it means that one must not look for one’s own good in working for the benefit of many people. The last sentence of this stanza refers to the good of oneself in the spiritual sense, not material. In the endeavor for setting oneself free from the cycle of existences (samsara), one must renounce the entire world. That is grand renunciation.",
        "One should not stay on in a place, which does not suit one’s purpose. One should move to a more suitable place. The lion, the virtuous man and the elephant leave the place not suited to them, unlike the crow, the wicked man and the deer who remain where they are to the last.",
        "In a place there is no one to give respect, or love, or there is no friend or teacher. One should not live in such place.",
        "If it is advisable to change from the old to the new it should be done. But not every change is good. There is saying, “Leaving the dead bison for the living one.” It means one should not do anything without being sure. The saying, “if one does not move to a new village, one will not be rich and respectable person, “ is checkmated by another saying which goes.” The tiger fated to die moves to another jungle”",
        "In doing business of selling and buying one should not hesitate to haggle. One should not feel diffident; or think it is not fair. In acquiring knowledge one should not feel shy and hesitate to question about what one does not know, or fear to make mistakes. As a messenger, one should speak out without fear. In having sex, one should be set-aside for a while one’s sense of shame and fear.",
        "The woman are twice better in appetite means they are good choosers of delicacies. Not like men who eat anything available. As they are adept at cooking, they are good at taking good food. Women are industrious six times more than men because household work occupies them in the house all day. The statement that women are four times stronger in intelligence means that they are more quick-witted than men. They have more wiles than men.",
        "The noble person and the wicked one are the reverse at every turn. The noble friend becomes more dependable as the friendship develops in intimacy, and the evil-minded friend proves less and less reliable. The taste of a sugar cane is used here as an example.",
        "The cultivator produces food, the trader works in business, the minister administers the country, and the virtuous monk spreads the Buddha’s Dhamma. The increase in number and the presence of these four all over a country will spell development and prosperity in that country",
        "One’s negligence of duty will spell for him. If a learner does not study, he will not learn his lessons. A lazy man’s house is full of filth and is going to ruin. Laziness is a blemish of the beautiful. Laxity in discipline is a blemish of a monk.",
        "The person who does not work hard loses his wealth and his property eventually gets into the hands of those who work hard. Lazy men blame their kamma (the effect of their past deeds), they do not see their own fault.",
        "The wise men do not put the blame of kamma. They word hard at what they have to do. If the result does not turn out as expected, they take the consequences as they are. The worker or the kamma has no fault.",
        "They are of low birth, with no education, not having comely appearance. These defects are obvious. But this is the age of decadence, when money matters most."]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 20
        
        self.tableView.register(UINib(nibName: "text2Cell", bundle: nil), forCellReuseIdentifier: "text2Cell")
        
    }
    
    // number of cell
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return engArray.count
    }
    
    // cell for row id
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "text2Cell") as! text2Cell
        
        let tmp = UserDefaults.standard.integer(forKey: "CurrentLanguage")
        if tmp == 0{
            let indexTitle : String = mmNumArray[indexPath.row] + "/" + mmNumArray[mmArray.count - 1]
            let exp : String = expText[0]
            cell.usernameLbl.text = "\r" + indexTitle + "\r" + mmArray[indexPath.row] + "\r" + "***" + exp + "***" + "\r" + mmExpArray[indexPath.row] + "\r"
        }else{
            let indexTitle2 = String(indexPath.row + 1) + "/" + String(engArray.count)
            let exp : String = expText[1]
            cell.usernameLbl.text = "\r" + indexTitle2 + "\r" + engArray[indexPath.row] + "\r" + "***" + exp + "***" + "\r" + engExpArray[indexPath.row] + "\r"
        }
        
        return cell
        
    }
    
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! text2Cell
        
        // CANCEL ACTION
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        
        // create menu controller
        let menu = UIAlertController(title: "Menu", message: nil, preferredStyle: .actionSheet)
        
        let myanmar = UIAlertAction(title: "Myanmar", style: UIAlertActionStyle.default) { (UIAlertAction) -> Void in
            
            let indexTitle : String = self.mmNumArray[indexPath.row] + "/" + self.mmNumArray[self.mmArray.count - 1]
            let exp : String = self.expText[0]
            cell.usernameLbl.text = "\r" + indexTitle + "\r" + self.mmArray[indexPath.row] + "\r" + "***" + exp + "***" + "\r" + self.mmExpArray[indexPath.row] + "\r"
        }
        let english = UIAlertAction(title: "English", style: UIAlertActionStyle.default) { (UIAlertAction) -> Void in
            
            let indexTitle2 = String(indexPath.row + 1) + "/" + String(self.engArray.count)
            let exp : String = self.expText[1]
            cell.usernameLbl.text = "\r" + indexTitle2 + "\r" + self.engArray[indexPath.row] + "\r" + "***" + exp + "***" + "\r" + self.engExpArray[indexPath.row] + "\r"
        }
//        let facebook = UIAlertAction(title: "Facebook", style: UIAlertActionStyle.default) { (UIAlertAction) -> Void in
//            
//            if SLComposeViewController.isAvailable(forServiceType: SLServiceTypeFacebook){
//                let facebookSheet:SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
//                facebookSheet.setInitialText(cell.usernameLbl.text!)
//                self.present(facebookSheet, animated: true, completion: nil)
//            } else {
//                self.showAlert()
//            }
//        }
//        
//        
//        menu.addAction(facebook)
        menu.addAction(myanmar)
        menu.addAction(english)
        menu.addAction(cancel)
        
        // show menu
        self.present(menu, animated: true, completion: nil)
        
        
        
    }
    
    func showAlert(){
        let alert = UIAlertController(title: "Accounts", message: "Please login to a Facebook account to share.", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }

    
}
