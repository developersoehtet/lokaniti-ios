//
//  friendVC.swift
//  Lokaniti
//
//  Created by Soe Htet on 5/11/16.
//  Copyright © 2016 Soe Htet. All rights reserved.
//

import UIKit
import Social

class friendVC: UITableViewController {
        
    var expText : [String] = ["ရွင္းခ်က္","Explanation"]
    
    var mmNumArray : [String] = ["၁","၂","၃","၄","၅","၆","၇","၈","၉","၁၀","၁၁","၁၂","၁၃","၁၄","၁၅","၁၆","၁၇","၁၈","၁၉","၂၀","၂၁","၂၂","၂၃","၂၄","၂၅","၂၆","၂၇","၂၈","၂၉","၃၀","၃၁","၃၂","၃၃","၃၄","၃၅","၃၆","၃၇","၃၈","၃၉","၄၀"]
    
    var mmArray : [String] = ["စီးပြားကို ေဆာင့္သည္ရွိေသာ္ သူတစ္ပါသည္းလည္း အေဆြမည္၏။ စီးပြားကို မေဆာင္မူ အေဆြသည္လည္း သူတစ္ပါးမည္၏။ အနာသည္ ကိုယ္၌ျဖစ္လ်က္ စီးပြားကို မေဆာင္ေလ။ ေဆးကားေတာ၌ ျဖစ္လ်က္ စီးပြားကို ေဆာင့္ေလသလွ်င္ကတည္း။",
        "မ်က္ကြယ္္၌ ေက်းဇူကို ေခ်တက္ေသာ၊ မ်က္ေမွာက္္၌ ခ်စ္ဖြယ္ေသာ စကားကိုဆို တက္ေသာ ထိုသို႔သေဘာရွိေသာ အေဆြခင္ပြန္းကို အဆိပ္အိုး၌ ပ်ားကိုၾကဥ္ရာသကဲ့သို႔ ၾကဥ္ရာ၏။",
        "ဥစၥာတည္းယုတ္မူကား အေဆြခင္ပြန္းသည္ စြန္႔တက္၏။ သားမယားတို႔သည္ လည္းေကာင္း၊ ညီသားအကိုတုိ႔သည္ လည္းေကာင္း စြန္႔တက္ကုန္၏။ ဥစၥာရွိေသာ သူတို႔ကိုသာ လွ်င္မွီ၀ဲကုန္၏။ ထို႔ေၾကာင့္ ဥစၥာသည္သာလွ်င္ ေလာက၌ ခင္ပြန္းႀကီးတည္း။",
        "ေလာက၌ ကၽြန္သူခစားကို ေစပါးခိုင္းခန္႔မွသာလွ်င္ ေကာင္းမေကာင္းသိရ၏။ ေဆြမ်ိဳးတို႔ကိုလည္း ေဘးေရာက္ေသာအခါ၌ သိရ၏။ အေဆြခင္ပြန္းကုိ ဥစၥာနည္းေသာအခါ သိရာ၏။ မယားကိုလည္း စည္းစိမ္ကုန္မွသာလွ်င္ သိရာ၏။",
        "အၾကင္သူသည္ အစီးအပြား၌ ယွဥ္ေစတက္၏။ ထိုသူသည္ အေဆြခင္ပြန္းမည္၏။ အ ၾကင္သူသည္ ေကၽြးေမြးတက္သည္ ျဖစ္၏။ ထိုသူသည္ မိဘအမည္၏။အၾကင္သူသည္ ခ်စ္ကၽြမ္း ၀င္၏။ ထိုသူသည္ ခင္ပြန္းမည္၏။ အၾကင္သူသည္ ႏွလံုးၿငိမ္၏။ ထိုသို႔ ၿငိမ္းေစတက္ေသာ သူသည္ မယားမည္၏။",
        "ရန္သူႏွင့္လည္း အကၽြမ္းမ၀င္းရာ။ အေဆြခင္ပြန္း ႏွင့္လည္း အကၽြမ္းမ၀င္ရာ။ အေဆြခင္ပြန္းသည္ အမ်က္ထြက္လတ္ေသာ္ ခပ္သိမ္းေသာ အျပစ္ကိုထင္ရွားျပသ၏။",
        "အၾကင္သူသည္ ခင္ပြန္းႏွင့္ တစ္ႀကိမ္ အမ်က္ထြက္ဖူး၏။ ထိုသူသည္ ေ၀ဖန္ေစ့စပ္ျခင္းငွာ အလိုရွိျငားအံ့။ ကိုယ္၀န္သည္ အသထိုရ္ျခင္းကို ဖြားအံ့ေသာျမင္းမကဲ့သို႔ ေသမင္းႏိုင္ငံတြင္ ေဆာင္၏။",
        "အၾကင္မွ်ေလာက္ေသာ မိမိ၏ အႀကံကာလသည္ မေရာက္ေသး။ ထိုေရြ႕ေလာက္ ရန္သူကိုလွ်င္ ပခံုးျဖင့္ ရြက္ေဆာင္ရာ၏။ မိမိ၏ အႀကံကာလသည္ ျပည့္စံုျခင္း သို႔ေရာက္ေလၿပီေသာ္ ထိုရန္သူကိုလွ်င္ ေက်ာက္၌ရြက္ေဆာင္ေသာ အိုးကိုခြဲသကဲ့သို႔ ဖ်က္ရာ၏။",
        "ၿမီးၾကြင္းသည္ လည္းေကာင္း၊ မီးၾကြင္းသည္ လည္းေကာင္း၊ ထို႔အတူသာလွ်င္ ေရာဂါ ၾကြင္းသည္ လည္းေကာင္း အဖန္တလဲလဲ ပြားတက္ကုန္၏။ ထို႔ေၾကာင့္ အၾကြင္းကို မျပဳရာ။",
        "အၾကင္သူ၏ မ်က္ႏွာကား ပဒုမၼၾကာကဲ့သို႔ရႊင္၏။ ဆိုေသာစကားသည္လည္း စႏၵကူးကဲ့သို႔ ခ်မ္းေအး၏။ ႏွလံုး၌ကား ေသေစတက္ေသာ အဆိပ္ကဲ့သို႔ ရွိ၏။ ထိုသို႔ေသာသေဘာရွိေသာသူကို မမွီ၀ဲရာ။",
        "ၾကမ္းၾကဳတ္ေသာ အရွင္းကို မမွီ၀ဲရာ။ ထိုထက္၀န္တိုရာ အရွင္ကို မမွီ၀ဲရာ။ ထိုထက္ မခ်ီျမွင့္တက္ ေသာ အရွင့္ကို မမွီ၀ဲရာ။ ထိုထက္ ႏွိပ္စက္တက္ေသာ အရွင္ကို မမွီ၀ဲရာသာလွ်င္တည္း။",
        "ဦးခ်ဳိရွိေသာ သတၱ၀ါကို အေတာင္းငါးဆယ္ အရပ္မွၾကဥ္ရာ၏။ ျမင္းကိုကား အေထာင္ငါး ရာမွ ၾကဥ္ရာ၏။ အစြယ္ရွိေသာ ဆင္ကိုကားအေထာင္ တစ္ေထာင္ကသာလွ်င္ ၾကဥ္ရာ၏။ သူမေကာင္းကိုကား အရပ္ကိုစြန္႔သျဖင့္ ၾကဥ္ရာ၏။",
        "မေကာင္းေသာအရပ္ကို လည္းေကာင္း၊ မေကာင္းေသာ ခင္ပြန္းကိုလည္းေကာင္း၊ မေကာင္းေသာ အမ်ိဳးကိုလည္းေကာင္း၊ မေကာင္းေသာအေဆြခင္ပြန္းကို လည္းေကာင္း၊ မေကာင္းေသာ မယားကိုလည္းေကာင္း၊ မေကာင္းေသာ ကၽြန္ကိုလည္းေကာင္း ေ၀းစြာေရွာင္ၾကဥ္ ရာ၏။",
        "အၾကင္ခင္ပြန္းတုိ႔သည္ အနာႏွိပ္စက္ေသာ ကာလ၌လည္းေကာင္း၊ ငတ္မြတ္ေခါင္းပါး ေသာ ကာလာ၌လည္းေကာင္း၊ စည္းစိမ္းပ်က္စီးေသာ ကာလ၌လည္းေကာင္း၊ ရန္သူမိေသာ ကာ လ၌လည္းေကာင္း၊ မင္းအိမ္တံခါး၌ လည္းေကာင္း၊ သုသာန္တစျပင္၌ လည္းေကာင္း ရပ္တည္ေပ ကုန္၏။ ထိုခင္ပြန္းတုိ႔သည္သာလွ်င္ ခင္ပြန္းေကာင္းမည္၏။",
        "တ္ခြန္းခ်ဳိေသာစကားရွိေသာသူသည္ မ်ားေသာအေဆြခင္ပြန္းရွိ၏။ ၾကမ္းၾကဳတ္ေသာ သူသည္ နည္းေသာ အေဆြခင္ပြန္းရွိ၏။ ေနမင္း လမင္းတို႔၏ ဥပမာကိုဤေနရာ၍ သိအပ္၏.။"]
    
    var engArray : [String] = [
        "The person who offers benefit is to be regarded as one's friend. If a friend does not offer benefit, he is regarded as a stranger. A disease which occurs in the body does not offer benefit, but the medicinal herb in the forest offers a cure.",
        "A person who speaks ill of one behind one's back, and talks lovingly in one's presence should be avoided as the bee shuns the pot of poison.",
        "If one is deprived of wealth, his friend leaves him, so do his son and wife, so, too, do his brothers. However, they all associate with the one who has wealth. Therefore, only wealth is one’s great friend.",
        "In this world, a servant’s true worth can be seen when he is assigned a duty; that of a relative when one is in danger; that of a friend when one’s wealth wanes; and that of one’s wife when is reduced to poverty.",
        "The person who helps one to prosper is one’s true friend. The persons who look after one are one’s parents. The person who has affection for one is her husband. The woman, who has a good heart and renders peace to one, is his wife.",
        "Do not get too intimate with enemy nor with friend. Sometimes when a friend gets angry, he is apt to show up all the faults one has.",
        "The person who is angry with his friend for once want to get reconciled. He will come to grief like the mare that gives birth to a stallion.",
        "One should carry even one’s enemy on one’s shoulders before one’s plan has produced results. When the time of success of the plan comes, one should throw the enemy off one’s shoulders just as the person carrying a pot on his head dashes it against a rock.",
        "The remaining portion of unpaid debt, the remnant of fire, so also the still-to-be cured part of a disease, is apt to increase. Therefore, all remnants should not be left behind.",
        "A certain man’s face is as fresh and cheerful as Padomma lotus, his words are cool and sweet like sandalwood, but in his heart is a deadly poison. One should not associate with such a person.",
        "Do not serve a master who is rude. More than that do not serve a master who is uncharitable. More so, do not serve a master who is reluctant to honor where honor is due. All the more, do not serve a master who is cruel.",
        "A horned animal should be avoided at a distance of fifty cubits, a horse at one hundred cubits, a tusker, one thousand cubits. The wicked man should be avoided by leaving the area he lives.",
        "An unwholesome place, a bad mate, a bad relative, a bad wife and a bad servant: they all should be left at a distance.",
        "The friends who look after once when one is suffering from a disease, or who contribute food during the time of famine, or who help one in his failing fortune, or who try to bail our when one is captured by the enemy, or who help to defend one when in custody by king’s order or who attend the funeral of one’s relative, are true friends.",
        "The person who speaks sweetly to one and all has many friends. The main of harsh words has few friends. The comparison between the sun and the moon should be considered in the context."
    ]
    
    
    var mmExpArray : [String] = ["မိမိအက်ိဳးေဆာင္သူသည္ မိမိ၏အေဆြ ခင္ပြန္းျဖစ္သည္။ အက်ိဳးမေဆာင္သူ သည္ တစ္စိမ္းမွ်သာျဖစ္သည္။ ေရာဂါသည္ မိမိ၏ ကိုယ္ထဲမွ ေပၚေပါက္လာေသာ္လည္းမိမိကို အက်ိဳးမျပဳ။ အႏၱရာယ္သာ ျဖစ္ေစသည္။ ေတာမွရေသာ သစ္ဥသစ္ဖုတုိ႔သည္ ေဆး၀ါးအျဖစ္ အနာကို ကုသေပး၍ အက်ိဳးေပးေပသည္။",
        "မိတ္ေဆြတုသည္ ေရွ႕တြင္ တစ္မ်ိဳး ေနာက္ကြယ္တစ္မ်ိဳး ေျပာတက္သည္။ ထိုသူသည္ မိတ္ေဆြမဟုတ္။ အတြင္းရန္သူ ျဖစ္သည္။ ေရွာင္ၾကဥ္အပ္၏။ အဆိပ္အိုးကို ပ်ားကေရွာင္သကဲ့သို႔ပင္။",
        "ေရႊရိုးမွ မ်ိဳးေတာ္ခ်င္တယ္ဟူေသာ စကားရွိသည္။ ဥစၥာရွိသူကုိသားမယား ညီအစ္ကို စသည့္ေဆြမ်ိဳးတို႔က ခ်ဥ္းကပ္ၾကသည္။ ဥစၥာမရွိသူကို ေရွာင္ၾကသည္။ ဥစၥာသည္သာ လွ်င္ မိမိ၏ ေဆြမ်ိဳးႀကီးျဖစ္သည္။",
        "မေစခံကိုခိုင္းၾကည့္မွ အေၾကာင္းသိႏိုင္သည္။ ေဆြမ်ိဳးအေၾကာင္းကို မိမိအခက္အခဲႀကံဳမွသိမည္။ မိတ္ေဆြအေၾကာင္းကို မိမိမွာ စီးပြားက်မွသိမည္။ မယားအေၾကာင္းကုိ မိမိမြဲသြားမွ သိမည္။ သာမန္အေျခအေနမွာ ေကာင္းေသာ္လည္း အေရးႀကံဳမွ နဂိုသေဘာကို သိႏိုင္ေပသည္။",
        "မိတ္ေဆြေကာင္းသည္ မိမိအားစီးပြားတက္ေစရန္ကူညီသည္။ မိမိအား ေကၽြးေမြး ေစာင့္ေရွာင့္သူသည္ မိမိ၏မိဘမ်ားျဖစ္၏။ မိမိအား ခ်စ္ခင္ျမတ္ႏိုးသူသည္ မိမိ၏လင္သားျဖစ္၏။ မိမိအား စိတ္ခ်မ္းသာမႈ ျဖစ္ေသေသာ သေဘာရွိသူသည္ မိမိ၏ မယားျဖစ္ေပသည္။ မိတ္ေဆြေကာင္း မိဘေကာင္း လင္ေကာင္း မယားေကာင္းတုိ႔ အေၾကာင္းကိုဆိုသည္။",
        "ေရာလြန္း ရင္းႏွီးျခင္းသည္ မေကာင္းေပ။ ရန္သူႏွင့္ မေရာႏွင့္။ အထူးဆိုစရာ မရွိေပ။ မိတ္ေဆြႏွင့္လည္း မေရာလြန္းႏွင့္ဟု ဆုိရေပမည္။ ေရာလြန္းလွ်င္ တစ္ခါတစ္ရံ မိမိကို စိတ္ဆိုးသည့္အခါ မိမိ၏အျပစ္တို႔ကို ေဖာ္တက္သည္။ ငယ္က်ိဳးငယ္နာ ေျပာတက္သည္။",
        "မိတ္ေဆြအျဖစ္ ရင္းႏွီးသည့္အေလ်ာက္ တစ္ေယာက္အေၾကာင္း အေကာင္းအ ဆိုးသိေနၾကသည္။ ထိုသို႔ ေနရာမွ တစ္ခါ၌ရန္ျဖစ္ၾက၏။ ရန္ျဖစ္ေသာအခါ ေဒါသအေလ်ာက္ ငယ္က်ိဳးငယ္နာအားလံုး ေဖာ္ထုတ္ေျပာဆိုမိသျဖင့္ စိတ္နာသြားၾကသည္။ ထို႔ေနာက္တစ္ေယာက္ က ျပန္လည္ေပါင္းသင္းလို၍ ေစ့စပ္ေစသည္။ ေစ့စပ္၍ ျပည္လည္ ေပါင္းသင္းေသာ္လည္း အသည္းနာျခင္း ရွိေနသည္။ ေျပာလိုက္ဆိုလိုက္သည္တို႔ကို ေမ့မပစ္ႏိုင္ျဖစ္ေလရာ ေနာက္တစ္ႀကိမ္ ျပန္၍ ရန္ျဖစ္သည္ဆိုလွ်င္ ယခင္ကထက္ပို၍ ဆိုးေခ်မည္။ တုတ္တျပက္ ဓါးတျပက္ ျဖစ္ကာ ေသဆံုးသည္အထိ အေျခအေန ဆိုးႏိုင္သည္။ ျမင္းမသည္ အသတိုရ္ ဟုေခၚေသာ ျမင္းလားႀကီးကို ေမြးရသျဖင့္ ေမြးၿပီးေနာက္ ေသရသကဲ့သို႔ ျပန္၍ ေပါင္းဖက္ ရာမွ ကြဲရသူတို႔သည္ ေသရတက္သည္။ ဤစာအပိုဒ္ရွိ အဆိုမွာ ထိုသို႔အျဖစ္မ်ိဳးကိုညႊန္းသည္။",
        "အႀကံမေအာင္ေသးသည့္ ကာလမွာ ရန္သူပင္ ျဖစ္လင့္ကစားမိမိ၏ ပခံုးေပၚတြင္ တင္၍ သယ္ေဆာင္သြားရေပမည္။ အႀကံေအာင္ေသာအခါ အိုးရြက္လာသူက အိုးကိုပစ္၍ ရိုက္ခြဲ သကဲ့သို႔ ပခံုးေပၚမွရန္သူကို ပစ္ခ်လိုက္ရေပလိမ့္မည္။ အႀကံမေအာင္ခင္ သည္းခံေအာက္က်ိဳ႕ဖို႔ လိုသည္။ သို႔မဟုတ္လွ်င္ မိမိ၏ အႀကံပ်က္သြားႏိုင္သည္။",
        "ေၾကြးအေျပမဆပ္ရဘဲ က်န္ေနလွ်င္ေသာ္လည္းေကာင္း၊ မီးၾကြင္းက်န္ခဲ့လွ်င္ ေသာ္လည္းေကာင္း၊ ေရာဂါမကင္းစင္ဘဲ ဆက္လက္ကုသရန္ က်န္ေနလွ်င္ေသာ္လည္းေကာင္း၊ ထိုအၾကြင္းတို႔သည္ ပြားတက္သည္။ ထို႔ေၾကာင့္အၾကြင္းမွန္သမွ်ကို ခ်န္မထားအပ္ေပ။",
        "အေပၚယံေကာင္းၿပီး အတြင္းေကာက္ေသာသူကို ေရွာင္ၾကဥ္ရေပမည္။",
        "၀န္းထမ္းမ်ားကို သတိေပးေသာ စကားျဖစ္သည္။ မိမိအထက္မွာရွိသည့္ ဆရာသမားတို႔၏ စိတ္ေနစိတ္ထားကုိ ေလ့လာၿပီမွ ၀န္ထမ္းမ်ားအျဖစ္ ေဆာင္ရြက္သင့္သည္ဟု ဆိုလိုသည္။ အထက္လူႀကီး ဆိုး၀ါး ယုတ္မာလွ်င္ လက္ေအာက္ငယ္သားမွာ စိတ္မခ်မ္းမသာ ျဖစ္ရသည္။ ဆိုး၀ါးပံုကို အဆင့္ဆင့္ ေဖာ္ျပထားသည္။ ၾကမ္းၾကဳတ္သည့္သူ၊ ထိုမွတစ္ဆင့္ ေစးနဲသူ၊ ထိုမွတစ္ဆင့္ ခ်ီးျမွင့္သင့္ပါလ်က္ ခ်ီးျမွင့္ရမွာ ၀န္ေလးသူ၊ ေနာက္ဆံုးအဆင့္မွာ ရက္စက္ျခင္းႏွင့္ သက္ဆိုင္သည္။",
        "အႏၱရာယ္ရွိေသာ တိရစာၦန္မ်ားကို အေ၀းမွေရွာင္ရာ၏။ သူယုတ္ကို ေရွာင္ရန္မွာ သူေနေသာအရပ္မွ မိမိကထြက္ခြာသြား၍ ေရွာင္ၾကဥ္ရာ၏။",
        "ေရွာင္ၾကဥ္အပ္ေသာ ပုဂိဳလ္မ်ားကို စားရင္းလုပ္ျပျခင္း ျဖစ္သည္။ မေကာင္းေသာ ဟူေသာ နာမ၀ိေသသနပုဒ္ျဖင့္ ေဖာ္ျပေသာ္လည္း မည္သုိ႔မေကာင္းေၾကာင္း မေဖာ္ျပေပ။ မိမိတို႔ ဘာသာအဓိပါယ္ေဖာ္ၾကရေပလိမ့္မည္။",
        "မင္းအိမ္၌လည္းေကာင္း ဆိုသည္မွာ အစိုးရ၏ ေထာင္ထဲသို႕ေရာက္ေနျခင္းကုိ ညႊန္းသည္ဟု ဆိုလိုသည္။ မင္းအိမ္ဆိုကာမွ်ႏွင့္ ဘုရင္ေနေသာအိမ္ဟု မယူဆရာ။ အမူအခင္းျဖစ္ ေနသည္ကိုညႊန္းသည္။ သုသာန္တစျပင္သို႔ အသုဘပို႔ရန္သြားျခင္း ျဖစ္သည္။ သာေရးနာေရးတို႔ မွာ ကူညီေထာက္ပံျခင္းကိုညႊန္းသည္။",
        "ႏႈတ္ခ်ိဳသွ်ိဳတစ္ပါးဟု ဆို၏။ အမ်ားႏွင့္ ဆက္ဆံေသာအခါ စကားကိုခ်ိဳခ်ိဳသာသာ ေျပေျပလည္လည္ ေျပာတက္သူကို အမ်ားက ႏွစ္သက္ခင္မင္ၾကသည္။ ႏႈတ္ၾကမ္းသူကို မိတ္ မဖြဲ႕ခ်င္ၾကေပ။ ေနမင္းသည္ပူေလာင္ေသာ အေရာင္ကို ေပးသည္။ လမင္းသည္ ေအးျမၾကည္လင္ ေသာအေရာင္ကိုေပး၏။ လူတို႔သည္ လေရာင္ကို ခံခ်င္ၾကသည္။ ယင္းဥပမာအတုိင္းပင္ျဖစ္၏။"]
    
    var engExpArray : [String] = ["The person who does a good trun to one, becomes one's friend. The friend who does not do anything for one's benefit remains a stranger. Disease arises in one's body and does harm. The medicinal herb obtained from the forest cures the disease, the giving benefit to him.",
        "The fake friend speaks well in one's presence and speaks ill in his absence. Such a person is not a genuine friends. He is an internal enemy. He should be shunned, just as the bee avoids the poison pot.",
        "People want to claim themselves as one’s relatives of one has gold”, goes a saying. If one is wealthy all relatives approach him, but when he becomes poor, they leave him.",
        "A servant’s true worth can be seen only when assigned to a duty. When in difficulty one can see if a relative is dependable. When one’s income is less, the true nature of a friend can be seen. One can know whether or not one’s wife is good when one becomes poor.",
        "A good friend helps one to become prosperous. One’s parents are those who look after him. One’s husband gives her affection. One’s wife makes his life peaceful and happy. This is about good friends, good parents, good husband and good wives.",
        "Intimacy is not good. One should not get intimate with the enemy. This is out of the question. But we must warn that one should not get too intimate with a friend. That friend when he gets angry will reveal all one’s blemishes.",
        "The two friends being intimate know each other’s faults and blemishes. One day they fall out and fight. Being very angry, the two filing words revealing each other’s blemishes. So both fell hurt. When they quarrel again they will probably resort to violence, using sticks and swords, thus causing death, like the mare that has to give birth to a stallion (a large and strong horse), at the cost of her life.",
        "Before a plan has proved successful, one should bear the injury from the enemy. When the plan is successful, one can well ignore the enemy’s attack. If one fights before the plan is successful, he can fail.",
        "If a portion of debt is left unpaid the amount of debt will grow with time. Some fire left unquenched may develop into a large fire (and burn down the building). If a disease is no completely cured, and something still has to be done, the disease will increase. Therefore, all remnants should not be left behind.",
        "The person who is well meaning on the outside and evil in the inside should be avoided.",
        "This is meant as a warning to servants. If a master is rude or satiny or unwilling to reward for good service, or worst of all, if he is cruel, no one should serve him.",
        "Dangerous animals should be shunned at respective distances. However, in the case of a wicked man, he should be shunned by leaving the place where he is residing.",
        "This is a listing of “bad” people. The adjective “bad” is prefixed to the various persons. How are they bad? There is no mention of the manner in which each of them is bad. We must define the term “bad” by ourselves.",
        "“The king’s house” is another term for jail. The king’s house doesn’t mean the royal palace. “At the funeral” means attending the funeral. ”Cemetery” indicates going to cemetery to attend the funeral ceremony of one’s relative. In a word, the true friend helps one through thick and thin.",
        "“Sweet words are a cause of popularity,” a saying goes. In dealing with many people the person who speaks pleasantly is popular. People are unwilling to make friends with a man of harsh words. People do not want to bask in hot sun’s rays. The light of the moon is cool and people are pleased to be exposed to it. That is the best example in this case."]
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 20
        
        self.tableView.register(UINib(nibName: "text2Cell", bundle: nil), forCellReuseIdentifier: "text2Cell")
        
    }
    
    // number of cell
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return engArray.count
    }
    
    // cell for row id
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "text2Cell") as! text2Cell
        
        let tmp = UserDefaults.standard.integer(forKey: "CurrentLanguage")
        if tmp == 0{
            let indexTitle : String = mmNumArray[indexPath.row] + "/" + mmNumArray[mmArray.count - 1]
            let exp : String = expText[0]
            cell.usernameLbl.text = "\r" + indexTitle + "\r" + mmArray[indexPath.row] + "\r" + "***" + exp + "***" + "\r" + mmExpArray[indexPath.row] + "\r"
        }else{
            let indexTitle2 = String(indexPath.row + 1) + "/" + String(engArray.count)
            let exp : String = expText[1]
            cell.usernameLbl.text = "\r" + indexTitle2 + "\r" + engArray[indexPath.row] + "\r" + "***" + exp + "***" + "\r" + engExpArray[indexPath.row] + "\r"
        }
        
        return cell
        
    }
    
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! text2Cell
        
        // CANCEL ACTION
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        
        // create menu controller
        let menu = UIAlertController(title: "Menu", message: nil, preferredStyle: .actionSheet)
        
        let myanmar = UIAlertAction(title: "Myanmar", style: UIAlertActionStyle.default) { (UIAlertAction) -> Void in
            
            let indexTitle : String = self.mmNumArray[indexPath.row] + "/" + self.mmNumArray[self.mmArray.count - 1]
            let exp : String = self.expText[0]
            cell.usernameLbl.text = "\r" + indexTitle + "\r" + self.mmArray[indexPath.row] + "\r" + "***" + exp + "***" + "\r" + self.mmExpArray[indexPath.row] + "\r"
        }
        let english = UIAlertAction(title: "English", style: UIAlertActionStyle.default) { (UIAlertAction) -> Void in
            
            let indexTitle2 = String(indexPath.row + 1) + "/" + String(self.engArray.count)
            let exp : String = self.expText[1]
            cell.usernameLbl.text = "\r" + indexTitle2 + "\r" + self.engArray[indexPath.row] + "\r" + "***" + exp + "***" + "\r" + self.engExpArray[indexPath.row] + "\r"
        }
//        let facebook = UIAlertAction(title: "Facebook", style: UIAlertActionStyle.default) { (UIAlertAction) -> Void in
//            
//            if SLComposeViewController.isAvailable(forServiceType: SLServiceTypeFacebook){
//                let facebookSheet:SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
//                facebookSheet.setInitialText(cell.usernameLbl.text!)
//                self.present(facebookSheet, animated: true, completion: nil)
//            } else {
//                self.showAlert()
//            }
//        }
//        
//        
//        menu.addAction(facebook)
        menu.addAction(myanmar)
        menu.addAction(english)
        menu.addAction(cancel)
        
        // show menu
        self.present(menu, animated: true, completion: nil)
        
        
        
    }
    
    func showAlert(){
        let alert = UIAlertController(title: "Accounts", message: "Please login to a Facebook account to share.", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }

    
}
