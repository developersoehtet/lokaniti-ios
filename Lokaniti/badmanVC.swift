//
//  badmanVC.swift
//  Lokaniti
//
//  Created by Soe Htet on 5/11/16.
//  Copyright © 2016 Soe Htet. All rights reserved.
//

import UIKit
import Social

class badmanVC: UITableViewController {
        
    var expText : [String] = ["ရွင္းခ်က္","Explanation"]
    
    var mmNumArray : [String] = ["၁","၂","၃","၄","၅","၆","၇","၈","၉","၁၀","၁၁","၁၂","၁၃","၁၄","၁၅","၁၆","၁၇","၁၈","၁၉","၂၀","၂၁","၂၂","၂၃","၂၄","၂၅","၂၆","၂၇","၂၈","၂၉","၃၀","၃၁","၃၂","၃၃","၃၄","၃၅","၃၆","၃၇","၃၈","၃၉","၄၀"]
    
    
    var mmArray : [String] = ["သူယုတ္ကို အလြန္ခ်စ္အပ္သည္ကို မျပဳအပ္။ ဦးေခါင္းျဖင့္ ရြက္ေဆာင္အပ္ေသာ္လည္း မျပည္ေသာေရရွိေသာအိုးကဲ့သို႔ အုပ္အုပ္က်က္က်က္ ကိုျပဳတက္၏။","ေၿမြသည္ အမ်က္လြန္၏။ သူယုတ္သည္လည္း အမ်က္လြန္၏။ ေျမြထက္ သူယုတ္သည္ လည္းအမ်က္လြန္၏။ ထိုေျမြကိုကား မႏၱယားေဆး၀ါးတို႔ျဖင့္ ၿငိမ္းေစႏိုင္၏။ သူယုတ္တို႔အား အဘယ္မည္ေသာ ေဆးျဖင့္ ၿငိမ္းေစႏိုင္အံ့နည္း။","အၾကင္သူမိုက္သည္ မိမိမိုက္ေသာအျပစ္ကုိ သိ၏။ ထိုသို႔ သိျခင္းေၾကာင့္ ထိုသို႔မိုက္ေသာ သူျဖစ္လ်က္လည္း ပညာရွိမည္ျဖစ္၏။ အၾကင္လူမိုက္သည္ကား မိုက္လ်က္လွ်င္ မိမိကိုယ္ကို ပညာရွိဟု ထင္၏။ ထိုသူမိုက္သည္သာလွ်င္ စင္စင္သူမိုက္ဟူ၍ ဆိုအပ္၏။","သူမိုက္သည္ အၾကင္မွ်ေလာက္ မိမိျပဳေသာ မေကာင္းမႈသည္ အက်ိဳးမေပးေသး။ ထုိမွ်ေလာက္ ပ်ားရည္ကဲ့သို႔ ေအာက္ေမ့တက္၏။ အၾကင္အခါ၌ကား မိမိျပဳေသာ မေကာင္းမႈသည္ အက်ိဳးေပး၏။ ထိုအခါ၌ ဆင္းရဲျခင္းသို႔ေရာက္၏။","သူမိုက္သည္ ကိုယ့္အားရွိမူ မေကာင္း။ အႏိုင္အထက္ သူ၏ဥစၥာကုိရေအာင္ ယူတက္၏။ ပညာနည္းေသာ ထိုလူမုိက္သည္ ကိုယ့္ခႏၶာပ်က္သည္မွ ေနာက္၌ ငရဲသို႔ေရာက္တက္၏။","အိမ္္၌ ဖ်က္္ဆီးတက္သည္ကား ၾကြက္တည္း။ ေတာ္၌ဖ်က္ဆီးတက္သည္ကား ေမ်ာက္တည္း၊ ငွက္္၌ဖ်က္ဆီးတက္သည္ကား က်ီးတည္း။ လူ၌ဖ်က္ဆီး တက္သည္ကား ပုဏၰားတညး္။","ႏိုးၾကားေသာသူအား ညဥ့္သည္ရွည့္၏။ ခရီးပန္းေသာသူအား တစ္ယူဇနာခရီးသည္ ရွည္၏။ သူေတာ္ေကာင္းတရားကုိ မသိကုန္ေသာသူမိုက္အား သံသရာသည္ရွည္၏။","ယုတ္ေသာ သေဘာရွိေသာသူသည္ သူတစ္ပါးတို႔၏ ႏွမ္းေစ့ခန္႔မွ်သာရွိေသာ အနည္းငယ္ေသာ အျပစ္ကိုသာလွ်င္ ျမင့္တက္၏။ အုန္းသီးေလာက္ရွိေသာ မိမိအျပစ္ကိုကား မျမင္တက္။","ပညာရွိေသာ သူသည္မိမိ၏ အျပစ္ကို သူတို႔ကိုမသိေစရာ။ သူတစ္ပါး၏ အျပစ္ကိုကား သိေအာင္ျပဳရာ၏။ အဘယ္ကဲ့သို႔နည္းဟူမူကား လိပ္တို႔သည္ ေျခလက္ေခါင္းတုိ႔ကို လွ်ိဳ၀ွက္သကဲ့သို႔ မိမိအျပစ္ကို လွ်ိဳ႕၀ွက္ရာ၏။ သူတစ္ပါးတို႔ အျပစ္ကိုလည္း မွတ္ရာ၏။","လူမိုက္သည္ ပညာရွိကား ခ်ီးမြန္းျခင္းျဖင့္ ဒဏ္ကိုေပးသည္မည္၏။ ပညာရွိကို ပညာရွိသည္သာလွ်င္ ခ်ီးမြန္းျခင္းသည္ ေကာင္းစြာ ခ်ီးမြန္းျခင္းမည္၏။","လိုခ်င္တပ္မက္ျခင္းရွိေသာသူကို တန္စိုးလက္ေဆာင္ေပးျခင္းျဖင့္ ယူရာ၏။ မာနခက္ထန္ ေသာသူကို လက္အုပ္ခ်ီသျဖင့္ ယူရာ၏။ သူမိုက္ကိုကား အလိုသိုလိုက္သျဖင့္ ယူရာ၏။ ပညာရွိကုိ အဟုတ္အမွန္ဆိုသျဖင့္ ယူရာ၏။"]
    
    
    
    var engArray : [String] = ["Do not be so loving to the wicked person. The pot, carried on one's head, not being full of water, is unstable on the head.","The snake has an excess of anger. So does the wicked person; even more furious than the snake. The snake can be subdued by charm and drug. How can one subdue the wicked? Which drug could do it?","The foolish person who knows his foolishness is said to be a wise person. However, the foolish person who thinks himself as a wise man inspite of his foolishness, must be said to be a real foolish man.","The fool thinks that his misded, which has yet to produce the results, is as sweet an honey. When his misdeed gives him due results,only then he will get into trouble.","If the fool has physical strength, it is not good. He is likely to rob others of their property. The fool who has little education goes to hell when his body perishes.","In the house, rat destroys things. In the forest, monkey is destructive. Among birds, the crow is destructive. Among people, the destructive person is the brahmin.","To the wakeful person the night is long. To the tired traveller one league is a long distance. To the fool who does not know the doctor of the virtuous, the samsara (cycle of existences) is a long duration.","Evil-minded person sees other's fault, how ever samll as a sesamun seed, but does not see his own fault which is as big as a coconut.","The wise man should not make his fault known to others while he talkes notice of other's faults; just as a tortoise hides its limbs. He should learn a lesson from other's faults.","When a fool praises a wise man, such praise amounts to punishment for the latter. Only when a wise man praises another, it is a genuine praise.","One should win over the greedy person by given him bribes. The arrogant one should be won over by giving him deep respects. The fool should be won over by giving him concessions. The wise man should be won over by telling the truth."]
    
    
    var mmExpArray : [String] = ["မျပည့္ေသာအိုး ေဘာင္ဘင္ခက္ဆိုသလို မတည္ၿငိမ္ေသာ လူယုတ္မာကို ေမတၱာမ်ားစြာ မထားသင့္ေပ။ လူယုတ္မာသည္ သမာဓိမရွိေသာေၾကာင့္ မိမိအေပၚတြင္ သစၥာမရွိဘဲ သူ႔အလိုမက်လွ်င္ ရန္ျပဳတက္သည္။","သူယုတ္သည္ ေျမြထက္ပို၍ ေဒါသျပင္းထန္၏။ ေျမြကို ေဆး၀ါးမႏာၱန္တို႔ျဖင့္ ႏွိပ္ကြပ္ႏိုင္သည္။ သူယုတ္ကို မည္သည့္ေဆး၀ါးျဖင့္မွ် ႏွိပ္ကြပ္ႏိုင္မည္ မဟုတ္ေခ်။ သူယုတ္သည္ အၿမဲပင္ အႏၱရာယ္ ျပဳႏိုင္သည္။"," မိုက္သူကား မိုက္မွန္းသိလွ်င္ ပညာရွိဟု ေခၚဆိုႏိုင္သည္။ အေၾကာင္းမွာ သူသည္ ဆက္၍ မမိုက္ေတာ့ဘဲ ျပဳျပင္၍ လူလိမၼာျဖစ္ႏိုင္သည္။ အကယ္၍ လူမိုက္က လူမိုက္ျဖစ္ပါလ်က္ သူ႔ကိုယ္သူ လူလိမၼာဟုထင္ေနလွ်င္ သူ၏အမိုက္ေရာဂါ မေပ်ာက္ဘဲ လူမိုက္စစ္စစ္ျဖစ္ေခ်သည္။","သူမိုက္သည္ သူ႔မေကာင္းမႈ အက်ိဳးမခံစားရေသးခင္မွာ ေကာင္းလွၿပီ၊ ပ်ားရည္ သို႔ ခ်ိဳသည္ဟုထင္၏။ မေကာင္းမႈ အက်ိဳးေပးေသာ အခါမွသာ ဒုကၡေရာက္ေလေတာ့သည္။","သူမိုက္သည္ သူ႔ခႏၶာကိုယ္ အင္းအားကို သူတစ္ပါးပစၥည္းကို လုယူရန္ အသံုးျပဳ တက္သည္။ သူေသေသာအခါ သူ႔အင္အား ေပ်ာက္ကြယ္သြားၿပီးလွ်င္ ေနာင္ဘ၀တြင္ ငရဲက်ေခ်သည္။"," ေရွ႕ေခတ္က ပုဏၰားသည္ မ်ားအားျဖင့္ ဘုရင့္အတိုင္ပင္ခံ ျဖစ္သည္။ စိတ္ထားယုတ္ေသာပုဏၰသည္ သူတစ္ပါးပ်က္စီးျခင္းငွာ ဘုရင့္ထံတြင္ ကုန္းတိုက္တက္သည္။ ဇာတ္နိပါတ္မ်ားတြင္ ပုဏၰားသည္ လူဆိုးအခန္းမွာ သရုပ္ေဆာင္ရသည္။","အတိုင္းအတာကာလတို႔သည္ ခံယူေသာပုဂိၢဳလ္၏ စိတ္သေဘာထားကိုလိုက္၍ ရွည္သည္တို႔သည္ ျဖစ္၏။ အိပ္မေပ်ာ္သူအဖို႔ ညသည္ ကာလရွည္ၾကာသည္။ ခရီးပန္းသူအဖို႔ ခရီးမကြာေ၀းလွ ေသာ္လည္းကြာေ၀းသည္ဟု ထင္သည္။ မသူေတာ္တို႔ အဖို႔ သံသရာမွ မလြတ္ႏိုင္ သည္အေလ်ာက္ သံသရာရွည္ေလသည္။","စိတ္ရင္းမေကာင္းသူသည္ သူတစ္ပါးအျပစ္ မည္မွ်ပင္ေသးေစကာမူ ျမင္တက္၏။ သူ႔အျပစ္ မည္မွ်ပင္ႀကီးပါေသာ္လည္း သူ မျမင္တက္ေခ်။ တစ္ကိုယ္ေကာင္းသမားတို႔၏ သဘာ၀ ပင္ျဖစ္၏။ သူတစ္ပါးအျပစ္ကေလးကို အက်ယ္ခ်ဲ့တက္သည္။ သူအျပစ္ကို အတံုခ်ံဳ႕တက္သည္။","လိမၼာေသာသူသည္ မိမိ၏အျပစ္အနာတို႔ကို သူတစ္ပါးတုိ႔အား မသိေစဘဲ လွ်ိဳ႕၀ွက္ထားသည္။ သူတစ္ပါးတုိ႔ သိလွ်င္ သူတို႔မွာ အကုသိုလ္ျဖစ္ႏိုင္သည္။ သို႔ေသာ္ သူတစ္ပါးတုိ႔၏ အျပစ္အနာအဆာတုိ႔ကို သိေအာင္လုပ္၍ သခၤန္းစာယူသင့္သည္။ လိပ္သည္ ေျခလက္ေခါင္းတို႔ကို သိုသိုသိပ္သိပ္ထားသကဲ့ပင္။","သူမိုက္က ပညာရွိကို ခ်ီးမြမ္းျခင္းမွာ ခ်ီးမြမ္းႏိုင္စြမ္း မရွိသူက ခ်ီးမြန္းျခင္းမွ်သာျဖစ္ သျဖင့္ အျပစ္ဆိုျခင္းတစ္မ်ိဳး ျဖစ္ေခ်သည္။ ပညာရွိကုိ ပညာရွိကခ်ီးမြမ္းမွသာ ထုိက္တန္ေသာ ခ်ီးမြမ္းျခင္း ျဖစ္ေပသည္။","လူတို႔၏ စရိုက္သဘာ၀ကို လိုက္၍ က်င့္သံုးတက္လွ်င္ အားလံုးႏွင့္ သင့္ျမတ္ေပမည္။"]
    
    var engExpArray : [String] = ["The not-full water pot makes a rocking sound'', goes a saying. One should not give his love and kindness to a wicked person. The vicious one is not serene and steady; he may betray and do harm if he is not satisfied.","The wicked is more harmful than the snake. The snake can be subdued by drug and charm, but no drug can subdue the wicked who can always do harm.","A foolish man who knows his foolishness can be called wise because he will mend his ways and become a wise man. On the other hand, a foolish man who inspite of his foolishness thinks himself a wise man will not reform, and so will remains a real fool.","The fool thinks his evil deeds are good as honey when the consequences of his deeds have not yet come to him, but when they come he will becomes miserable.","The fool uses his physical strength to plunder other's property. When he dies his strength disappears and in his after-life he falls into hell.","In the days of old, the brahmin was usually a king's counsellor. The brahmin with an evilmind tired  to  set his enemy against the king by telling tales. In jattaka and tables, the brahmin has to take the role of a villain.","The distance or the duration is long or shoot in accordance with the attitude of the person concerned. The wakeful one finds the night too long (for he has to spend the night without sleep). To the tired traveller even a short distance seems too long (because he is too weak to walk any longer). The foolish one who has not the attributes of a virtuous person has to roll around the unending samara.","The person of evil mind sees other's fault however small, and fails to see, or ignores, his own however serious. It is the nature of evilminded persons. Such persons can enlarge other's faults and makes little of their own.","The wise man conceals his defects, or other will probably think ill about him, thus getting themselves involved in sin. However, he should try to discover other's faults and take note of them in order to learn a lesson about life."," A fool's praise of a wise man is a sort of punishment, because the fool has no ability to appraise the worth of a wise man. Only if a wise man praises another wise man, is a worthy praise."," All people could be properly treated if one acts according to their respective characteristic attitudes and behaviours."]
    

   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 20
        
        self.tableView.register(UINib(nibName: "text2Cell", bundle: nil), forCellReuseIdentifier: "text2Cell")
        
    }
    
    // number of cell
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return engArray.count
    }
    
    // cell for row id
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "text2Cell") as! text2Cell
        
        let tmp = UserDefaults.standard.integer(forKey: "CurrentLanguage")
        if tmp == 0{
            let indexTitle : String = mmNumArray[indexPath.row] + "/" + mmNumArray[mmArray.count - 1]
            let exp : String = expText[0]
            cell.usernameLbl.text = "\r" + indexTitle + "\r" + mmArray[indexPath.row] + "\r" + "***" + exp + "***" + "\r" + mmExpArray[indexPath.row] + "\r"
        }else{
            let indexTitle2 = String(indexPath.row + 1) + "/" + String(engArray.count)
            let exp : String = expText[1]
            cell.usernameLbl.text = "\r" + indexTitle2 + "\r" + engArray[indexPath.row] + "\r" + "***" + exp + "***" + "\r" + engExpArray[indexPath.row] + "\r"
        }
        
        return cell
        
    }
    
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! text2Cell
        
        // CANCEL ACTION
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        
        // create menu controller
        let menu = UIAlertController(title: "Menu", message: nil, preferredStyle: .actionSheet)
        
        let myanmar = UIAlertAction(title: "Myanmar", style: UIAlertActionStyle.default) { (UIAlertAction) -> Void in
            
            let indexTitle : String = self.mmNumArray[indexPath.row] + "/" + self.mmNumArray[self.mmArray.count - 1]
            let exp : String = self.expText[0]
            cell.usernameLbl.text = "\r" + indexTitle + "\r" + self.mmArray[indexPath.row] + "\r" + "***" + exp + "***" + "\r" + self.mmExpArray[indexPath.row] + "\r"
        }
        let english = UIAlertAction(title: "English", style: UIAlertActionStyle.default) { (UIAlertAction) -> Void in
            
            let indexTitle2 = String(indexPath.row + 1) + "/" + String(self.engArray.count)
            let exp : String = self.expText[1]
            cell.usernameLbl.text = "\r" + indexTitle2 + "\r" + self.engArray[indexPath.row] + "\r" + "***" + exp + "***" + "\r" + self.engExpArray[indexPath.row] + "\r"
        }
//        let facebook = UIAlertAction(title: "Facebook", style: UIAlertActionStyle.default) { (UIAlertAction) -> Void in
//            
//            if SLComposeViewController.isAvailable(forServiceType: SLServiceTypeFacebook){
//                let facebookSheet:SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
//                facebookSheet.setInitialText(cell.usernameLbl.text!)
//                self.present(facebookSheet, animated: true, completion: nil)
//            } else {
//                self.showAlert()
//            }
//        }
        
        
        //menu.addAction(facebook)
        menu.addAction(myanmar)
        menu.addAction(english)
        menu.addAction(cancel)
        
        // show menu
        self.present(menu, animated: true, completion: nil)
        
        
        
    }
    
    func showAlert(){
        let alert = UIAlertController(title: "Accounts", message: "Please login to a Facebook account to share.", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    

    
}
