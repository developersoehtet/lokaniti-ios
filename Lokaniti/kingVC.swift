//
//  kingVC.swift
//  Lokaniti
//
//  Created by Soe Htet on 6/11/16.
//  Copyright © 2016 Soe Htet. All rights reserved.
//

import UIKit
import Social

class kingVC: UITableViewController {
        
    var expText : [String] = ["ရွင္းခ်က္","Explanation"]
    
    var mmNumArray : [String] = ["၁","၂","၃","၄","၅","၆","၇","၈","၉","၁၀","၁၁","၁၂","၁၃","၁၄","၁၅","၁၆","၁၇","၁၈","၁၉","၂၀","၂၁","၂၂","၂၃","၂၄","၂၅","၂၆","၂၇","၂၈","၂၉","၃၀","၃၁","၃၂","၃၃","၃၄","၃၅","၃၆","၃၇","၃၈","၃၉","၄၀"]
    
    var mmArray : [String] = ["မင္းကား တစ္ယာမ္သာအိမ္ရာ၏။ ပညာရွိကား ႏွစ္ယာမ္သာလွ်င္ အိပ္ရာ၏။ အိပ္ရာ ေထာင္ေသာလူ အေပါင္းကား သံုးယာမ္သာ အိမ္ရာ၏။ ဖုန္းေတာင္း ယာစကားကား ေလးယာမ္ လံုးသာလွ်င္ အိပ္ရာ၏။",
        "အၾကင္အရပ္၌ ဥစၥာရွိေသာသူသည္လည္းေကာင္း၊ အၾကားမျမင္သူအား ပညာရွိသည္ လည္းေကာင္း၊ မင္းသည္လည္းေကာင္း၊ ျမစ္သည္လည္းေကာင္း၊ ထိုၿမဳိ႕ ေဆးသမားသည္လည္း ေကာင္း၊ ဤငါးပါးတို႔သည္ မရွိကုန္။ ထိုအရပ္၌ တစ္ေန႔တစ္ရက္မွ် မေနရာ။",
        "အၾကင္အရပ္၌ ျမတ္ႏိုးျခင္းလည္းမရွိ၊ ခ်စ္ျခင္းလည္းမရွိ၊ အေဆြခင္ပြန္းလည္း မရွိ။ တစ္စံု တစ္ေယာက္မွ် အတက္ပညာ သင္စိမ့္ေသာသူသည္ မရွိ၊ ထိုအရပ္၌ တစ္ေန႔တစ္ရက္မွ် မေနရာ။",
        "ေလာက၌ သားမရွိေသာ အိမ္သည္ ဆိတ္ၿငိမ္၏။ မင္းမရွိေသာ တိုင္းျပည္သည္ ဆိတ္ၿငိမ္ ၏။ အတက္မရွိေသာသူ၏ ခံတြင္းဆိတ္ၿငိမ္၏။ ဆင္းရဲျခင္းကား အလံုးစံု ဆိတ္ၿငိမ္၏။",
        "ေလာက၌ ဥစၥာကို အလိုရွိျငားအံ့။ ကုန္သြယ္ရာ၏။ အတက္ကို အလိုရွိမူကား အၾကားအျမင္ ရွိေသာ ပုဂိၢဳလ္ကို ဆည္းကပ္ရာ၏။ သားကိုအလိုရွိမူကား၊ မိန္မငယ္ကိုေနရာ၏။ မင္းမတ္ျဖစ္ျခင္းကို အလိ္ုရွိမူကားမင္း၏ အလိုသို႔လုိက္ရာ၏။",
        "ရဟန္းသည္ မေရာင့္ရဲမူ ပ်က္၏။ ေျမႀကီးနင္း ျပည့္ရွင္းမင္းသည္လည္း ေရာင့္ရဲတက္မူ လည္းပ်က္၏။ ျပည့္တန္ဆာမသည္ ရွက္တက္မူ ပ်က္၏။ အမ်ိဳးေကာင္းသမီးသည္ မရွက္တက္မူ ပ်က္၏။",
        "ငွက္တို႔၏အားကား ေကာင္းကင္တည္း။ ငါးတုိ႔၏အားကား ေရတည္း။ အားနည္းသူတို႔၏ အားကားမင္းတည္း။ သူငယ္တုိ႔၏အားကား ငိုျခင္းတည္း။",
        "သည္းခံျခင္းသည္လည္းေကာင္း၊ ႏိုးၾကားျခင္းသည္လည္းေကာင္း၊ ထၾကြလံုးခရွိျခင္းသည္ လည္းေကာင္း၊ ေ၀ဖန္ေပးကမ္းျခင္းသည္လည္းေကာင္း၊ သနားျခင္းလည္းေကာင္း၊ ေျမာ္ျမင္ျခင္း လည္းေကာင္း ဤေျခာက္ပါးတို႔ကား မင္းစေသာ လူႀကီးမိဘတို႔၏ အလိုရွိအပ္ကုန္ေသာ ဂုဏ္ေက်းဇူးတို႔တည္း။",
        "ေလာက၌ မင္းတို႔သည္ တစ္ႀကိမ္သာ ဆိုကုန္၏။ ရဟန္းတို႔သည္ တစ္ႀကိမ္သာဆိုကုန္၏။ ဘုရားအစရွိေသာ သူေတာ္ေကာင္းတို႔သည္ တစ္ႀကိမ္သာဆိုကုန္၏။ ဤသို႔ေသာ သေဘာတရား သည္ ေရွ႔သူေတာ္ေကာင္းတို႔ အက်င့္တည္း။",
        "ေလာက၌ ပ်င္းေသာ အိမ္ရာေထာင္ေသာ သူသည္မေကာင္း၊ ကိုယ္ႏႈတ္ႏွလံုး မေစာင့္ ေသာရဟန္းသည္ မေကာင္း၊ မဆင္မျခင္ျပဳေလ့ရွိေသာ မင္းသည္မေကာင္း၊ ပညာရွိသည္ အမ်က္ထြက္၏။ ထိုပညာရွိသည္လည္း မေကာင္း။",
        "အၾကင္အရပ္၌ အႀကီးအကဲတို႔သည္ မ်ားကုန္၏။ ခပ္သိမ္းေသာ သူတိုသည္ပညာရွိသာ ျပဳလိုကုန္၏။ ခပ္သိမ္းေသာ သူတို႔သည္ အႀကီးအျမတ္အျဖစ္ကို အလိုရွိကုန္၏။ ထိုသူတုိ႔၏ အမႈသည္ ပ်က္တက္၏။",
        "မင္းသည္ကား အခြန္အတုတ္၏ ထြက္မထြက္ကိုလည္းေကာင္း၊ မ်ိဳးရိကၡာကုန္မကုန္ကုိ လည္းေကာင္း၊ ကိုယ္တိုင္သိေအာင္ျပဳရာ၏။ အမႈထမ္းသည္ မထမ္းကိုသည္ကိုလည္း သိေအာင္ျပဳ ရာ၏။ ႏွိမ္ျခင္းငွာ ထိုက္ေသာသူကိုလည္းႏွိမ္ရာ၏။ ေျမွာက္ျခင္းငွာ ထိုက္ေသာသူကိုလည္း ေျမွာက္ ရာ၏။",
        "ေလာက၌ ေနမင္းကို ေက်ာျဖင္ကပ္ရာ၏။ မီးကို၀မ္းျဖင့္သာလွ်င္ ကပ္ရာ၏။ အရွင္သခင္ကို နည္းမ်ိဳးစံုျဖင့္ ကပ္ရာ၏။ တမလြန္ဘ၀ကိုကား မမိုက္သျဖင့္ ကပ္ရာ၏။",
        "ေလာက၌ မီးကုိလည္းေကာင္း၊ ေရကိုလည္းေကာင္း၊ မိန္းမကိုလည္းေကာင္း၊ လူမိုက္ကို လည္းေကာင္း၊ ေျမြကိုလည္းေကာင္း၊ မင္းမ်ိဳးတို႔ကို လည္းေကာင္း၊ ေရွာင္ရွားသျဖင့္ သြားအပ္၏။ လက္တေလာ အသက္ကိုခ်တက္ကုန္၏။",
        "ေလာက၌မေကာင္းေသာမယားႏွင့္ ေပါင္းဖက္သူသည္လည္းေကာင္း၊ ၾကမ္းတမ္းလွစြာ ေသာ စိတ္ရွိေသာကၽြန္ကိုေစေသာ သူသည္လည္းေကာင္း၊ ေျမြရွိေသာအိမ္၌ ေနေသာသူသည္ လည္းေကာင္း၊ ေသအံသည္သာလွ်င္ အမွန္တည္း။ ယံုမွားမရွိေပ။",
        "ေလာက၌ မိုက္မဲလွစြာေသာ တပည့္ကို အတက္ပညာ သင္ၾကားသျဖင့္ လည္းေကာင္း၊ မိန္းမယုတ္ျဖင့္ ေပါင္းဖက္သျဖင့္ လည္းေကာင္း၊ မသူေတာ္ႏွင့္ ေပါင္းဖက္သျဖင့္လည္းေကာင္း ပညာရိွေသာသူပင္ ျဖစ္ေသာ္ လည္း နစ္တက္ေခ်သာလွ်င္လည္း။",
        "ေလာက၌ ယုတ္မာေသာမႈကုိ သားတည္းျပဳေလမႈကား အမိျပဳသည္မည္၏။ ထုိ႔အတူ တပည္႔ တည္း မေကာင္းမႈျပဳမႈကား ဆရာျပဳသည္ မည္၏။ တုိင္းသူျပည္သား တည္းျပဴမူကား မင္းျပဳသည္ မည္၏။ မင္းတည္း မေကာင္းမႈျပဳမႈကား ပုေရာဟိတ္မေကာင္းမႈျပဳသည္ မည္၏။",
        "မင္းမည္သည္ကား အမ်က္မာန္ေခါင္ ႀကီးလွေသာသူကုိ အမ်က္နည္းသျဖင္႔ ေအာင္ရာ၏။ မေတာ္မွန္လွေသာသူကုိ မိမိက သူေတာ္ေကာင္းလုပ္သျဖင္႔ ေအာင္ရာ၏။ ၀န္တုိေသာသူကုိလည္း ေပးကမ္းျခင္းျဖင္႔ ေအာင္ရာ၏။ စကားမမွန္ေသာသူကုိလည္း စကားအမွန္ဆုိျခင္းျဖင္႔ ေအာင္ရာ၏။",
        "ေလာက၌ ေပးကမ္းျခင္းသည္ မယဥ္ေက်းေသာသူကုိ ယဥ္ေက်းေစတတ္၏။ ေပးကမ္းျခင္းသည္ ခပ္သိမ္းေသာအက်ုိးကုိ ၿပီးေစတတ္၏။ ေပးကမ္းျခင္းသည္ ခပ္သိမ္းေသာအက်ဴိးကုိ ၿပီးစတတ္၏။ ေပးကမ္းျခင္းေၾကာင္႔လည္းေကာင္း၊ ခ်စ္ဖြယ္ေသာစကား ေၾကာင္႔လည္းေကာင္း မိမိကလည္း သူ႔အေပၚ မုိေမာက္ႏုိင္သည္။ မိမိသုိ႔လည္း ရုိညႊတ္ကုန္၏။",
        "ေလာက၌ ေပးကမ္းျခင္းသည္ ခ်စ္ေစတတ္ေသာ ေဆးတည္း။ ၀န္တုိျခင္းကား မုန္းေစတတ္ေသာ ေဆးတည္း။ ေပးကမ္းျခင္းကား အေျခြအရံမ်ားေသာ ေဆးတည္း။ ၀န္တုိျခင္းကား အထီးက်န္ေသာ ေဆးတည္း။",
        "ေလာက၌ မ်ားစြာကုန္ေသာ ဂုဏ္ေက်းဇူးမွ ကင္းကုန္သူတုိ႔အား ညီညႊတ္သည္ရွိေသာ္ ေအာင္အပ္ေသာ ရန္ကုိ ေအာင္ႏုိင္၏။ ျမက္ျဖင္႔လြန္ကုိ က်စ္အပ္၏။ ထုိလြန္ျဖင္႔ ဆင္ေျပာင္ကုိလည္း ဖြဲ႔ႏုိင္သလွ်င္ကတည္း။",
        "ဗုိလ္ပါရဲမက္သဟဲ အေဖာ္ ျပည္႔စုံညီညႊတ္လ်က္လည္း ရန္ကုိ မေအာင္စြမ္းႏုိင္ေသာမင္းကား တန္ခုိးျဖင္႔ဘယ္ျပဳအံ႔နည္း။ ေလးေထာင္႔ရွိေသာအရပ္၌ ညွိအပ္ ေသာမီးသည္ အလုိလုိသာလွ်င္ ၿငိမ္းေခ်သလွ်င္ကတည္း။",
        "ကာမစည္းစိမ္ကုိ မင္းႏွင္႔အတူ တရံတစ္ဆစ္မွ် မခံစားရာ ။ အသြင္လည္းေကာင္း၊ အရသာ စားျခင္းကုိ လည္းေကာင္း၊ ပန္းပန္ျခင္း၊ နံသာထုံျခင္းကုိလည္းေကာင္း၊ ပုဆုိးအ၀တ္ကုိလည္းေကာင္း၊ တန္ဆာဆင္ျခင္းကု္လည္း အလုံးစုံကုိိ မင္းႏွင္႔အတူ မျပဳရာ။",
        "မင္းကား ငါ၏ခင္ပြန္းသည္ မျဖစ္။ မင္းကားဂုဏ္ရည္တူေသာ အေပါင္းအေဖာ္သည္ မျဖစ္၊ ဤမင္းကားင္၏ ငါ၏အရွင္တည္း၊ ဤသုိ႔စိတ္၌ အၿပီးေကာင္းစြာ ထားရာ၏။",
        "မင္းခေယာက်ာ္းသည္ မင္းအား အလြန္ေ၀းေသာအရပ္၌ မခစားရာ။ အလြန္နီးသည္၌ မခစားရာ။ ေလညာ၌ မခစားရာ။ အလြန္နိမ္႔ေသာ အရပ္၌လည္း မခစားရာ။ ဤသုိ႔ ေျခာက္ပါးေသာ အျပစ္တုိ႔ကုိ ၾကည္႔ရာ၏။ မီးကဲ႔သုိ႔ေစာင္႔စည္းလ်က္ တည္ရာ၏။",
        "ျမတ္စြာဘုရားႏွင့္ တူေသာေက်းဇူးရွိေသာ သူျဖစ္လင့္ကစားမင္းတည္းဟူေသာ မွီရာမရ မတင့္တယ္။ ပတၱျမားသည္ အဖိုးအတိုင္းအသိ ထိုက္ေသာ္လည္း ေရႊကိုမွီမွသာလွ်င္ တင့္တယ္၏။"]
    
    var engArray : [String] = ["A king should sleep only one watch of the night, a sage two watches, a family man three, and a beggar all the four.",
        "Do not live in a place where there is no rich man, no sage, no king, no river, and no physician. If these five are absent, one should not reside there even for a day.",
        "One should not live in a place where there is no element of respect or love, no relative or no teacher. One should not reside there even for a day.",
        "In this world, the house in which there are no children is dreary. The country without a king is desolate. The man of no learning has his lips sealed tight. Destitution makes for absence of all things.",
        "In this world, if one wants to make money, he should do trading. If one wants to acquire learning, he should attend to a man of learning. If one wants to have a child, he should marry a maiden. If one wants to become a king’s minister, he should serve him to his satisfaction.",
        "If a monk is not contented, he will go to ruin. If a king is contented, he will go to ruin. If a prostitute is shy, she will fail in her trade. If a maiden of good family is unashamed, she will become cheap and her life will be ruined.",
        "The source of strength for birds is the sky. The source of strength for fishes is the water. The source of strength for the weak is the king. The source of strength for infants is crying.",
        "Forbearance, vigilance, industry, charity, kindness and foresight, these six are the attributes of the king, elders and parents.",
        "In this world, kings speak but once, so do monks and Brahmins, so do the virtuous headed by Buddha. This is the practice of ancient virtuous persons.",
        "In this world the family man who is lazy is not good. The monk who does not control his speech and acts is not good. The king who does impulsively is not good. The sage who is angry is not good.",
        "In a certain place where there are many chiefs and leaders, where many persons want to acts as sages, where many persons want to be chiefs and nobles, all their endeavors will end in failure.",
        "A king should personally inspect the revenue as well as expenditure. He should personally inspect the work of government servants. He should punish those who deserve punishment and honor those who deserve to be honored.",
        "One should approach the sun with his back towards it, the fire with his stomach, the master by all means, and the next life by not falling into folly.",
        "In this world one should shun fire, water, woman, fool, snake and the royal family. They may harm and kill him instantly.",
        "The person who couples with a bad wife, or who employs a rude servant, or who lives in a house where there is a snake, will surely die is no doubt about it.",
        "In this world, teaching a foolish pupil, living with a wicked woman, associating with an unvirtuous person, these are the factors that could cause harm, including death, even to a wise man.",
        "In this world, if the son committed wicked acts, it means the mother did it. So also, if the pupil committed a sin, it means the teacher did it. If the people of a country have done something wrong, it means the king did it. If the king committed a sin, it means the king’s counselor did it.",
        "The king should win over the person who is in great anger by showing no anger. The dishonest persons should be won over by honesty. The stingy person should be won over by generosity. The liar must be quelled by telling the truth.",
        "Charity makes the untamed one tame. It achieves all objectives. Charity and sweet words place one above others, and they will bow to him in deep respect.",
        "In this world, charity is live potion. Miserliness is poison of hate. Charity attracts many attendants. Niggardliness leaves the miser alone.",
        "In this world if many persons who have no worth of their own are united, they can conquer what is to be conquered. A rope can be used to tie up a huge elephant.",
        "How will the king who cannot conquer the enemy although his troops are united? A fire lit in a windy place will die by itself.",
        "One should not enjoy sensual pleasures, or assume the appearance, or take the delicious food, or use flowers or toiler, or wear dress or adorn one self in the same manner as the king.",
        "The king is not my friend, nor is he my companion with equal status. He is my lord. Keep this in mind forever.",
        "A servant of the king should not wait upon the king at a great distance, or too near, or directly in front, or the direction of the wind, or in a place too high or too low. He should avoid these six faults, in the way as one who keeps away from the fire.",
        "Even a person having the qualities of Buddha is not in a position of advantage if not under the patronage of the king. The ruby, though priceless, is not becoming if it is not set in gold."]
    
    var mmExpArray : [String] = ["ညကာလကို အခ်ိန္ကာလ သံုးပါးပိုင္းျခားသည္ဟု သိၾကသည္။ ဗုဒၶစာေပေတြမွာ ညဥ့္သံုးယံ ဟုဆိုသည္ဆိုသည္။ နီတိဆရာက ညဥ့္ေလးယံဟု ဆိုသည္။ ညဥ့္သံုးယံ အပိုင္းအျခား မွာ ေျခာက္နာရီမွ ဆယ္နာရီထိ ပထမယံ။ ဆယ္နာရီမွ ႏွစ္နာရီထိ ဒုတိယယံ၊ ႏွစ္နာရီမွ မိုးလင္း ေျခာက္နာရီအထိ တတိယယံျဖစ္သည္။ နီတိဆရာအလိုအားျဖင့္ ပထမယံေျခာက္နာရီမွ ကိုးနာရီ၊ ဒုတိယယံ ကိုးနာရီမွ ဆယ့္ႏွစ္နာရီ၊ တတိယယံ ဆယ့္ႏွစ္နာရီမွ သံုးနာရီ၊ စတုတၳယံ သံုးနာရီ မွ နံနက္ေျခာက္နာရီဟူ၍ပင္။ ဘုရင့္မွာ အိပ္ခ်ိန္အနည္းဆံုးျဖစ္သည့္အေၾကာင္းမွာ သူမွာ တိုင္းျပည္ အုပ္ခ်ဳပ္သူ ျဖစ္သျဖင့္ တာ၀န္၀တၳရား မ်ားလွေခ်သည္။ အလုပ္မ်ားမ်ားလုပ္ရ၍ အိပ္နည္းသည္ဟု ဆုိၾက သည္။ အခ်ိဳ႕က ယူဆၾကသည္မွာ ဘုရင့္မွာ အႏၱရာယ္ႀကီးစြာရွိသည္။ အိပ္ေနခိုက္ လုပ္ႀကံခံရမွာ စိုးရသည္။ ထို႔ေၾကာင့္ တစ္ညမွာ သုံးနာရီ ေလးနာရီေလာက္ သူမ်ားေတြ မအိပ္ေသးခ်ိန္မွာ အိပ္ရသည္။ ထို႔ေနာက္ႏိုးႏိုးၾကားၾကာေနမွ လုပ္ႀကံရန္လာသူတို႔ကို ခုခံကာကြယ္ႏိုင္မည္ဟူ၍ပင္။ ပညာရွိမွာ ညတစ္၀က္ အခ်ိန္ေပး၍ အိပ္ႏိုင္သည္။ က်န္အခ်ိန္တြင္ တရားအား ထုတ္မွဳျပဳ၍ ျပႆနာ ပုစာၦတို႔ကိုေျဖရွင္းႏိုင္ရန္ ႀကံစည္ေတြးေခၚ ေနသင့္ဟု ဆိုလိုသည္။ အိမ္ ေထာင္သည္က ညဥ့္သံုးယံအိပ္ႏိုင္သည္။ နံနက္မွာ ေစာေစာထ၍ အိမ္မႈေရးရာ ကိစၥမ်ားကို ေဆာင္ ရြက္ရေပမည္။ နံနက္ေစာေစာအိပ္ရာမွထၿပီလွ်င္ ဘုရားရွိခိုးျခင္း၊ တရားထိုင္ျခင္း၊ ေန႔ကာလာမွာ လုပ္ရမည္ အလုပ္အတြက္ ျပင္ဆင္ျခင္း၊ လုပ္ရေပမည္။ သူေတာင္းစားအဖို႔ တစ္ညလံုး အိပ္ႏိုင္ သည္။ သူ႔မွာ မည္သည့္အလုပ္တာ၀န္မွ် မရွိေခ်။",
        "ေဖာ္ျပထားေသာ ငါးမ်ိဳးမရွိလွ်င္ လူမူေရး၊ ပညာေရး၊ စီးပါြးေရး၊ က်မ္းမာေရးတို႔ ခ်ိဳ႕တဲ့မည္ျဖစ္သျဖင့္ ထိုငါးပါးမရွိသာအရပ္တြင္ တစ္ေန႔တစ္ရက္မွ် မေနအပ္ဟု ဆိုလိုသည္။",
        "မေနအပ္သည္အရပ္၌ လကၡဏာ မ်ားကို ဆက္လက္ေဖာ္ျပျခင္း ျဖစ္သည္။",
        "ဆိတ္ၿငိမ္ျခင္းဆိုသည္မွာ ဆိတ္သုဥ္းျခင္းကိုညႊန္းသည္။ ဘုရင္မရွိေသာ တိုင္းျပည္ကို ဆိတ္ၿငိမ္ျခင္းဟု ဆိုျခင္းသည္ အထင္မွားစရာျဖစ္၏။ နီတိဆရာ၏ ဆိုလိုခ်က္မွာ ေကာင္းေသာလကၡဏာမ်ား ဆိတ္သုဥ္းသည္ဟူ၏။",
        "ဘုရင္အမတ္ျဖစ္ျခင္လွ်င္ မင္းလိုလိုက္ရမည္ဆိုေသာစကားသည္ ေရွးစကား ျဖစ္ေသာ္လည္း ယခုထက္တိုင္ မွန္ေနေသးသည္။ ေနာင္လည္းထိုအတိုင္းပင္ ျဖစ္မည္ဟု ယံုၾကည္ ရသည္။",
        "လာဘ္လာဘကိုမက္ေသာ ရဟန္းသည္ သိကၡာပ်က္တက္သည္။ ေရာင့္ရဲျခင္း ရွိေသာမင္းသည္ တိုင္းျပည္တိုးတက္ေရး၌ အားသြန္ခြန္စိုက္မရွိသျဖင့္ တိုင္းသူျပည္းသားတို႔ အက်ိဳးေက်းဇူးမရဘဲ ရွိမည္။ ေရာင့္ရဲျခင္း၊ သည္းခံျခင္းမ်ားလြန္းလွ်င္ ၾသဇာအာဏာနည္းၿပီးလွ်င္ ပ်က္စီးတက္သည္။",
        "အားသည္ အားကိုစရာကို ဆိုလိုသည္။ ငွက္သည္ ေကာင္းကင္ကို အားကိုသည္။ လြတ္လပ္စြာ ပ်ံႏုိင္သည္။ ရန္သူေတြ႔လွ်င္ ပ်ံတက္သြားႏိုင္သည္။ ငါးတုိ႔သည္ ေရကိုအားကိုးၾက သည္။ ေရနည္းလွ်င္း ငါးအဖို႔အားနည္းသြားရသည္။ အားနည္းသူတို႔ အားကိုးစရာမွာ မင္းျဖစ္သည္။ မင္းျပဌာန္းထားေသာ ဥပေဒမ်ားက အကာအကြယ္ေပးၾကသည္။ ကေလးငယ္တို႔သည္ ငိုလွ်င္ ႏို႔စို႔ရသည္။",
        "မင္းႏွင့္လူႀကီးမ်ားသည္ အျပစ္မ်ားကို တက္ႏိုင္သေလာက္ သည္းခံခြင့္လႊတ္အပ္ ၏။ လုပ္ကိုင္ေဆာင္ရြက္မႈမ်ားတြင္ သတိၿမဲ၍ လံုလထုတ္ရန္လိုအပ္၏။ ေပးကမ္းစြန္႔ႀကဲမႈလည္း ျပဳ၏။ သနားစာနာျခင္း ရွိအပ္ေပသည္။ ကိစၥအ၀၀တို႔၌ ႀကိဳတင္ေျမာ္ျမင္မႈရွိမွသာ လုပ္ေဆာင္မႈ တို႔ ေအာင္ျမင္ၿပီးစီးေပလိမ့္မည္။ ယင္းေျခာက္ပါးတို႔သည္ မင္းႏွင့္တကြ သူႀကီးသူမတို႔ (ေခါင္းေဆာင္ တို႔)၏ လိုအပ္ေသာဂုဏ္ေက်းဇူးမ်ား ျဖစ္သည္။",
        "မင္း၊ ရဟန္း၊ ပုဏၰားမ်ား ဘုရားအစ ရွိေသာသူေတာ္ေကာင္းမ်ားသည္ ေျပာစရာ ရွိလွ်င္ တစ္ခါမွ်သာေျပာသည္။ ေျပာစကားကို ဘယ္ေတာ့မွ် မျပင္ေတာ့။ သူတုိ႔သည္ အေၾကာင္း မဲ့ မဆိုၾကေပ။ ဆိုလွ်င္လည္း ခိုင္ၿမဲသည္တည္။",
        "ပ်င္းေသာ အိမ္ေထာင္သည္သည္စီးပြားမျဖစ္ေသာေၾကာင့္ မေကာင္းေခ်။ သိကၡာ မေစာင့္ဘဲ ေျပာဆိုျပဳလုပ္ေသာရဟန္းသည္ မေကာင္းေခ်။ မဆင္မျခင္ဘဲ လုပ္ခ်င္ရာလုပ္ေသာ မင္းသည္လည္းမေကာင္းေခ်။ ထင္ရာေလွ်ာက္လုပ္ေသာ မင္းသည္ အႏၱရာယ္ျဖစ္၏။ မင္းသည္ တိုင္းျပည္အုပ္ခ်ဳပ္ေရးရာ၌ မူးမတ္တို႔၊ ပညာရွိတို႔ႏွင့္ တိုင္ပင္ၿပီးမွ ေဆာင္ရြက္သင့္ေပသည္။ သို႔မဟုတ္လွ်င္ တိုင္းျပည္ႏွင့္ လူမ်ိဳးတို႔ ဆိုးက်ိဳးအေထြေထြ ရၾကေခ်သည္။",
        "အႀကီးအကဲေတြ မ်ားေသာေနရာ၊ အားလံုးကသိသည္၊ တက္သည္ဆုိၿပီး ပညာရွိ လုပ္ခ်င္ေနသည့္ေနရာ၊ ထိုေနရာတြင္ ထိုသူတို႔အားလံုး၏ ျပဳမူတို႔သည္ ပ်က္စီးသည္သာ ျဖစ္ေခ် မည္။ (ပရမ္းပတာ ျဖစ္ကုန္ၾကေခ်သည္)။",
        "ဘုရင္သည္ ႏိုင္ငံ၏ အခြန္ဘ႑ာႏွင့္ အသံုးစရိတ္တို႔ကို ကိုယ္တိုင္ စစ္ေဆးရေပ မည္။ အမႈထမ္းမ်ား တာ၀န္ေက်ပြန္မႈကိုလည္း စီစစ္ရေပမည္။ အျပစ္ေပးထိုက္သူကို အျပစ္ေပး၍ ခ်ီးျမွင့္ထိုက္သူကို ခ်ီးျမွင့္ရေပမည္။ သို႔မွသာလွ်င္ ႏိုင္ငံေတာ္ဖြံ႔ၿဖိဳးတိုးတက္ေပမည္။",
        "ေနကို ေက်ာေပးရမည္။ မီးလံွဴရာတြင္ မ်က္ႏွာခ်င္းဆိုင္ရမည္။ အရွင္သခင္ကို နည္းအမ်ိဳးမ်ိဳးျဖင့္ ကပ္ရမည္။ ေနာင္တမလြန္ဘ၀ကို မမိုက္ဘဲ တရားက်င့္ႀကံအားထုတ္ျခင္းျဖင့္ ရင္ဆိုင္ရေပမည္။",
        "မီးကို ေရွာင္သင့္သည္။ မီးကိုမေရွာင္လွ်င္ ေလာင္တက္သည္။ ေရကိုေရွာင္သင့္၊ မေရွာင္လွ်င္ ေရနစ္တက္သည္။ မိန္းမယုတ္ကိုေရွာင္ သင့္သည္။ မေရွာင္လွ်င္ ဒုကၡေရာက္ႏိုင္ သည္ ။ လူမိုက္ကိုေရွာင္မွ သာလွ်င္ သူ၏ မိုက္မဲမႈ၏ ဆိုးက်ိဳးမွ လြတ္ကင္းမည္။ ေျမြကိုသတိထား ၍ေရွာင္ရမည္။ မင္းမ်ိဳးတို႔သည္ မင္းအားကိုးႏွင့္ စိတ္မလိုလွ်င္ ဒုကၡေပးတက္သည္။ ေရွာင္ႏိုင္သမွ် လြတ္ကင္းေအာင္ ေနသင့္သည္။ ယင္းတုိ႔သည္ မိမိအား ေသေစသည္အထိအႏၱရာယ္ျပဳႏိုင္သည္။",
        "မေကာင္းေသာမယားဟူသည္ သစၥာေဖာက္ေသာမိန္းမ ျဖစ္သည္။ လင္သားကို အမ်ိဳးမ်ိဳးႏွိပ္စက္မည္။ ထိုသို႔ေသာ မိန္းမႏွင့္ ေပါင္းဖက္ေနသူသည္ ဒုကၡေရာက္လိမ့္မည္။ မိုက္ကန္းေသာ အေစခံကို ခုိင္းေစေသာသူသည္ အေစခံက အတိုက္အခံျပဳျခင္း ခံရမည္သာျဖစ္၏။ အက်ိဳးပ်က္ေစႏိုင္သည္။ ထိုအေစခံမ်ိဳးကို ဆက္လက္ထားလွ်င္ ဒုကၡေရာက္လိမ့္မည္။ ေျမြရွိေသာ အိမ္မွာေနလွ်င္ ေျမြကိုက္ခံရမည္။",
        "ေပါင္းသင္းမႈသည္ လူ႔ဘ၀တါင္ အလြန္အေရးပါသည္။ အေပါင္းအသင္းမေကာင္း လွ်င္ မိမိက မည္မွ်ပင္ေကာင္းေစဦး၊ အႏၱရာယ္ ျဖစ္တက္သည္။ အဆင္ျခင္ရွိေသာ ပညာရွိပင္လွ်င္ ပ်က္စီးတက္သည္။",
        "သားသမီးမေကာင္း မိဘေခါင္း၊ တပည္႔မေကာင္း ဆရာ႔ေခါင္း၊ ျပည္သူမေကာင္း ရာဇာေခါင္းဟူေသာ စကားမ်ဴိးကဲ႔သုိ႔ပင္။ ငယ္သူ၏ အျပစ္ကုိ ႀကီးသူ ( အႀကီးအကဲ) က တာ၀န္ခံရသည္။",
        "ေဒါသအလြန္ႀကီးသူကုိ ေဒါသမရွိဘဲဆက္ဆံလွ်င္ ေအာင္ျမင္သည္။ မင္းတုိ႔သည္ေဒါသႀကီးလွ်င္ မေတာ္တဆ အမွားလုပ္မိမည္။ ယင္းသုိ႔ လုပ္မီလွ်င္ ဒုကၡမ်ားတတ္သည္။ ႏုိင္ငံျခား ဆက္ဆံမႈမွာလည္း ေဒါသဆႀကီး ျပဴမူလုိက္လွ်င္ ႏွစ္ျပည္ေထာင္ စစ္မက္ျဖစ္ႏုိင္သည္။ မင္းေကာင္းမင္းျမတ္တုိ႔သည္ သည္းခံျခင္းတရားကုိ အၿမဲထားရွိရမည္။ မေတာ္မွန္မေျဖာင္႔မတ္သူကုိ မိမိက သူေတာ္ေကာင္းတရားအတုိင္း ေတာ္မွန္ေျဖာင္႔မတ္စြာ ဆက္ဆံလွ်င္ ေအာင္ျမင္မည္။ ႏွေျမာကပ္ၿငိသူကုိ ရက္ေရာျခင္းျဖင္႔ ေအာင္ျမင္ႏုိင္သည္။ အခ်ဴပ္ဆုိေသာ္ မေကာင္းမႈတုိ႔ကုိ ေကာင္းမႈတုိ႔ကုိ ေကာင္းမႈျဖင္႔ ေအာင္ႏုိင္ရာ၏။",
        "ေပးကမ္းစြန္႔ႀကဲျခင္း၌ တန္ခုိးႀကီးစြာရွိ၏။ ယင္းသည္ ရုိင္းပ်သူအား ယဥ္ေက်းေစသည္။ ခပ္သိမ္းေသာကိစၥတုိ႔ကုိ ၿပီးေစႏုိင္သည္။ ေပးကမ္းစြန္႔ႀကဲမႈႏွင္ပ ခ်ဳိေသာစကားတုိ႔ျဖင္႔ အားလုံးေသာ သူတုိ႔ကုိ ေအာင္ႏုိင္သည္။ သူတုိ႔၏ ရုိေသေလးစား မႈတုိ႔ကုိလည္း ရႏုိင္သည္။",
        "ေပးကမ္းစြန္႔ႀကဲျခင္းသည္ ပီယေဆးျဖစ္၏။ ႏွေျမာကပ္ၿငိျခင္းသည္ အမုန္းေဆး ျဖစ္၏။ ေပးကမ္းစြန္႔ႀကဲလွ်င္ အေျခြအရံေပါသည္။ ႏွေျမာကပ္ၿငိလွ်င္ အေျခြအရံမဲ႔ၿပီး တစ္ ကုိယ္တည္း ေနရေခ်မည္။ ေျခြမွရံသည္ဟု ဆုိ၏။",
        "ညီညႊတ္ျခင္းသည္ အင္အားဟူေသာ ဆုိရုိးစကားကုိ ေဖာ္ျပျခင္းျဖစ္သည္။ တစ္ဦးခ်င္းအားျဖင္႔ ဂုဏ္သတၱိမရွိေသာ သူမ်ားစြာကုိ စုစည္းလွ်င္ ခဲခက္ေသာကိစၥမ်ားကုိပင္ ပူးေပါင္းေဆာင္ရြက္ျခင္းျဖင္႔ ျပဳလုပ္ထားေသာႀကဳိးသည္ ဆင္ေျပာင္ႀကီးကုိ ထိန္းခ်ဴပ္ႏုိင္ ေလာက္ေအာင္ ခုိင္သည္။",
        "ညီညႊတ္ေသာ အင္အားစု ရရွိပါလွ်က္ႏွင္႔ ရန္သူကုိ မေအာင္ႏုိင္ေသာ မင္းသည္ ေခါင္းေဆာင္မႈ ညံ႔၏။ ေလထန္ေသာ အရပ္၌ မီးညွိ၍ မရႏုိင္သကဲ႔သုိ႔ ေခါင္ေဆာင္မႈညံလွ်င္ ေအာင္ျမင္မႈ မရႏုိင္။ ေနာက္လုိက္တုိ႔သည္ အစုအေ၀းအျဖစ္ အေရးပါသည္ႏွင္႔ အမွ် ေခါင္းေဆာင္သူ၏ စြမ္းရည္လည္း အေရးပါလွ၏။ ပုိ၍ပင္ ပါေပသည္။",
        "ဘုရင္ကုိတုၿပီး အေနအထုိင္ အျပဳအမႈတုိ႔ကုိ မျပဳသင္႔ ။ ျပဳလွ်င္ မင္းကသူ႕ကုိ ၿပဳိင္ရေကာင္းလားသေဘာျဖင္႔ အမ်က္ထြက္ကာ အျပစ္ဒဏ္ေပးလိမ္႔မည္။ ဤ၀ါဒသည္ ေရွးခတ္ႏွင္႔ဆုိင္သည္။ ယခုေခတ္သည္ ဘုရင္ေခတ္ မဟုတ္ေတာ႔ေပ။ ရွိေနေသာ ဘုရင္သည္လည္းအမည္ခံတုိ႔သာ ျဖစ္၏။ သုိ႔ေသာ္ဤစကား၏ ရသသည္ ယခုတုိင္ ရွိေနေသးသည္ ။ မတူလွ်င္ မတုႏွင္႔၊။ တုလွ်င္ဒုကၡ ေရာက္ႏုိင္သည္ဟူေသာ ၾသ၀ါဒ ျဖစ္သည္။ မတူဘဲႏွင္႔တုလွ်င္ တုသူမွာသာ နစ္နာဖြယ္သာရွိသည္။ မတူသည္မွာ တူလာမည္မဟုတ္။ မိမိ၏ဘ၀က ခင္းေပးထားေသာလမ္းကုိေလွ်ာက္လွ်င္ မိမိအဖုိ႔ သတ္မွတ္ထားေသာပန္းတုိင္သုိ႔ ေရာက္ေပလိမ္႔မည္။",
        "ဘုရင္ကုိ မိတ္ေဆြဟု မမွတ္သင္႔၊ ဂုဏ္ရည္တူအေပါင္းအေဖာ္ဟူ၍လည္း မမွတ္သင္႔။ ယင္းသုိ႔မွတ္ထင္ၿပီး အေရာ၀င္လွ်င္ ဘုရင္က သူကုိအရုိအေသတန္သည္ဟု ထင္လိမ္႕မည္။ သူ႕ကု္အရွင္သခင္ဟူ၍သာ အၿမဲမွတ္သင္႔သည္။",
        "မင္းကိုခစားရာ၌ အထူးသတိထားရမည္။ သင့္ေတာ္ေသာေနရာမွ ခစားရမည္။ သုိ႔မဟုတ္လွ်င္ အျပစ္ျဖစ္ႏိုင္သည္။ မင္းခေယာက္်ား ကမ္းနားသစ္ပင္ဆိုေသာ စကားရွိသည္။ အခါ မလင့္ မင္းျပစ္မင္းဒဏ္ ခံရတက္သည္။",
        "ဘုရင္၏ ဘုန္းတန္းခိုးသည္ ႀကီးလွ၏။ တစ္ေယာက္ေသာသူ၌ ျမတ္စြာဘုရားကဲ့ သို႔ ဂုဏ္ေက်းဇူး ရွိေနေစကာမူ ဘုရင့္ေက်းကၽြန္ ျဖစ္သည့္အေလ်ာက္ ဘုရင့္၏အရိပ္ေအာက္တြင္ မရွိလွ်င္ မတင့္တယ္။ ပတၱျမားသည္ အဖိုးမည္မွ် တန္ေစကာမူလည္း ေရႊမကြပ္လွ်င္ မတင့္တယ္။ ဤအဆိုသည္ ဘုရင့္ကို အႀကီးအက်ယ္ ေျမွာက္စားထားေသာ အဆိုျဖစ္သည္။ နီတိဆရာက သူ႔စာကို ဘုရင့္ကို ဦးစြာဆက္ရမည္ျဖစ္သျဖင့္ မင္းအႀကိဳက္ဆိုလိုက္ျခင္း ျဖစ္သည္ထင္ရသည္။"]
    
    var engExpArray : [String] = ["The duration of a night is divided into three, or four, period. In Buddhist literature three periods are mentioned. First period, 6p.m to 10p.m, second, 10 t0 2a.m to 6a.m.According to the duration of the night is in four periods, each lasting three hours beginning with 6p.m. The king is required to sleep only three or four hours of early night. That is probably because as the ruler of a country has many duties to perform, so during the first period. Another view is that the king has to be wakeful after a three or four hours sleep so that he would be on the alert to defend himself from would be assassins. The sage should sleep half the night and work out problems and questions in the second half. The family man may enjoy sleep for the first three period of the night. The beggar has nothing to do, so he can sleep the whole night.",
        "This is meant to say if these five are absent, social, educational, economic, health would be defective. So one should not live in such a place even for a day.",
        "This is a continuation of the description of the place in which one should not reside.",
        "“Silence” in this context means destitution. The country without a king is not quiet. There is a misunderstanding in this matter.The Noti master means to say that “Silence” is absence of good things.",
        "The statement that if one wants to be appointed minister by the king, he should fulfill the king’s wishes is an old saying but we believe that it remains true even to this day and will remain so in future.",
        "The monk who hankers after property, fails to observe the Law prescribed for the monks. The king who lives in contentment and does not work hard, and people will not get the benefit of prosperity. If a king is over-contented, he will lack influence and power, and so he will be ruined.",
        "“Strength” in this context is source of strength or something to depend upon. Birds have the sky in which to fly freely or fly up when they meet with enemies. Fishes depend on water. The fish in low water is helpless. The weak look up to the king who prescribes laws for their protection. Infants get milk when they cry.",
        "The king and elders should have patience and should pardon the faults as far as possible. They should be alert and work hard on all affairs. When there is an occasion for distribution of favors, they should be liberal. For solving problems they should have foresight so that their endeavor gains success. These are the six attributes of the king and elders (or leaders).",
        "Kings, monks and Brahmins speak only once in any case. So do the Buddha and the virtuous. They never alter their words. They do not speak for no reason. They seldom make remarks or pass judgment, but when they do make a statement, their word is firm.",
        "The lazy householder, being lazy, will not be able to bring prosperity to his family. The monk who should observe the rules of the Holy Order but acts without restraint is not good. The king who acts impulsively is not good. The king in ruling the country should take necessary action only after consultation with ministers, generals and wise men. Otherwise, the country and the people will suffer from his misdeeds.",
        "In a certain place there are many chiefs and leaders, where many want to pose as wise men, many aspire to become leaders, all their attempts will fail.",
        "The king should personally examine the state revenue and expenditure. He should review the work of state officials and punish those who fail in their duty and who are corrupt, and promote those whose work is commendable. Then only will the kingdom prosper.",
        "take the sun’s rays on one’s back. Sit at fire-side with one’s front to the fire. Approach the master in many ways. In order to gain a better position in the next existence by doing good according to Dhamma, and not resorting to folly.",
        "Avoid fire, or one may be burnt. Avoid water, or one may be drowned. Avoid bad women, or one would get into trouble. The fool should be shunned, or one might suffer from the effect of his folly. Snake should be avoided, for snakebite is fatal. Members of the royal family are likely to give trouble if they are displeased. One should avoid them as far as possible. They all may harm or even kill one who does not shun them.",
        "A bad wife is an unfaithful woman. She will give her husband trouble in many ways and he will suffer from staying with her. The rude and foolish servant will oppose his master and may cause ruin if the master continues to employ him. If one lives in a house where there is a snake, he will die from snakebite.",
        "Association with persons is an important factor in human life. However good one may be, association with bad persons could cause injury. Even a wise man who usually thinks be fore taking action is likely to go to ruin.",
        "The folly of children fails in the parent’s head; the teacher is responsible for his pupil’s wrong acts, the king is held responsible for the wrongs committed by the people of his country. The chief or leader is responsible for the follower’s acts.",
        "If a person is very angry one can get the better of him by showing no anger. If one is in great anger one may do wrong by chance. In the relations between two countries, if a king is in great anger, there could be war. Good kings should have patience and forbearance. One should teat dishonest over by liberal acts. The liar can be won over by speaking the truth.",
        "Charity has great power. It can make the rude person civil. It can achieve all things. Charity together with cordiality will make one highly respected.",
        "Charity is love potion. Stinginess is a hate poison. IF one gives something (money or things), many will be willing to serve him. A miser has no attendant, and so he has to remain a lonely person.",
        "This is the description of the saying, “Union is strength.” Individually worthless persons can be united and with their united effort they can achieve many difficulties. A rope made of grass can curb a large elephant; the rope is so strong.",
        "The king who cannot win the war although he has a united army. A fire cannot be successfully lit in a windy place. So also, poor leadership will not achieve anything. A united body of followers is clearly important, but able leadership is equally, or even more, important.",
        "One should imitate the ways of the king. He may be offended, and the imitator will get punishment. This advice is for ancient times. At present, the age of kings ha passed and the few monarchs are kings just in name. But the essence of that advice remains. Do not imitate if one is not equal to the other, or one would get into trouble. That is what the advice means. The imitator will never be the same as the imitated. If one walks along the path paved by one’s destiny, he will eventually reach the destination prescribed for him by his kamma.",
        "The king is not one’s friend or one’s equal. He is one’s lord. If one mistakes the king as friend or equal, the king will think that one is taking advantage. One should always regard him as one’s lord.",
        "In waiting upon the king, the king’s servant should very careful. Or he could in fault. There is a saying to the effect that a king’s servant is like a tree on the edge of the bank of a river. It means that he could get punishment at any time, just as the tree could be uprooted and falls into the river.",
        "The king’s power is great. Even if one has the attributes of Buddha (even if he is so exalted), he is just a subject of the king. Unless he under the patronage of the king, he is not at an advantage. We think that the NIti master, who was obliged to submit his treatise to the king for approval and patronage, tries to please the king."]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 20
        
        self.tableView.register(UINib(nibName: "text2Cell", bundle: nil), forCellReuseIdentifier: "text2Cell")
        
    }
    
    // number of cell
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return engArray.count
    }
    
    // cell for row id
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "text2Cell") as! text2Cell
        
        let tmp = UserDefaults.standard.integer(forKey: "CurrentLanguage")
        if tmp == 0{
            let indexTitle : String = mmNumArray[indexPath.row] + "/" + mmNumArray[mmArray.count - 1]
            let exp : String = expText[0]
            cell.usernameLbl.text = "\r" + indexTitle + "\r" + mmArray[indexPath.row] + "\r" + "***" + exp + "***" + "\r" + mmExpArray[indexPath.row] + "\r"
        }else{
            let indexTitle2 = String(indexPath.row + 1) + "/" + String(engArray.count)
            let exp : String = expText[1]
            cell.usernameLbl.text = "\r" + indexTitle2 + "\r" + engArray[indexPath.row] + "\r" + "***" + exp + "***" + "\r" + engExpArray[indexPath.row] + "\r"
        }
        
        return cell
        
    }
    
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! text2Cell
        
        // CANCEL ACTION
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        
        // create menu controller
        let menu = UIAlertController(title: "Menu", message: nil, preferredStyle: .actionSheet)
        
        let myanmar = UIAlertAction(title: "Myanmar", style: UIAlertActionStyle.default) { (UIAlertAction) -> Void in
            
            let indexTitle : String = self.mmNumArray[indexPath.row] + "/" + self.mmNumArray[self.mmArray.count - 1]
            let exp : String = self.expText[0]
            cell.usernameLbl.text = "\r" + indexTitle + "\r" + self.mmArray[indexPath.row] + "\r" + "***" + exp + "***" + "\r" + self.mmExpArray[indexPath.row] + "\r"
        }
        let english = UIAlertAction(title: "English", style: UIAlertActionStyle.default) { (UIAlertAction) -> Void in
            
            let indexTitle2 = String(indexPath.row + 1) + "/" + String(self.engArray.count)
            let exp : String = self.expText[1]
            cell.usernameLbl.text = "\r" + indexTitle2 + "\r" + self.engArray[indexPath.row] + "\r" + "***" + exp + "***" + "\r" + self.engExpArray[indexPath.row] + "\r"
        }
//        let facebook = UIAlertAction(title: "Facebook", style: UIAlertActionStyle.default) { (UIAlertAction) -> Void in
//            
//            if SLComposeViewController.isAvailable(forServiceType: SLServiceTypeFacebook){
//                let facebookSheet:SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
//                facebookSheet.setInitialText(cell.usernameLbl.text!)
//                self.present(facebookSheet, animated: true, completion: nil)
//            } else {
//                self.showAlert()
//            }
//        }
//        
//        
//        menu.addAction(facebook)
        menu.addAction(myanmar)
        menu.addAction(english)
        menu.addAction(cancel)
        
        // show menu
        self.present(menu, animated: true, completion: nil)
        
        
        
    }
    
    func showAlert(){
        let alert = UIAlertController(title: "Accounts", message: "Please login to a Facebook account to share.", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }

    
}
