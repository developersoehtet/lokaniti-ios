//
//  textCell.swift
//  Lokaniti
//
//  Created by Soe Htet on 2/11/16.
//  Copyright © 2016 Soe Htet. All rights reserved.
//

import UIKit

class textCell: UITableViewCell {

    @IBOutlet weak var textLbl: UITextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        textLbl.translatesAutoresizingMaskIntoConstraints = false
        
        self.contentView.addConstraints(NSLayoutConstraint.constraints(
            withVisualFormat: "V:|-10-[txt]-10-|",
            options: [], metrics: nil, views: ["txt":textLbl]))
        
        self.contentView.addConstraints(NSLayoutConstraint.constraints(
            withVisualFormat: "V:|-5-[txt]-5-|",
            options: [], metrics: nil, views: ["txt":textLbl]))
        
    }

    
}
