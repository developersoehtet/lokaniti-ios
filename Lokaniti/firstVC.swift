//
//  firstVC.swift
//  Lokaniti
//
//  Created by Soe Htet on 2/11/16.
//  Copyright © 2016 Soe Htet. All rights reserved.
//

import UIKit

class firstVC: UITableViewController {
    
    var namesArray : [String] = ["Lauren Richard Lauren Richar Lauren Richar Lauren Richar Lauren Richar Lauren Richar Lauren Richar Lauren Richar Lauren Richard Lauren Richar Lauren Richar Lauren Richar Lauren Richar Lauren Richar Lauren Richar Lauren RicharLauren Richard Lauren Richar Lauren Richar Lauren Richar Lauren Richar Lauren Richar Lauren Richar Lauren RicharLauren Richard Lauren Richar Lauren Richar Lauren Richar Lauren Richar Lauren Richar Lauren Richar Lauren Richar", "Nicholas Ray", "Kim White", "Charles Gray", "Timothy Jones", "Sarah Underwood", "William Pearl", "Juan Rodriguez", "Anna Hunt", "Marie Turner", "George Porter", "Zachary Hecker", "David Fletcher"]


    override func viewDidLoad() {
        super.viewDidLoad()
        

        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 200
    }

  
    
    // number of cell
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return namesArray.count
    }
    
    // cell for row id
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! textCell
        
        
        cell.textLbl.text = namesArray[indexPath.row]
        
        
        return cell
        
    }
    
   

    
}
