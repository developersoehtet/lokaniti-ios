//
//  womenVC.swift
//  Lokaniti
//
//  Created by Soe Htet on 6/11/16.
//  Copyright © 2016 Soe Htet. All rights reserved.
//

import UIKit
import Social

class womenVC: UITableViewController {
   
    
    var expText : [String] = ["ရွင္းခ်က္","Explanation"]
    
    var mmNumArray : [String] = ["၁","၂","၃","၄","၅","၆","၇","၈","၉","၁၀","၁၁","၁၂","၁၃","၁၄","၁၅","၁၆","၁၇","၁၈","၁၉","၂၀","၂၁","၂၂","၂၃","၂၄","၂၅","၂၆","၂၇","၂၈","၂၉","၃၀","၃၁","၃၂","၃၃","၃၄","၃၅","၃၆","၃၇","၃၈","၃၉","၄၀"]
    
    var mmArray : [String] = ["ဥၾသတို႔၏ အဆင္းကားအသံလွ်င္တည္း။ မိန္းမတုိ႔၏ အဆင္းကား လင္ကိုျမတ္ႏိုးျခင္း တည္း။ မေကာင္းေသာ ရုပ္သ႑ာန္ရွိေသာသူ၏ အဆင္းကား အတက္တည္း၊ ရေသ့တို႔၏ အဆင္းကား သည္းခံျခင္းတည္း။",
        "မိန္းမတို႔၏ ဥစၥာကား အဆင္းတည္း။ ေယာက္်ားတို႔၏ ဥစၥာသည္ အတက္လွ်င္တည္း။ ဥစၥာ ကားသီလွ်င္တည္း။ မင္းတို႔၏ ဥစၥာသည္ကား ရဲမက္ဗိုလ္ပါတည္း။",
        "ရေသ့ ရဟန္းတို႔သည္ကားႀကံဳမွသာလွ်င္ တင့္တယ္ကုန္၏။ ေျခေလးခုရွိေသာ သတၱ၀ါတို႔ သည္ကား ဆူကုန္မွသာလွ်င္ တင့္တယ္ကုန္၏။ ေယာက္်ားတို႔သည္ ကားအတက္ပညာရွိမွ်သာ လွ်င္ တင့္တယ္ကုန္၏။",
        "တက္လွစြာေသာ ေစာင္းသမားသည္ ငါးရက္မွ် ေစာင္းႏွင့္ကင္းမူပ်က္၏။ တက္လွစြာေသာ ေလးသမားသည္ ခုႏွက္ရက္မွ် ေလးႏွင့္ ကင္းမူပ်က္၏။ ေတာ္လွစြာေသာ မယားျဖစ္လွ်င္မူလည္း လင္ႏွင့္တစ္လကင္းမူ ပ်က္၏။ တပည့္တို႔ကား လခြဲကင္းမူ ပ်က္၏။",
        "ကၽြဲကားညႊန္ရွိမွ သာလွွ်င္ေမြ႔ေလ်ာ္၏။ ဟသၤာငွက္သည္ ေရကန္ရွိမွသာလွ်ွင္ေမြ႔ေလ်ာ္၏။ မိန္းမကား ေယာက္်ားရွိမွသာ ေမြ႕ေလ်ာ္၏။ ရဟန္းကား တရားရွိမွသာလွ်င္ ေမြ႔ေလ်ာ္၏။",
        "ထမင္းကိုကား အစာေက်မွ ခ်ီးမြမ္းရာ၏။ မယားကိုလည္း အရြယ္လြန္မွ ခ်ီးမြမ္းရာ၏။ ရဲမက္ ကိုကား စစ္ေအာင္၍ျပန္မွ ခ်ီးမြမ္းရာ၏။ ေကာက္ကို အိမ္သို႔ ေရာက္မွ ခ်ီးမြမ္းရာ၏။",
        "ႏွစ္လင္ သံုးလင္ကြာၿပီးေသာ မိန္းမသည္လည္းေကာင္း၊ ႏွစ္ေက်ာင္း သံုးေက်ာင္းေျပာင္းၿပီး ေသာ ရဟန္းသည္လည္းေကာင္း၊ ေက်ာ့ကြင္းမွ ႏွစ္ႀကိမ္သံုးႀကိမ္လြတ္ၿပီးေသာ ငွက္သည္လည္း ေကာင္း ျပဳအပ္ေသာ မာယာမ်ားသည္သာလွ်င္တည္း။",
        "သူယုတ္ကို ပုတ္ခတ္သျဖင့္ ယဥ္ေစရာ၏။ မေကာင္းေသာ ခင္ပြန္းကို စကားဆိုၿမဲ မဆိုသ ျဖင့္ယွဥ္ေစရာ၏။ မေကာင္းေသာ မိန္းမတို႔ကို စည္းစိမ္ကို မအပ္ႏွင္းသျဖင့္ယဥ္ေစရာ၏။ အစာ၌ တပ္မက္ေသာ သူကိုအစာကိုယုတ္ေစသျဖင့္ ယဥ္ေစရာ၏။",
        "လမွကင္းေသာ ညသည္ မတင့္တယ္။ လိႈင္းတံပိုးမွကင္းသာ သမုဒၵရာသည္လည္း မတင့္တယ္။ ဟသၤာမွကင္းေသာ ေရကန္သည္လည္း မတင့္တယ္။ လင္မွကင္းေသာ အမ်ိဳးသမီး သည္လည္း မတင့္တယ္။",
        "လင္သည္သာလွ်င္ စည္းစိမ္ဥစၥာကို ျဖစ္ေစအပ္၏။ မိန္းမမူကား လံုေစသည္ သာလွ်င္တည္း။ ထိုစကားသည္ သင့္စြ။ ေယာက္်ားသည္သာလွ်င္ ပဓာနတည္း။ မိန္းမ မူကား အပ္၌ ခ်ည္ႏွင့္သာ တူပါသတည္း။",
        "ခပ္သိမ္းေသာ ျမစ္တို႔သည္ ျမစ္ေကာက္သာရွိကုန္၏။ ခပ္သိမ္းေသာ ေတာတို႔သည္ ထင္းျဖင့္သာ ၿပီးကုန္၏။ ခပ္သိမ္းေသာ မိန္းမတို႔သည္ ဆိတ္ၿငိမ္ေသာ အရပ္ကို ရသည္ရွိေသာ္ မေကာင္းမႈကို ျပဳၿမဲတည္း။",
        "အၾကင္မိန္းမသည္ ျငင္းခံုတက္ေသာ အေလ့ရွိ၏။ ျငဴစူေစာင္းေျမာင္း၍ ေျပာေလ့ရွိ၏။ ျမင္တိုင္းကိုလည္း တပ္၏။ မ်ားစြာသည္လည္း ခ်က္၍စားတက္၏။ လင္၏အဦးစားတက္ေသာ အေလ့ရွိ၏။ သူ႔အိမ္သို႔ သြားတက္ေနတက္ေသာ အေလ့ရွိ၏။ ထိုမိန္းမသည္ သားတစ္ရာပင္ ျမင္ ျငားေသာ္လည္း ေယာက္်ားသည္ စြန္႔အပ္၏။",
        "အၾကင္မိန္းမသည္ စားေသာအခါ၌လည္းေကာင္း၊ အ၀တ္တန္ဆာ ျပဳျပင္ဆင္ယင္ေသာ အခါ၌ လည္းေကာင္းအမိကဲ့သုိ႔ ႏွစ္သက္ဖြယ္ျပဳျပင္တက္၏။ လွ်ိဳ႔၀ွက္အပ္ကုန္ေသာ မက်စ္မလ်စ္ ေသာအရာတို႔၌ ႏွမကဲ့သုိ႔ လြန္စြာရွက္တက္၏။ အမႈလုပ္ေသာအခါတို႔၌လည္းေကာင္း၊ လင္း၏အပါး၌ သြားလာေသာအခါ၌လည္းေကာင္း၊ ကၽြန္မကဲ့သို႔ ရိုေသစြာျပဳတက္၏။ ေဘးရွိေသာ အခါတို႔၌ တိုင္ပင္ဖက္ျဖစ္ရာ၏။ အိပ္ေသာအခါတို႔၌လည္း ေမြ႔ေလ်ာ္ေစတက္၏။ အေရာင္အဆင္း တို႔ကိုျပဳျပင္ျခင္းတို႔၌ တင့္တယ္စြာ၏။ အမ်က္ထြက္ေသာ အခါတို႔၌ သည္းခံတက္၏။ ထိုမိန္းမကို ျမတ္၏ဟူ၍ ပညာရွိတို႔သည္ ဆိုကုန္၏။ ထိုမိန္းမသည္ ေသသည္၏ အျခားမဲ၌လည္း နတ္ျပည္၌ ျဖစ္ရာ၏။",
        "အၾကင္မိန္းမငယ္သည္ ေရႊသားအသားရွိ၏။ သမင္မ်က္စိသကဲ့သို႔ ညိဳေသာမ်က္စိလည္း ရွိ၏။ ခါးစည္းတင့္က်ယ္၊ ငယ္ေသာ၀မ္းလည္းရွိ၏။ ဆင္ႏွာေမာင္းသကဲ့သို႔ အရင္းမွအဖ်ားတိုင္ ေအာင္သြယ္ေသာေပါင္ရွိ၏။ ေျဖ၍ ေနာက္သို႔လန္ေသာရွည္ေသာ အဖ်ားေကာတက္ေသာဆံ လည္းရွိ၏။ ႀကီးငယ္မထင္ ညီညႊတ္ေသာသြားအစဥ္းလည္း ရွိ၏။ နက္ေသာ ခ်က္လည္းရွိ၏။ ေကာင္းေသာအေလ့အထလည္း ရွိ၏။ ထိုသို႔ေသာ မိန္းမငယ္ကို ယုတ္ေသာအမ်ိဳးတို႔၌ ျဖစ္ေသာ္လည္း ထိုမိန္းမပ်ိဳကို ဆက္ဆံထိမ္းျမားရာ၏။",
        "ကာလတို႔တြင္ တန္ေဆာင္မုန္းလသရဒဥတု သည္ျမတ္၏။ မယားတို႔တြင္ အဆင္းလွေသာ မိန္းမတို႔သည္ျမတ္၏။ သားတို႔တြင္ သားႀကီးသည္ျမတ္၏ ။ အရပ္မ်က္ႏွာတို႔တြင္ ေျမာက္အရပ္ သည္ျမတ္၏။",
        "အၾကင္မိန္းမသည္ ျဖစ္တိုင္းျဖစ္တိုင္း တဖန္တလဲလဲ ေယာက္်ား၏အျဖစ္ကိုအလိုရွိျငားအံ့၊ ထိုမိန္းမသည္ သိၾကားမင္း၏မယားသည္ သိၾကားမင္းကိုအရိုအေသလုပ္ေကၽြးသကဲ့သို႔ လင္ကို ရိုေသလုပ္ေကၽြးရာ၏။",
        "အၾကင္ေယာက္်ားသည္ ျဖစ္တိုင္းျဖစ္တိုင္း အဖန္တလဲလဲ ေယာက္်ား၏အျဖစ္ကို အလိုရွိ ျငားအံ့၊ ထိုေယာက္်ားသည္ သူတစ္ပါး၏မယားကို ေျခေဆးၿပီးေသာ ေယာက္်ားသည္ညႊန္ကို ေရွာင္ၾကဥ္သကဲ့သို႔ ေရွာင္ၾကဥ္ရာ၏။",
        "အရြယ္လြန္ၿပီးေသာေယာက္်ားသည္ တည္သီးမွ်ငယ္ေသာ သားျမတ္ရွိေသာ မိန္းမပ်ိဳကို .ယူေဆာင္ သိမ္းဆည္၏။ မိမိအလိုရွိရာ မပါသည္ျဖစ္၍ ေစာင္းေျမာင္းျငဴစူတက္၏။ လင္အို တစ္ေယာက္၌ သာ ေမြ႔ေလ်ာ္လတၱံဟု မယံုၾကည္။ ထို႔ေၾကာင့္ ထိုငယ္ေသာ မယားကိုသိမ္းဆည္း ျခင္းသည္ ပ်က္ဆီးအံ့သည္၏ အေၾကာင္းတည္း။"]
    
    var engArray : [String] = ["The beauty of cuckoos is their voice. The beauty of women is devotedness to their husbands. The beauty of ugly person is education. The beauty of hermits is forbearance.",
        "The wealthy women is beauty. The wealth of men is education. The wealth of monks is the percepts (sila). The wealth of kings is the army.",
        "It is proper for hermits and monks to be lean. It is proper for four-legged creatures to be plump. It is proper for men to be educated. It is proper for women to be with their husbands.",
        "An accomplished harpist becomes weak in his musical skill if separated from his harp for five days. An expert archer becomes faulty if he is away from his bow and arrow from seven days. A virtuous wife is liable to folly if she is separated from her husband for a month. Pupils would falter in their lessons if they are away from their teacher for half a month.",
        "Buffalo-takes delight in mire. Hintha (brahminy duck) takes delight in the pond. Woman takes delight in her man. The monk takes delight in the Dhamma.",
        "Food is to be praised after is digested. The wife should be praised after she has passed her prime. The soldier should be honored after his successful return from the battlefield. The crop should be said to be successful when it arrives at the storehouse.",
        "The woman who has changed two or three husbands; the monk has changed two or three monasteries; the bird which has escaped from two or three traps; these have played practical tricks.",
        "The wicked man should be tamed by beating. The bad friend should be tamed by refusing to speak with him. The bad wife should be tamed by refusing to bestow wealth. The gourmand should be tamed by giving him less food.",
        "The night without the moon lacks glory. The ocean without huge waves, too, is not grand. The pond without hintha birds lacks beauty. The young woman without husband is not quite becoming.",
        "The husband is the earner of wealth. The wife is the keeper of the wealth thus earned. That is the appropriate remark. Man is the prime mover. The wife is just a thread in the needle.",
        "All rivers flow in a crooked way. All forests are full of firewood. All women getting to a secluded place will always do evil.",
        "Woman is in the habit of making dispute, of slighting others, wants to have everything she sees. She cooks many kinds of food and eats too much. She has also the habit of taking meal a head of her husband. She often visits other people’s houses and stays there long. Such woman should be left by her husband even though she has given birth to one hundred sons.",
        "At meal and when dressed, a woman should behave like a mother. In matters of concealment and undress, she should be as shy as a sister. In performance of domestic duties and approaching her husband, she should move about like a maidservant. In time of danger, she should give pleasure. She should look prim and proper in her dress. When the husband is in anger, she should have forbearance. Such a woman, say the sages, is noble. After death has claimed her body, she should go to the celestial kingdom.",
        "A certain young lady is golden complexioned, her eyes brown like a fawn’s eyes, her stomach with waist narrow and wide haunches, and thighs which tapers toward the feet, like an elephant’s trunk, long hair with ends upturned, even rows of teeth, deep navel and good conduct. Such maiden, through of low birth, should be taken to wife.",
        "Of all seasons, the season covered by the month of Tazaungmon (of Myanmar Calendar) is the best. Among the sons, the eldest is the best. Of all directions, north is the best.",
        "It a woman is every existence of beginning a woman desires to become man, she should serve her husband as respectfully as the wife of the king of celestial kingdom serves her husband.",
        "If a man desires to become a man in his existences, he should avoid committing adultery as he would avoid mud in order not to soil his clean feet.",
        "A man past his prime marries a young woman whose breasts are as tiny as fig. If the young wife does not satisfy his desire, the old man is jealous and talks slightingly of her. He does not believe that she will be satisfied with an old man. Therefore, the old man’s talking a very young woman will lead him to ruin."]
    
    
    var mmExpArray : [String] = ["မိန္းမတို႔၏ အဆင္းကားလင္ကိုျမတ္ႏိုးျခင္းတည္းဟု ဆိုရာ၌ မိန္းမတို႔ ဆိုသည္မွာ မယားတို႔ပင္ျဖစ္၏။ မယား၀တၱရားေက်ပြန္ေသာ မိန္းမသည္ က်က္သေရရွိ၏ ျမတ္၏။",
        "ဥစၥာသည္ အင္အားပင္ျဖစ္သည္။ အားကိုရာ ပစၥည္းပင္တည္း။ မင္း၏တန္ခိုးသည္ လက္နက္ကိုင္တပ္မ်ား ေပၚတြင္မူတည္သည္။",
        "သင့္တင္ေလ်ာက္ပတ္ျခင္းသည္ အေရးပါေသာ အရည္အေသြးတစ္ခုပင္ျဖစ္၏။ မသင့္ေလ်ာ္ျခင္းသည္ မတင့္တယ္။ ရဟန္းသည္ အရာရာ၌ ေရာင္ရဲျခင္းရွိမွ တင့္တယ္သည္။ ၿခိဳးၿခံ ျခင္းေၾကာင့္ ႀကံဳလွီးေသာ ကိုယ္ကာယရွိသည္။ ေျခေလးေခ်ာင္း သတၱ၀ါတို႔ကား ၀မွၾကည့္ေကာင္း သည္။ ေယာက္်ားတို႔အဖုိ႔ ပညာရွိမွ တင့္တယ္သည္.။ မိန္းမတို႔မွာ လင္သားရွိမွ တင့္တယ္သည္။ အိမ္ေထာင္ရွိမွ လူရိုေသမည္။",
        "ကင္းကြာမႈသည္ နဂိုအေျခအေနကို ပ်က္ျပားေစတက္သည္။ ေစာင္းသမားႏွင့္ ေလးသမားတို႔သည္ သူတို႔၏ ေစာင္းႏွင့္ေလးတို႔ကို ရက္ၾကာၾကာ မကိုင္ဘဲေနလွ်င္ လက္ပ်က္တက္ သည္။ မယားေကာင္းသည္ လင္ႏွင့္တစ္လမွ် ကြဲကြာလွ်င္ ေဖာက္ျပန္တက္သည္။ တပည့္တို႔သည္ ဆရာႏွင့္ ေ၀းေနၾကလွ်င္ သင္ၾကားထားသမွ် အခ်ိဳ႕ကိုေမ့ကုန္တက္ၾကသည္။ .ယင္းအဆိုတို႔သည္ ျဖစ္တက္ျခင္းကိုသာ ျပသည္။ ျခြင္းခ်က္မ်ား ရွိေပသည္။",
        "ႏွစ္သက္မႈရွိမွ ေမြ႔ေလ်ာ္သည္။ ေပ်ာ္သည္ဟု ဆိုသည္။ စိတ္၀င္စားမႈရွိမွ ႏွစ္သက္ သည္။ နီတိဆရာက ဆိုလိုသည္မွာ သဘာ၀အားျဖင့္ ေမြ႔ေလ်ာ္ျခင္းပင္ ျဖစ္၏။",
        "မည္သည့္ကိစၥမဆို မၿပီးေျမာက္ေသးခင္ ေအာင္ျမင္ၿပီဟု မဆိုသာေပ။ မေမွ်ာ္လင့္ ေသာ မထင္မိေသာ အေၾကာင္းတစ္ခု ေပၚလာႏိုင္သည္။ ထိုအေၾကာင္းေၾကာင့္ အေျခအေနေျပာင္း လဲသြားႏိုင္သည္။ ၿပီးေျမာက္မွသာ ေအာင္ျမင္သည္ဟု သက္မွတ္ရေပသည္။ နီတိဆရာသည္ မိန္း မမ်ားကို ဖိ၍ဆိုတက္သည္။ ယင္းသည္ ေယာက္်ားတို႔၏ ပင္ကိုသေဘာထားဟု ဆိုႏိုင္သည္။ သို႔ေသာ္ ျခြင္းခ်က္သည္ ရွိစၿမဲပင္။",
        "သံုးေက်ာင္းေျပာင္းေသာ ရွင္၊ သံုးလင္ေျပာင္းေသာ မယားဆိုေသာ စကားရွိသည္။ မာယာၾကြယ္သည္ ဟုယူဆၾကသည္။ သို႔ေသာဤေနရာမွာလည္း ျခြင္းခ်က္မ်ား ရွိသည္။ မာယာမ်ားေသာေၾကာင့္ မဟုတ္ဘဲ မညီညႊတ္ေသာ အေၾကာင္းမ်ားလည္း ရွိႏိုင္သည္။",
        "ယဥ္ေစရာ၏ ဆိုေသာစကားမွာ မုိုက္ရိုင္းေနသည္ကို ယဥ္ေက်းလာေအာင္ ဆံုးမျခင္းကို ဆိုလိုသည္။ ယုတ္မာသူကို ရိုက္ႏွက္ဆံုးမၿပီး ယုတ္မာျခင္းမေပ်ာက္ေစသင့္သည္ ဆိုရာ၍ သူယုတ္သည္ ေကာက္က်စ္၏။ သူကိုအၾကမ္းဖက္လွ်င္ သင့္ေလွ်ာ္မလားဟုစဥ္းစားၿပီးမွ ရိုက္ႏွက္ဆံုးမရမည္သာ ျဖစ္သည္။ မိတ္ေဆြကဆိုးလွ်င္ သူ႔ကုိစကားမေျပာဘဲဲ ေနလိုက္ျခင္းျဖင့္ ဆံုးမရာ၏ ဆိုရာမွာလည္း မိမိကစကားမေျပာဘဲေနလွ်င္ သူကလည္းမာနျဖင့္ မေျပာလိုျဖစ္ႏိုင္ သည္။ သူ႔အမွားရွိလွ်င္ ေထာက္ျပသင့္သည္။ မယားဆိုးကို ဥစၥာမအပ္ျခင္းသည္ သင့္၏ဟု ထင္ပါသည္။ အစားႀကီး သူကိုေလွ်ာ့ေကၽြးသင့္၏။",
        "ၾကည့္ရႈရာ၌ မျပည့္စံုသည့္ အခ်က္တို႔ေၾကာင့္ တင့္တယ္ျခင္းမရွိေခ်။ လိုက္ဖက္ေသာ ပစၥည္းမပါလွ်င္ သင့္ေလ်ာ္ေသာပံုစံကို မရွိေခ်။",
        "လင္သားက ပစၥည္းဥစၥာရေအာင္ အလုပ္လုပ္ရသည္။ မယားကရသမွ် ဥစၥာတို႔ကို သိမ္းဆည္းရသည္။ ထိုစကားသည္ မွန္ပါ၏။ သို႔ေသာ္ ေခတ္ကာလ အေလ်ာက္အေျခအေန ေျပာင္းလဲျခင္းမ်ား ရွိေနသည္။ လင္ေရာမယားပါ ေငြေၾကးရွာရေသာေခတ္သို႔ ေရာက္ေလၿပီ။ သိုေသာ္လည္း ေယာက္်ားက ဦးေဆာင္ရမည္ကားမွန္ေပသည္။ မိန္မကရလာသမွ်ကို မိသားစု အတြက္ သင့္သလိုသံုးၿပီးလွ်င္ က်န္ရွိသမွ်ကို သိမ္းဆည္းရေပမည္။",
        "ျမစ္တုိ႔သည္ ေကာက္ေကြ႔စီး၏။ ေတာတို႔၌ ထင္းမ်ားျပည့္လ်က္ရွိ၏။ မိန္းမတို႔သည္ ဆိတ္ကြယ္ရာအရပ္၌ ေဖာက္ျပားၿမဲျဖစ္သည္။ မိန္းမသားမွာ အားႏြဲ႔သူျဖစ္သျဖင့္ ေယာက္်ားတို႔ ေစာ္ကားမႈကို ခံရသည္သာျဖစ္သည္။ ေယာက္်ားကစ၍ေဖာက္ျပားသည္သာမ်ား၏။ မိန္းမက ျမွဴဆြယ္သည္းလည္း ရွိ၏။ ႏွစ္ေယာက္မွာ အျပစ္ရွိသည္။",
        "မေကာင္းေသာမိန္းမအေၾကာင္းကုိ ေဖာ္ျပျခင္း ျဖစ္သည္။ ထိုမိန္မမ်ိဳးကို ေယာက္်ားက ေပါင္းသင္၍ မည္မွ်ပင္ၾကာေစကာမူ စြန္႔သင့္သည္ဟုဆို၏။ မယားဆိုးကို ဆက္၍ေပါင္းေနလွ်င္ ေယာက္်ားမွာနစ္နာသည္ မွန္၏။ တစ္ျပန္တစ္လွည့္ဆိုရပါမူ လင္းဆိုးကိုေပါင္းရေသာ မိန္မကလည္း လင္ကိုမစြန္႔သင့္ပါသေလာ။ မိန္းမက စြန္႔ရန္ခက္လွသည္။ ေယာက္်ားကမိန္းမကို ပိုင္သည္။ မိန္းမက ေယာက္်ားကို မပိုင္ေခ်။ ယခုအခါ ေခတ္ေျပာင္းေလၿပီ။ မေကာင္းလွ်င္ မေကာင္းသူကို ေကာင္းသူက စြန္႔ပစ္သင့္သည္ဟုဆိုလွ်င္ ပို၍ သင့္သည္။",
        "မိန္းမေကာင္း မိန္းမျမတ္တို႔၏ က်င့္ထံုးျဖစ္သည္။ လင္ကိုရိုေသျခင္း၊ အိမ္မႈတာ၀န္ ကိုေက်ပြန္ျခင္း ႏွင့္ သစၥာရွိျခင္းတို႔သည္ အေရးပါဆံုးျဖစ္သည္ဟု ထင္ပါသည္။",
        "မိန္းမပ်ိဳ၏ ကိုယ္ကာယအလွႏွင့္ ေကာင္းေသာအမူအက်င့္တို႔ကို ေဖာ္ျပသည္။ ထိုမိန္းမပ်ိဳကို ယုတ္ညံ့ေသာ အမ်ိဳးမွပင္ျဖစ္ေစ၊ ထိန္းျမားရန္ မေႏွာင္ေႏွးသင့္။ ေျခာက္ျပစ္ကင္း သည္ ရွာလွေခ်သည္။ အေပၚယံအလွထက္ စိတ္အလွကပိုအေရးႀကီးပါသည္။ စိတ္အလွတြင္ အမူအရာရည္မြန္ျခင္းႏွင့္ အက်င့္ေကာင္းျခင္းတုိ႔ ပါ၀င္သည္။",
        "တန္ေဆာင္မုန္းလသည္ မိုးေလကင္းစင္၍ သာယာသည္။ ေကာင္းကင္ျပင္တြင္ နကၡတ္စံုသည္။ ေပ်ာ္ပြဲရႊင္မ်ား က်င္းပၾကသည္။ ထိုသို႔ေသာ သာယာေသာကာလသည္ အေကာင္းဆံုးျဖစ္သည္။ မိန္းမအေပါင္းတို႔တြင္ လွပေသာ မိန္းမတို႔သည္ အေကာင္းဆံုးျဖစ္ေသာ ေၾကာင့္ ထိမ္းျမားသင့္သည္။ သားတို႔တြင္အႀကီးဆံုးသားသည္ အရင္ဆံုး အားကိုးရတက္သည္။ ထို႔ေၾကာင့္ အေကာင္းဆံုးျဖစ္သည္ ဆိုႏိုင္သည္။ အရပ္ရွစ္မ်က္ႏွာတြင္ ေျမာက္အရပ္ ဗုဒၶၶၶျမတ္စြာ ေျမာက္အရပ္ကို ေခါင္းျပဳ၍ ပရိနိဗၺာန္စံေတာ္မူေသာေၾကာင့္ ျမတ္၏ဟု ယူဆၾကသည္။",
        "မိန္းမဘ၀သည္ ေယာက်္ားဘ၀ထက္ ေလ်ာ့နိမ့္၏ဟု ယူဆၾကသည္အေလ်ာက္ မိန္းမတစ္ေယာက္က ေယာက္်ားဘ၀ ရလိုပါေၾကာင္း မိန္မဘ၀ရတိုင္း ဆုေတာင္လွ်င္ ထိုမိန္းမက မိမိလင္ကို အထူးရိုေသစြာ လုပ္ေကၽြးသမႈျပဳရပမည္။",
        "လင္ရွိမယားကို ၾကာခိုျခင္းေၾကာင္း ထိုေယာက္်ားသည္ ငရဲက်ၿပီးေနာက္ လူျပန္ျဖစ္လွ်င္ မိန္းမဘ၀သို႔ ေရာက္တက္သည္။ ထို႔ေၾကာင့္ ေယာက္်ားဘ၀ကို ဆက္လက္ရရွိ လိုလွ်င္ ထိုအကုသိုလ္ကံကို ေရွာင္ရေပမည္။",
        "အရြယ္ခ်င္းမမွ်သူတို႔ ေပါင္းဖက္ျခင္း၌ အႏၱရာယ္ရွိတက္သည္။ ဇရာအိုႏွင့္ ကညာပ်ိဳတို႔ေပါင္းဖက္လွ်င္ အဖိုးအိုက သူ႔ဇနီးကို မယံုႏိုင္ေခ်။ ယင္းသို႔ ေပါင္းဖက္ျခင္းသည္ ထို႔သူ အုိ႔အဖို႔ ပ်က္စီးရာအေၾကာင္းျဖစ္သည္။"]
    
    var engExpArray : [String] = ["The beauty of women “is devotion to their husbands. Here “women” stand for “wives”. A dutiful wife is graceful and noble.",
        "Wealth is one’s possession, which can be depended upon. The power of a monarch lies in his armed forces.",
        "Being proper is an important quality. Improper means unbecoming. A monk should be contented and frugal, so he will be of a thin body, and this is proper for ascetics. Four-legged animals must be plump so that they will e liked by people. A man must be educated to be fit for a good position in life. A woman must be married, or she will not get due respects from people.",
        "Separation usually makes for a decline from normal situation. Harpist and archer would weaken in their skill if they lacked in their practice for sometime. A virtuous wife would possibly weaken her virtue if separated from her husband for a month. Pupils away from their teacher for a half month might forger some of the lessons. However, there are exceptions.",
        "Delight sterns from being pleased. In other words, if one is delighted one becomes happy. Interest precedes pleasure. The author of Niti means to say that such pleasures occur naturally.",
        "No matter can be said to be successful until it is achieved. It is premature to say that an act is successful until the end is reached. The Niti master is usually critical about women. It is the general attitude of men. However, there are always exceptions.",
        "“The monk who changes three monasteries, the woman who changes three husbands”, so it is said, are regarded as full of wiles. Here, too, there are exceptions. The monk who is well intentioned has to change his residence because the inmates are lax about the Law. The woman who is earnest about marriage finds the husband bad. So she is obliged to change her rather unstable mate in wedlock.",
        "To tame means to let the person concerned mend his ways and become cordial. To tame a wicked person by beating raises the question of whether it is possible to tame him by beating. The wicked one is usually crafty. We should first consider whether or not it is good to beat him. Taming a bad friend by refusing to talk with may not be good, for the bad friend will probably refuse to talk because his pride is hurt. It might be better to point out his mistake, thus giving him a chance to correct himself. It is good to deprive the bad wife of the wealth. It is also good to reduce the diet if an over eater.",
        "In observing things, the suitable property should be present to make them look good. If such property were missing, they would not be in their proper form.",
        "The husband has to earn money. The wife has to preserve it. That is correct. But times have changed. We have come to the age when the husband, as well as the wife, is obliged to go to work to earn money. However, the fact remains that man is the leader in the family and woman has to keep in store the money left after spending for the family.",
        "Rivers meander forests are full of firewood. That is correct. As for the statement that woman commits sin in a secluded place needs qualification. Women are weak. A woman has to suffer man’s attack. It is actually man who starts the evil deed. Of course, woman also entices. The fault lies with both.",
        "This is a statement about a bad wife. If the husband continues keeping her in wedlock, however long their married life may be the husband is the loser. That is true. On the contrary, if the husband turns bad, should not the wife desert him? It is difficult for her to do that. It is the man who owns his wife, not the wife her husband. Times have changed now. It would be better to say that the good should abandon the bad.",
        "This is the code of conduct for a woman who should be deemed good and noble. Respect for the husband, dutiful as a housewife and loyalty are perhaps most important.",
        "The stanza describe the physical comeliness and good conduct of a young lady. One should not hesitate to marry such a lady. Perfection is rare. Intrinsic beauty is more important than superficial beauty. Intrinsic beauty includes charming acts and a good character.",
        "The Myanmar month of Tazaungmon is the time of no rain and is cool, for it is the beginning of cold season. So it is the best time for festivities. So it is the best time for the year. Beautiful woman is the best to marry. The eldest son is usually the most dependable, so he is regarded as the best son. North is considered sacred because Buddha lay with his head to that direction when He passed into NIbbana.",
        "Womanhood is considered inferior to manhood. Therefore, if a woman desires to become a man in her next existence and accordingly prays for it, she should serve her husband very respectfully.",
        "Committing adultery sends the sinner down to hell, and if he recovers the existence of a human being, he will become a woman. Therefore, he must avoid that sin.",
        "Marriage of persons of unequal age has dangers. An old man will not have faith in his young wife. Such a unicorn constitutes a cause for the old man’s ruin."]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 20
        
        self.tableView.register(UINib(nibName: "text2Cell", bundle: nil), forCellReuseIdentifier: "text2Cell")
        
    }
    
    // number of cell
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return engArray.count
    }
    
    // cell for row id
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "text2Cell") as! text2Cell
        
        let tmp = UserDefaults.standard.integer(forKey: "CurrentLanguage")
        if tmp == 0{
            let indexTitle : String = mmNumArray[indexPath.row] + "/" + mmNumArray[mmArray.count - 1]
            let exp : String = expText[0]
            cell.usernameLbl.text = "\r" + indexTitle + "\r" + mmArray[indexPath.row] + "\r" + "***" + exp + "***" + "\r" + mmExpArray[indexPath.row] + "\r"
        }else{
            let indexTitle2 = String(indexPath.row + 1) + "/" + String(engArray.count)
            let exp : String = expText[1]
            cell.usernameLbl.text = "\r" + indexTitle2 + "\r" + engArray[indexPath.row] + "\r" + "***" + exp + "***" + "\r" + engExpArray[indexPath.row] + "\r"
        }
        
        return cell
    }
    
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! text2Cell
        
        // CANCEL ACTION
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        
        // create menu controller
        let menu = UIAlertController(title: "Menu", message: nil, preferredStyle: .actionSheet)
        
        let myanmar = UIAlertAction(title: "Myanmar", style: UIAlertActionStyle.default) { (UIAlertAction) -> Void in
            
            let indexTitle : String = self.mmNumArray[indexPath.row] + "/" + self.mmNumArray[self.mmArray.count - 1]
            let exp : String = self.expText[0]
            cell.usernameLbl.text = "\r" + indexTitle + "\r" + self.mmArray[indexPath.row] + "\r" + "***" + exp + "***" + "\r" + self.mmExpArray[indexPath.row] + "\r"
        }
        let english = UIAlertAction(title: "English", style: UIAlertActionStyle.default) { (UIAlertAction) -> Void in
            
            let indexTitle2 = String(indexPath.row + 1) + "/" + String(self.engArray.count)
            let exp : String = self.expText[1]
            cell.usernameLbl.text = "\r" + indexTitle2 + "\r" + self.engArray[indexPath.row] + "\r" + "***" + exp + "***" + "\r" + self.engExpArray[indexPath.row] + "\r"
        }
//        let facebook = UIAlertAction(title: "Facebook", style: UIAlertActionStyle.default) { (UIAlertAction) -> Void in
//            
//            if SLComposeViewController.isAvailable(forServiceType: SLServiceTypeFacebook){
//                let facebookSheet:SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
//                facebookSheet.setInitialText(cell.usernameLbl.text!)
//                self.present(facebookSheet, animated: true, completion: nil)
//            } else {
//                self.showAlert()
//            }
//        }
//        
//        
//        menu.addAction(facebook)
        menu.addAction(myanmar)
        menu.addAction(english)
        menu.addAction(cancel)
        
        // show menu
        self.present(menu, animated: true, completion: nil)
        
        
        
    }
    
    func showAlert(){
        let alert = UIAlertController(title: "Accounts", message: "Please login to a Facebook account to share.", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
}
